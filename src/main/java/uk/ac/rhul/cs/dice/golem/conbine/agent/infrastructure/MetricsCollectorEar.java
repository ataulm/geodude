package uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure;

import uk.ac.rhul.cs.dice.golem.action.AbstractSensor;
import uk.ac.rhul.cs.dice.golem.agent.AgentBody;
import uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType;

/**
 * The sensor for the MetricsCollector only interprets two action types, the
 * order from {@link MarketController} saying that it should start collecting
 * and also the notification that a run is complete.
 * 
 * @author ataulm 
 */
public class MetricsCollectorEar extends AbstractSensor {
    public MetricsCollectorEar(String id, AgentBody context) {
        super(id, context);
        addType(ConbineActionType.COLLECT_METRICS.toString());
        addType(ConbineActionType.NOTIFY_RUN_COMPLETE.toString());
    }
}
