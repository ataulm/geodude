package uk.ac.rhul.cs.dice.golem.container;

import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;

public class ContainerIdFormer {
    private static final String URI_SCHEME = "container";
    private URI id;
    private final int port;
    private String host;
    private final String name;

    public ContainerIdFormer(String name, int port) {
        this.name = name;
        this.port = port;

        try {
            host = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        try {
            id = new URI(URI_SCHEME, this.name + "'@'" + host + "':'"
                    + this.port, null, null, null);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public String getHost() {
        return host;
    }

    public String getName() {
        return name;
    }

    public int getPort() {
        return port;
    }

    /**
     * Formats the URI to a format we can use as the {@link Container} ID.
     * 
     * @return id the formatted id
     */
    @Override
    public String toString() {
        return "'" + id.toASCIIString().replace("'", "") + "'";
    }
}
