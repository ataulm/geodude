package uk.ac.rhul.cs.dice.golem.conbine.agent.faratin;

import java.util.ArrayList;
import java.util.List;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.agent.AgentBrain;
import uk.ac.rhul.cs.dice.golem.conbine.action.NegotiationAction;
import uk.ac.rhul.cs.dice.golem.conbine.agent.AbstractBuyerAgent;
import uk.ac.rhul.cs.dice.golem.conbine.agent.AgentParameters;

@SuppressWarnings("serial")
public class FaratinResourceDependentBuyer extends AbstractBuyerAgent {
	private final static int ACCEPT = 0;
	private final static int COMMIT = 1;
	private final static int EXIT = 2;
	private final static int NUM_RANDOM_ACTIONS = EXIT;
	private final static double K = 0.5;
		
	public FaratinResourceDependentBuyer(AgentBrain brain,
			AgentParameters params, String product) {
		super(brain, params, product);
	}	

	@Override
	protected List<Action> decideActionBasedOnOffer(NegotiationAction offer) {
		List<Action> actionsToPerform = new ArrayList<>();
		
		double utilityOpponentOffer = getUtility(Double.parseDouble(offer.getValue()));
		double counterOffer = generateNextOffer(offer.getDialogueId());
		double utilityMyCounterOffer = getUtility(counterOffer);
		
		if (utilityOpponentOffer >= utilityMyCounterOffer) {
			actionsToPerform.addAll(super.acceptOpponentOffer(offer));
		} else {
			actionsToPerform.addAll(super.sendCounterOffer(offer, counterOffer));
		}
		
		return actionsToPerform;
	}
	
	@Override
	protected double generateNextOffer(String dialogueId) {
		int numSellers = getDialogues().size();
		double concessionRate = K + (1 - K) * Math.exp(-numSellers);
		
		return getInitialPrice() + 
				(getReservationPrice() - getInitialPrice()) * concessionRate;
	}

	@Override
	protected List<Action> decideActionBasedOnAccept(NegotiationAction accept) {
		switch (getRandom().nextInt(NUM_RANDOM_ACTIONS)) {
			case ACCEPT:
				return super.acceptOpponentOffer(accept);
				
			case COMMIT:
				return super.commitToMyLastBid(accept);
				
			case EXIT:
				return super.exitFromDialogue(accept.getDialogueId());				
		}
		
		return new ArrayList<>();
	}
}
