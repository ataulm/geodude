package uk.ac.rhul.cs.dice.golem.physics;

import uk.ac.rhul.cs.dice.golem.action.Event;

public abstract class AbstractPhysics implements Physics {
    @Override
    public boolean attempt(Event event) {
        return isPossible(event);
    }
}
