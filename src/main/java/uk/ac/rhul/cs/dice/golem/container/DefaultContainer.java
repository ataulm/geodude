package uk.ac.rhul.cs.dice.golem.container;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.action.Event;
import uk.ac.rhul.cs.dice.golem.agent.AgentBody;
import uk.ac.rhul.cs.dice.golem.entity.Entity;
import uk.ac.rhul.cs.dice.golem.physics.Physics;
import uk.ac.rhul.cs.dice.golem.util.Logger;

public class DefaultContainer extends AbstractContainer {
    private final Map<String, Entity> entities = new ConcurrentHashMap<>();
    private final ContainerHistory history;
    
    public DefaultContainer(String name, Physics governor) {
        super(name, governor);
        history = new ContainerHistory();
    }
    
    public DefaultContainer(String name, int port, Physics governor) {
        super(name, port, governor);
        history = new ContainerHistory();
    }



    /**
     * Returns the set of entities which are registered in this
     * {@link Container}.
     * 
     * @return entities the set of registered {@link Entity} objects in this
     *         Container
     */
    protected Map<String, Entity> getEntities() {
        return entities;
    }
    
    @Override
    public Entity getEntity(String id) {
       	return entities.get(id);
    }
    
    @Override
    public void makePresent(Entity entity) {
        if (!entities.containsKey(entity.getId())) {
            entities.put(entity.getId(), entity);
        }
    }
    
    @Override
    public boolean clearAll() {
        entities.clear();
    	return history.clear() && ((entities.size() == 0) ? true : false) && super.clearSubscriptions();
    }

    @Override
    protected void notifyAll(Event event) {
        Action action = event.getAction();
        String recipient = action.getRecipient();

        if (recipient != null) {        	
            Logger.micro(this, "whisper to " + event.getAction().getRecipient()
                    + ", saying: " + event.getAction().toString());
            // Whisper to specific recipient
            Entity entity = entities.get(recipient);

            if (entity instanceof AgentBody) {
                ((AgentBody) entity).perceive(action);
            }
        } else {
            Logger.micro(this, "broadcast made, saying: "
                    + event.getAction().toString());
            super.notifySubscribed(action);
        }
    }

    @Override
    public void removeEntity(String id) {
        entities.remove(id);
    }

    @Override
    public boolean clearHistory() {
        return history.clear();        
    }

    public ContainerHistory getHistory() {
        return history;
    }


    @Override
    public boolean assertEventInHistory(Event event) {
        if (event != null) {
            return history.assertEvent(event);
        }        
        throw new NullPointerException("Cannot assert null event into ContainerHistory.");
    }

}
