package uk.ac.rhul.cs.dice.golem.conbine.agent;

public interface OfferHistory {
	/**
     * Returns this agent's last bid.
     * 
     * @return bid  the agent's last bid
     */
    public double getMyLastBid();

    public void updateMyLastBid(double bid);

    /**
     * Get one of the opponent's previous offers.
     * 
     * For n == 0, this returns the opponent's last offer.
     * For n == 1, this returns the one (opponent's) before that, and so forth.
     * 0 <= n < k, where k is the total number of offers the opponent has made.
     * For k == 0, there is no valid n.
     * 
     * If n is outside the allowed range, an IndexOutOfBounds exception will be thrown
     * 
     * @param n  the number of offers back to look, n >= 0
     * @return bid  the opponent's bid at -n
     * @throws IndexOutOfBoundsException  if !(0 <= n < k)
     */
    public double getMinusNthOpponentBid(int n);
    
    /**
     * Returns the oldest opponent bid.
     * 
     * @return
     */
    public double getOldestOpponentBid();

    /**
     * Inserts the bid at the start of the list (position 0).
     * 
     * @param bid  the bid to insert
     */
    public void addOpponentBid(double bid);
}
