package uk.ac.rhul.cs.dice.golem.conbine.action;

import org.javatuples.Triplet;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.conbine.agent.MarketAgent.AgentType;

/**
 * As per {@link ConbineActionType#EXIT_ALL}.
 * 
 * @author ataulm
 * 
 */
public class ExitAllAction extends Action {
	public static final int PRODUCT_ID = 0;
	public static final int AGENT_TYPE = 1;
	public static final int AGENT_ID = 2;
		
	public static class Builder {

        private String agentType;
        private String agentId;
        private String productId;

        public ExitAllAction build() {
            if (agentType == null || agentType.length() == 0) {
                throw new IllegalStateException(
                        "Agent type (AgentType.BUYER.toString() | AgentType.SELLER.toString()) must be set.");
            } else if (agentType.equals(AgentType.BUYER.toString())
                    ^ !agentType.equals(AgentType.SELLER.toString())) {
                throw new IllegalArgumentException(
                        "Agent type must be either [AgentType.BUYER.toString() | AgentType.SELLER.toString()]");
            }

            if (agentId == null || agentId.length() == 0) {
                throw new IllegalStateException(
                        "Agent id must be set so the MarketBroker registers the correct agent.");
            }

            if (productId == null || productId.length() == 0) {
                throw new IllegalStateException(
                        "Product id must be set so the agent knows which product is being discussed.");
            }

            Triplet<String, String, String> payload = new Triplet<>(
                    productId,
            		agentType,
                    agentId                    
            );
            return new ExitAllAction(payload);
        }

        public Builder setAgentId(String agentId) {
            this.agentId = agentId;
            return this;
        }

        public Builder setAgentType(String agentType) {
            this.agentType = agentType;
            return this;
        }

        public Builder setProductId(String productId) {
            this.productId = productId;
            return this;
        }
    }

    protected ExitAllAction(Triplet<String, String, String> payload) {
        super(ConbineActionType.EXIT_ALL.toString(), null, payload);
    }

    public String getAgentId() {
        return (String) getPayload().getValue(AGENT_ID);
    }

    public String getAgentType() {
        return (String) getPayload().getValue(AGENT_TYPE);
    }

    public String getProductId() {
        return (String) getPayload().getValue(PRODUCT_ID);
    }

    @Override
    public String toString() {
        return getAgentType() + ", " + getAgentId() + ", " + getProductId();
    }
}
