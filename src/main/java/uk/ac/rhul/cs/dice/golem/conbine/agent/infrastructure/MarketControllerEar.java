package uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure;

import uk.ac.rhul.cs.dice.golem.action.AbstractSensor;
import uk.ac.rhul.cs.dice.golem.agent.AgentBody;
import uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType;

/**
 * The sensor for the MarketController only interprets two action types, the
 * response from {@link MetricsCollector} saying that it's finished collecting,
 * and the EXIT_ALL message from MarketAgents so it can remove them from the
 * container.
 * 
 * @author ataulm 
 */
public class MarketControllerEar extends AbstractSensor {
    public MarketControllerEar(String id, AgentBody context) {
        super(id, context);
        addType(ConbineActionType.COLLECT_METRICS.toString());
        addType(ConbineActionType.EXIT_ALL.toString());
    }
}
