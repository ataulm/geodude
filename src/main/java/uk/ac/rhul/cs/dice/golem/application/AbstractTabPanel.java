package uk.ac.rhul.cs.dice.golem.application;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import net.miginfocom.swing.MigLayout;
import uk.ac.rhul.cs.dice.golem.gui.ApplicationTab;
import uk.ac.rhul.cs.dice.golem.gui.StatusBar;
import uk.ac.rhul.cs.dice.golem.util.Debug;
import uk.ac.rhul.cs.dice.golem.util.Logger;

public abstract class AbstractTabPanel extends JPanel implements
        ApplicationTab, ActionListener {

    private static final long serialVersionUID = -4984494766467511203L;
    private JButton closeBtn;
    private final Application context;
    private final String name;
    private final boolean scrollingTab;
    private final JPanel contentPane;
    private StatusBar statusBar;

    protected AbstractTabPanel(Application context, String name,
            boolean scrollingTab) {
        if (context == null)
            Logger.w("DefaultTabPanel", "Application context not set");
        if (name == null)
            Logger.w("DefaultTabPanel", "Tab name not set");

        contentPane = new JPanel();

        this.context = context;
        this.name = name;
        this.scrollingTab = scrollingTab;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (event.getSource().equals(closeBtn)) {
            destroy();
        }
    }

    /**
     * Adds a close button to the tab
     */
    private void addCloseButton() {
        JPanel tab = new JPanel(new GridBagLayout());
        tab.setOpaque(false);

        JLabel title = new JLabel(name);

        BufferedImage closeIcon = null;
        try {
            closeIcon = ImageIO.read(getClass().getResource(
                    "/resources/img/img_close.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        closeBtn = new JButton(new ImageIcon(closeIcon.getScaledInstance(15,
                15, Image.SCALE_SMOOTH)));
        closeBtn.setBorderPainted(false);
        closeBtn.setContentAreaFilled(false);
        closeBtn.addActionListener(this);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        tab.add(title, gbc);

        gbc.gridx++;
        gbc.weightx = 0;
        tab.add(closeBtn, gbc);

        int index = getContext().getDesktopManager().getTabCount() - 1;
        getContext().getDesktopManager().setTabComponentAt(index, tab);
    }

    /**
     * Perform any further cleanup required when destroying a tab. This is where
     * you should remove listeners or stop any running simulations.
     */
    protected abstract void cleanup();

    /**
     * Removes the tab from the JTabbedPane and then performs cleanup, if
     * needed.
     * 
     * Subclasses should call super.destroy() if overriding this method.
     */
    @Override
    public void destroy() {
        Logger.i(this, "destroy() called.");

        // Remove the tab from the user's view
        getContext().getDesktopManager().remove(this);

        // Remove action listeners
        closeBtn.removeActionListener(this);

        // Implement this method to perform any additional cleanup (remove
        // further action listeners, stop simulations or process engines etc.)
        cleanup();
    }

    /**
     * Returns the JPanel to which content can be added.
     * 
     * @return
     */
    @Override
    public JPanel getContentPanel() {
        return contentPane;
    }

    /**
     * Returns a reference to the encapsulating Application
     * 
     * @return
     */
    @Override
    public Application getContext() {
        return context;
    }

    /**
     * Returns the name which will be displayed on this tagetContentPane()b
     * 
     * @return
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * Application specific setup should be done here, such as adding UI
     * components to this tab.
     */
    protected abstract void initSetup();

    protected void setStatus(String status) {
        statusBar.setStatus(status);
    }

    /**
     * Adds this tab to the JTabbedPane, and sets itself as the selected tab
     */
    @Override
    public void setup() {
        setLayout(new MigLayout("wrap 1", "grow, fill", "grow, fill"));

        // Add status bar to the main JPanel, docked the bottom
        statusBar = new StatusBar();
        statusBar.addToHost(this);

        if (scrollingTab) {
            // Sets the content pane as a vertically scrollable view
            JScrollPane scroller = new JScrollPane(contentPane);
            scroller.setBorder(BorderFactory.createEmptyBorder());
            scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
            scroller.getVerticalScrollBar().setUnitIncrement(10);

            // Add the scrolling pane to the main JPanel
            add(scroller);
        } else {
            // Add the content pane to the main JPanel
            add(getContentPanel());
        }

        if (Debug.SHOW_COLORS_UI) {
            getContentPanel().setBackground(Color.YELLOW);
            this.setBackground(Color.GREEN);
            statusBar.setOpaque(true);
            statusBar.setBackground(Color.CYAN);
        }

        // add main JPanel to the TabbedPane, set it as selected
        getContext().getDesktopManager().add(this, name);
        getContext().getDesktopManager().setSelectedIndex(
                getContext().getDesktopManager().getTabCount() - 1);

        // add a close button to the tab
        addCloseButton();

        // Allow the content pane to be populated with content
        initSetup();
    }

}
