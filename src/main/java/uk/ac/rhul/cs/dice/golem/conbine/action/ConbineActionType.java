package uk.ac.rhul.cs.dice.golem.conbine.action;

import org.javatuples.Pair;
import org.javatuples.Quartet;
import org.javatuples.Quintet;
import org.javatuples.Triplet;
import org.javatuples.Tuple;
import org.javatuples.Unit;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure.Clicker;
import uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure.MarketController;
import uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure.MetricsCollector;
import uk.ac.rhul.cs.dice.golem.container.Environment;

/**
 * All elements in these ActionTypes are of type String.
 * 
 * The number of elements, and a high-level overflow is given, followed by each
 * parameter and then an example {@link Tuple}.
 * 
 * @author ataulm
 * 
 */
public enum ConbineActionType {
    /**
     * A 5-element {@link Tuple} ({@link Quintet}) describing an offer on an
     * item.
     * 
     * "protocol" "dialogue_id" "reply-to_id" "product_id" "offer_amount" "ao"
     * "456123" "myBuyer" "laptop" "500"
     * 
     */
    OFFER,

    /**
     * A 4-element {@link Tuple} ({@link Quartet}) describing a firm intention
     * to buy.
     * 
     * COMMIT is only possible by the Buyer, following an ACCEPT from Seller, or
     * an OFFER from Seller that the Buyer is happy with.
     * 
     * When the COMMIT is sent, there is a fixed time to which the Seller can
     * respond; after this time, it is assumed that the Seller agrees with the
     * COMMIT, and is only allowed to ConbineActionType.DECOMMIT. Prior to this time, the Seller
     * can EXIT, without penalty.
     * 
     * "protocol" "dialogue_id" "reply-to_id" "product_id" "ao" "456123"
     * "myBuyer" "laptop"
     */
    COMMIT,

    /**
     * A 4-element {@link Tuple} ({@link Quartet}) cancelling a firm intention
     * to buy.
     * 
     * Either the Buyer or the Seller can DECOMMIT, if the negotiation is in a
     * state of commitment.
     * 
     * There is a penalty associated with this, and once a DECOMMIT has been
     * sent in a negotiation, the negotiation enters a failed state, and no
     * further messages are transacted.
     * 
     * "protocol" "dialogue_id" "reply-to_id" "product_id" "ao" "456123"
     * "myBuyer" "laptop"
     */
    DECOMMIT,

    /**
     * A 4-element {@link Tuple} ({@link Quartet}) indicating the Seller's
     * intention to terminate the negotiation with agreement, or indicating the
     * actual termination of the negotiation with agreement, when sent by the
     * Buyer.
     * 
     * When Seller sends an ACCEPT in response to a Buyer's OFFER, the Buyer can
     * COMMIT, EXIT or ConbineActionType.ACCEPT.
     * 
     * When the Buyer sends an ACCEPT, an agreement has been reached and the
     * negotiation has terminated.
     * 
     * "protocol" "dialogue_id" "reply-to_id" "product_id" "ao" "456123"
     * "myBuyer" "laptop"
     */
    ACCEPT,

    /**
     * A 4-element {@link Tuple} ({@link Quartet}) leaving the negotiation with
     * no penalty.
     * 
     * The agents can EXIT from a negotiation at any time, unless it is in a
     * committed state.
     * 
     * "protocol" "dialogue_id" "reply_to_id" "product_id" "ao" "456123"
     * "myBuyer" "laptop"
     */
    EXIT,

    /**
     * A 3-element {@link Tuple} ({@link Triplet}) announcing the arrival of the
     * agent in the market.
     * 
     * A Seller will indicate its arrival so that the MarketBroker can notify
     * Buyers that a new Seller is available to negotiate with.
     * 
     * A Buyer will indicate its arrival so it can be notified by the
     * MarketBroker about existing or new Sellers.
     * 
     * "agent_type" "reply-to_id" "product_id" "buyer" "myBuyer" "laptop"
     */
    ANNOUNCE_ENTRANCE,

    /**
     * A 3-element {@link Tuple} ({@link Triplet}) announcing the exit of the
     * agent from all negotiations (the agent is ready to leave the market).
     * 
     * The MarketBroker will update its registers so that it's not giving Buyers
     * the IDs of Sellers no longer in the system, and also not notifying
     * non-existent Buyers.
     * 
     * The MarketController will request an agent to leave occasionally - when
     * the agent has concluded its negotiations (by sending EXITs), it will
     * announce using EXIT_ALL. The MarketController can then safely remove this
     * agent from the {@link Environment}.
     * 
     * "agent_type" "reply-to_id" "product_id" "buyer" "myBuyer" "laptop"
     */
    EXIT_ALL,

    /**
     * A 2-element {@link Tuple} ({@link Pair}) notifying a buyer about a new
     * seller that it previously did not know about, and passing a universally
     * unique dialogue ID that it should use, if it wants to start a new
     * dialogue with that seller.
     * 
     * "new_seller_id" "dialogue_id" "newSeller" "3248172948723412"
     */
    NOTIFY_ABOUT_NEW_SELLER,

    /**
     * A 1-element {@link Tuple} ({@link Unit}) notifying the
     * {@link MetricsCollector} that the current run has ended, and the label
     * for the combination of variables with which the run was initialised.
     * 
     * The notification that the current run has ended is implicit.
     * 
     * The label is the number of the combination as determined by the {@link Clicker}.
     * 
     */
    NOTIFY_RUN_COMPLETE,

    /**
     * A 1-element {@link Tuple} ({@link Unit}) notifying the recipient about
     * the new demand/supply ratio (number of buyers/number of sellers) in the
     * Market.
     * 
     * The payload is a single double, representing the ratio (b/s).
     */
    NOTIFY_ABOUT_CHANGE_IN_DEMAND_SUPPLY_RATIO,

    /**
     * A 1-element {@link Tuple} ({@link Unit}) notifying the recipient about a
     * change in the number of competitors in the market.
     * 
     * This notification is only sent to MarketAgents of type AgentType.BUYER,
     * and the payload consists of a single integer representing the number of
     * other buyers in the marketplace (i.e. NOT including the recipient of this
     * message).
     */
    NOTIFY_ABOUT_CHANGE_IN_NUMBER_OF_COMPETITORS,
    
    /**
     * A payload-less {@link Action} notifying the recipient (a MarketAgent) that
     * they should leave the marketplace. The action is only sent from the
     * {@link MarketController}, and is used to regulate the market density and
     * buyer to seller ratio. 
     */
    NOTIFY_LEAVE_MARKET,

    /**
     * A 4-element {@link Tuple} ({@link Quartet}) notifying the recipient about
     * the state of action, COLLECT_METRICS, for the specified agent.
     * 
     * The {@link MarketController} will send the Action with a "collectMetrics"
     * payload, and the ID of the agent about which to collect stats.
     * 
     * The {@link MetricsCollector} will respond with the "finished" payload
     * when it's collected the metrics for the specified agent.
     * 
     * The payload consists of the state (whether the action is describing the
     * request to collect metrics, or is notifying that the metrics have been
     * collected), the subject (the ID of the agent concerning which metrics are
     * being collected), a reply-to ID and the parameters used to initialise the
     * agent.
     * 
     */
    COLLECT_METRICS;

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
}
