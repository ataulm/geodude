package uk.ac.rhul.cs.dice.golem.conbine.agent.faratin;

import java.util.List;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.agent.AgentBrain;
import uk.ac.rhul.cs.dice.golem.conbine.action.NegotiationAction;
import uk.ac.rhul.cs.dice.golem.conbine.agent.AgentParameters;

@SuppressWarnings("serial")
public class FaratinRandomAbsoluteTitForTatSeller extends AbstractFaratinWithOpponentHistorySeller {
	private static final int M = 10;
	
	public FaratinRandomAbsoluteTitForTatSeller(AgentBrain brain,
			AgentParameters params, String product) {
		super(brain, params, product);
	}	
	
	@Override
	protected List<Action> decideActionBasedOnOffer(NegotiationAction offer) {
		List<Action> actionsToPerform = super.decideActionBasedOnOffer(offer);
		
		DefaultDialogueStateFratinSellerOfferHistory dialogueState =
				(DefaultDialogueStateFratinSellerOfferHistory) getDialogues().get(offer.getDialogueId());
		
		dialogueState.addOpponentBid(Double.parseDouble(offer.getValue()));
		
		double utilityOpponentOffer = getUtility(Double.parseDouble(offer.getValue()));
		double counterOffer = generateNextOffer(offer.getDialogueId());
		double utilityMyCounterOffer = getUtility(counterOffer);
		
		if (utilityOpponentOffer >= utilityMyCounterOffer) {
			actionsToPerform.addAll(super.acceptOpponentOffer(offer));
		} else {
			actionsToPerform.addAll(super.sendCounterOffer(offer, counterOffer));
		}
		
		return actionsToPerform;
	}
	
	@Override
	protected List<Action> sendCounterOffer(NegotiationAction offer,
			double counterOffer) {
		DefaultDialogueStateFratinSellerOfferHistory dialogueState =
				(DefaultDialogueStateFratinSellerOfferHistory) getDialogues().get(offer.getDialogueId());
		
		dialogueState.updateMyLastBid(counterOffer);
		
		return super.sendCounterOffer(offer, counterOffer);
	}
	
	@Override
	protected double generateNextOffer(String dialogueId) {
		DefaultDialogueStateFratinSellerOfferHistory dialogueState =
				(DefaultDialogueStateFratinSellerOfferHistory) getDialogues().get(dialogueId);
		
		double opponentLastOffer = 0;
		double opponentLastLastOffer = 0;
		double myLastBid = (dialogueState.getMyLastBid() == 0) ? getInitialPrice() : dialogueState.getMyLastBid();
		
		try {
			opponentLastOffer = dialogueState.getMinusNthOpponentBid(0);
		} catch (IndexOutOfBoundsException e) {
			return myLastBid;
		}
			
		try {
			opponentLastLastOffer = dialogueState.getMinusNthOpponentBid(1);
		} catch (IndexOutOfBoundsException e) {
			opponentLastLastOffer = dialogueState.getOldestOpponentBid();
		}
		
		double opponentDelta = opponentLastLastOffer - opponentLastOffer;
		int randomChange = getRandom().nextInt(M + 1);
		
		double counterOffer = myLastBid - opponentDelta - randomChange;
		if (counterOffer < getReservationPrice()) {
			counterOffer = getReservationPrice();
		}
		
		return counterOffer;
	}
}
