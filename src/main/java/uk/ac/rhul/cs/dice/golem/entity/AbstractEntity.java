package uk.ac.rhul.cs.dice.golem.entity;

public class AbstractEntity implements Entity {

    private String id;

    @Override
    public String getId() {
        return id;
    }
}
