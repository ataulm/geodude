package uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure;

import uk.ac.rhul.cs.dice.golem.action.AbstractEffector;
import uk.ac.rhul.cs.dice.golem.agent.AgentBody;
import uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType;

public class MarketBrokerMouth extends AbstractEffector {

    public MarketBrokerMouth(String id, AgentBody context) {
        super(id, context);
        addType(ConbineActionType.NOTIFY_ABOUT_NEW_SELLER.toString());
    }
}
