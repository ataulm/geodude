package uk.ac.rhul.cs.dice.golem.action;

import org.javatuples.Tuple;
import org.javatuples.Unit;

import uk.ac.rhul.cs.dice.golem.agent.AgentBody;
import uk.ac.rhul.cs.dice.golem.container.Environment;

/**
 * Action consists of an action type (String) and payload ({@link Tuple}).
 * 
 * An action can be a "whisper" or a "broadcast" - if a recipient is specified
 * it's a whisper, and only that recipient should be notified of the action.
 * 
 * If it's a broadcast, then all agents in the {@link Environment} that have
 * subscribed to that action type will receive a notification about the action.
 * 
 * @author ataulm
 * @see {@link AgentBody#registerSensor(Sensor, boolean)}
 */
public abstract class Action {
    /**
     * It is intended that common action types are listed here.
     * 
     * @author ataulm
     * 
     */
    public enum ActionType {
        /**
         * A 1-element {@link Tuple} ({@link Unit}) representing a collection of
         * spoken words.
         * 
         * The element is of type String, and represents the collection of
         * words.
         * 
         * order of elements: "words_spoken" example:
         * "These are some words I said"
         */
        SPEAK;

        private static final String PREFIX = "uk.ac.rhul.cs.dice.golem.action.";

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
    }

    /**
     * Type of Action being performed.
     */
    private final String type;

    /**
     * ID of the recipient (null for broadcast messages)
     */
    private final String recipient;

    /**
     * The "value" of the Action.
     * 
     * The content will depend on the application being delivered - it is up to
     * the application developer to decide how to interpret the value of this
     * variable (syntax-wise), given a particular ActionType.
     */
    private final Tuple payload;

    /**
     * Constructs an Action, specifying the type and payload.
     * 
     * @param type
     *            the category of this action
     * @param recipient
     *            the recipient of the message, or null if it's a broadcast
     * @param payload
     *            a collection of objects forming the value of the action
     */
    protected Action(String type, String recipient, Tuple payload) {
        this.type = type;
        this.recipient = recipient;
        this.payload = payload;
    }

    /**
     * Returns the category this Action belongs to.
     * 
     * @return type the category of this action
     */
    public String getActionType() {
        return type;
    }

    /**
     * Returns the value of the payload. Given an ActionType, the type of the
     * object returned may vary between applications.
     * 
     * @return payload the action to perform
     */
    public Tuple getPayload() {
        return payload;
    }

    public String getRecipient() {
        return recipient;
    }

}
