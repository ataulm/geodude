package uk.ac.rhul.cs.dice.golem.conbine.action;

import org.javatuples.Unit;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.conbine.app.Ratio;

/**
 * As per {@link ConbineActionType#NOTIFY_ABOUT_CHANGE_IN_DEMAND_SUPPLY_RATIO}.
 * 
 * @author ataulm
 * 
 */
public class NotifyAboutChangeInDemandSupplyRatioAction extends Action {
	public static final int NEW_RATIO = 0;
	
    public static class Builder {
        private String recipientId;
        private Ratio newRatio;

        public NotifyAboutChangeInDemandSupplyRatioAction build() {
            if (newRatio == null) {
                throw new IllegalStateException("The new ratio must be set.");
            }

            if (recipientId == null || recipientId.length() == 0) {
                throw new IllegalStateException(
                        "Recipient must be set for notify actions.");
            }

            Unit<Ratio> payload = new Unit<>(newRatio);

            return new NotifyAboutChangeInDemandSupplyRatioAction(recipientId,
                    payload);
        }

        public Builder setRatio(Ratio ratio) {
            newRatio = ratio;
            return this;
        }

        public Builder setRecipient(String recipientId) {
            this.recipientId = recipientId;
            return this;
        }
    }

    protected NotifyAboutChangeInDemandSupplyRatioAction(String recipient,
            Unit<Ratio> payload) {
        super(ConbineActionType.NOTIFY_ABOUT_CHANGE_IN_DEMAND_SUPPLY_RATIO
                .toString(), recipient, payload);
    }

    public Ratio getRatio() {
        return (Ratio) getPayload().getValue(NEW_RATIO);
    }

    @Override
    public String toString() {
        return getActionType() + "(" + getRatio().value() + ")";
    }
}
