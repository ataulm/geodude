package uk.ac.rhul.cs.dice.golem.conbine.action;

import org.javatuples.Tuple;

import uk.ac.rhul.cs.dice.golem.action.Action;

/**
 * As per {@link ConbineActionType#NOTIFY_LEAVE_MARKET}.
 * 
 * @author ataulm
 * 
 */
public class NotifyLeaveMarketAction extends Action {    
    public static class Builder {
        private String recipientId;

        public NotifyLeaveMarketAction build() {
        	if (recipientId == null || recipientId.length() == 0) {
                throw new IllegalStateException(
                        "Recipient must be set for notify actions.");
            }

            return new NotifyLeaveMarketAction(recipientId, null);
        }

        public Builder setRecipient(String recipientId) {
            this.recipientId = recipientId;
            return this;
        }
    }

    protected NotifyLeaveMarketAction(String recipient, Tuple payload) {
        super(ConbineActionType.COLLECT_METRICS.toString(), recipient, payload);
    }

    @Override
    public String toString() {
        return getActionType() + "()";
    }
}
