package uk.ac.rhul.cs.dice.golem.conbine.app;

import java.util.Random;

public abstract class ValueBucket {
    private final String id;
    private final Random random;

    protected ValueBucket(String id) {
        this.id = id;
        random = new Random();
    }

    /**
     * Get identifier for this bucket.
     * 
     * @return id id of this bucket
     */
    public String getId() {
        return id;
    }

    /**
     * As {@link Random#nextDouble()}
     * 
     * @return random a random double between 0 and 1.0
     */
    protected double nextDouble() {
        return random.nextDouble();
    }

    /**
     * As {@link Random#nextInt(int)}
     * 
     * @param maxExclusive
     *            the upper bound, exclusive
     * @return random random int between 0 and (maxExclusive - 1)
     */
    protected int nextInt(int maxExclusive) {
        return random.nextInt(maxExclusive);
    }

    /**
     * Returns a value from the bucket.
     * 
     * @return value the value returned
     */
    public abstract Object pickValue();
}
