package uk.ac.rhul.cs.dice.golem.agent;

import java.util.concurrent.ConcurrentHashMap;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.action.Effector;
import uk.ac.rhul.cs.dice.golem.action.Sensor;
import uk.ac.rhul.cs.dice.golem.container.Environment;
import uk.ac.rhul.cs.dice.golem.entity.Entity;

public interface AgentBody extends Entity {
    /**
     * Passes the {@link Action} to an appropriate {@link Effector} if it has
     * one.
     * 
     * Returns true if the AgentBody has an Effector that can perform this
     * Action, otherwise false. Makes no assertion on the success of the
     * attempted action.
     * 
     * @param action
     *            the Action to attempt
     * @return isCapable whether or not the AgentBody has an Effector that can
     *         perform this Action
     */
    public boolean act(Action action);

    /**
     * Returns all the {@link Effector} objects registered with this AgentBody.
     * 
     * @return effectors list of Effector objects registered with the AgentBody
     */
    public ConcurrentHashMap<String, Effector> getAllEffectors();

    /**
     * Returns all the {@link Sensor} objects registered with this AgentBody.
     * 
     * @return effectors list of Sensor objects registered with the AgentBody
     */
    public ConcurrentHashMap<String, Sensor> getAllSensors();

    /**
     * Returns the {@link AgentBrain} associated with this AgentBody.
     * 
     * @return brain the associated brain
     */
    public AgentBrain getBrain();

    /**
     * Returns the effector with the specified id, or null, if it's not found.
     * 
     * @param effectorId
     *            id of the target {@link Effector} object
     * @return the effector with the specified id
     */
    public Effector getEffector(String effectorId);

    /**
     * Returns a reference to the {@link Environment} the AgentBody is
     * immediately located within.
     * 
     * @return environment the GolemContainer containing the AgentBody
     */
    public Environment getEnvironment();

    /**
     * Returns the sensor with the specified id, or null, if it's not found.
     * 
     * @param sensorId
     *            id of the target {@link Sensor} object
     * @return the sensor with the specified id
     */
    public Sensor getSensor(String sensorId);

    /**
     * Passes the {@link Action} to an appropriate {@link Sensor} if it has one.
     * 
     * Returns true if the AgentBody has a Sensor capable of interpreting the
     * Action.
     * 
     * @param action
     *            the Action to attempt
     * @return isCapable whether or not the AgentBody has an Sensor that can
     *         perform this Action
     */
    public boolean perceive(Action action);

    /**
     * Adds an {@link Effector} object to the collection of effectors registered
     * with the AgentBody.
     * 
     * @param effector
     *            the effector to register
     */
    public void registerEffector(Effector effector);

    /**
     * Adds an {@link Sensor} object to the collection of sensors registered
     * with the AgentBody.
     * 
     * @param sensor
     *            the sensor to register
     * @param subscribeToBroadcasts
     *            true if this Sensor should subscribe to broadcasts
     * @see Action
     */
    public void registerSensor(Sensor sensor, boolean subscribeToBroadcasts);

    /**
     * Sets the associated {@link AgentBrain} with this AgentBody.
     * 
     * @param brain
     *            the associated brain
     */
    public void setBrain(AgentBrain brain);

    /**
     * Sets the reference to the {@link Environment} the AgentBody is
     * immediately located within.
     * 
     * @param environment
     *            the Environment the AgentBody is immediately located within
     */
    public void setEnvironment(Environment environment);
}
