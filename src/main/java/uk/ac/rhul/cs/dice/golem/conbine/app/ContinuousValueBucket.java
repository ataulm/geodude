package uk.ac.rhul.cs.dice.golem.conbine.app;

import org.javatuples.Pair;

/**
 * Allows the user to define a range with a fixed minimum and maximum.
 * 
 * Specify the minimum and maximum values for the bucket; {@link #pickValue()}
 * will return a value in this range as a {@link Number} - the instance will be
 * of type {@link Integer} or {@link Double} depending on the primitives used to
 * construct this bucket.
 * 
 * @author ataulm
 * 
 */
public class ContinuousValueBucket extends ValueBucket {
    private final Pair<?, ?> range;

    public ContinuousValueBucket(String id, double min, double max) {
        super(id);

        if (min > max) {
            double temp = max;
            max = min;
            min = temp;
        }

        range = new Pair<>(min, max);
    }

    public ContinuousValueBucket(String id, int min, int max) {
        super(id);

        if (min > max) {
            int temp = max;
            max = min;
            min = temp;
        }

        range = new Pair<>(min, max);
    }

    /**
     * Returns a random value between the range specified by this ValueBucket.
     * 
     * @return value the random value
     */
    @Override
    public Number pickValue() {
        if (range.getValue0() instanceof Integer) {
            Integer min = (Integer) range.getValue0();
            Integer max = (Integer) range.getValue1();
            return min + super.nextInt(max - min + 1);
        } else {
            Double min = (Double) range.getValue0();
            Double max = (Double) range.getValue1();
            return min + (max - min) * super.nextDouble();
        }
    }
}