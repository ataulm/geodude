package uk.ac.rhul.cs.dice.golem.conbine.app;

import org.javatuples.Pair;

/**
 * Represents a fraction that is the quotient of A divided by B (A:B).
 * 
 * @author ataulm
 * 
 */
public class Ratio {
    private final Pair<Integer, Integer> ratio;

    public Ratio(Integer antecedent, Integer consequent) {
        ratio = new Pair<>(antecedent, consequent);
    }

    /**
     * Returns term A of A:B
     * 
     * @return value the antecedent
     */
    public int getAntecedent() {
        return ratio.getValue0();
    }

    /**
     * Returns term B of A:B
     * 
     * @return value the consequent of the ratio
     */
    public int getConsequent() {
        return ratio.getValue1();
    }

    /**
     * Gets the value of the quotient (A/B).
     * 
     * @return value the value of the ratio as a double (A/B)
     */
    public double value() {
        return ratio.getValue0() / ratio.getValue1();
    }
}
