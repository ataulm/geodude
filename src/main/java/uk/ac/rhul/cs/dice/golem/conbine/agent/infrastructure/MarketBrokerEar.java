package uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure;

import uk.ac.rhul.cs.dice.golem.action.AbstractSensor;
import uk.ac.rhul.cs.dice.golem.agent.AgentBody;
import uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType;

/**
 * The sensor for the MarketBroker only interprets two action types, the
 * announcements when a market agent enters and leaves the market.
 * 
 * @author ataulm 
 */
public class MarketBrokerEar extends AbstractSensor {
    public MarketBrokerEar(String id, AgentBody context) {
        super(id, context);
        addType(ConbineActionType.ANNOUNCE_ENTRANCE.toString());
        addType(ConbineActionType.EXIT_ALL.toString());
        addType(ConbineActionType.NOTIFY_RUN_COMPLETE.toString());
    }
}
