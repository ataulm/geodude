package uk.ac.rhul.cs.dice.golem.action;

public class DefaultPercept implements Percept {

    private final Action action;
    private final long timestamp;

    public DefaultPercept(Action action, long timestamp) {
        this.action = action;
        this.timestamp = timestamp;
    }

    @Override
    public Action getPerceptContent() {
        return action;
    }

    @Override
    public long getTimeStamp() {
        return timestamp;
    }

}
