package uk.ac.rhul.cs.dice.golem.conbine.agent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.action.Percept;
import uk.ac.rhul.cs.dice.golem.agent.AbstractAgentMind;
import uk.ac.rhul.cs.dice.golem.agent.AgentBrain;
import uk.ac.rhul.cs.dice.golem.conbine.action.AnnounceEntranceAction;
import uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType;
import uk.ac.rhul.cs.dice.golem.conbine.action.ExitAllAction;
import uk.ac.rhul.cs.dice.golem.conbine.action.NegotiationAction;
import uk.ac.rhul.cs.dice.golem.conbine.action.NotifyLeaveMarketAction;
import uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure.MarketController;
import uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure.MetricsCollector;

@SuppressWarnings("serial")
public abstract class AbstractMarketAgent extends AbstractAgentMind implements MarketAgent {
	private boolean announcedArrival;
	private boolean trading;
	private final AgentType type;
	private final int initialPrice;
	private final int reservationPrice;
	private final int deadline;
	private final String product;
	private final Map<String, DialogueState> dialogues;
	private final Random random;

    protected AbstractMarketAgent(AgentBrain brain, AgentParameters params, String product, AgentType type) {
		super(brain);
		this.type = type;
		
		if (type == AgentType.BUYER) {
			initialPrice = params.get(AgentParameters.BUYER_INITIAL);
			reservationPrice = params.get(AgentParameters.BUYER_RESERVATION);
		} else {
			initialPrice = params.get(AgentParameters.SELLER_INITIAL);
			reservationPrice = params.get(AgentParameters.SELLER_RESERVATION);
		}
		
		this.product = product;
		deadline = params.get(AgentParameters.DEADLINE) * 1000;
		dialogues = new HashMap<>();
		random = new Random();
	}
	
	protected AbstractMarketAgent(AgentBrain brain, AgentParameters params, String product, AgentType type, Map<String, DialogueState> dialogues) {
		super(brain);
		this.type = type;
		
		if (type == AgentType.BUYER) {
			initialPrice = params.get(AgentParameters.BUYER_INITIAL);
			reservationPrice = params.get(AgentParameters.BUYER_RESERVATION);
		} else {
			initialPrice = params.get(AgentParameters.SELLER_INITIAL);
			reservationPrice = params.get(AgentParameters.SELLER_RESERVATION);
		}
		
		this.product = product;
		deadline = params.get(AgentParameters.DEADLINE);
		this.dialogues = dialogues;
		random = new Random();
		
		setLogLevel(MICRO);
	}
	
	protected final Random getRandom() {
	    return random;
	}
	
	protected final int getInitialPrice() {
		return initialPrice;
	}
	
	protected final int getReservationPrice() {
		return reservationPrice;
	}
	
	protected final int getDeadline() {
		return deadline;
	}
	
	protected final String getProduct() {
		return product;
	}
	
	protected final Map<String, DialogueState> getDialogues() {
		return dialogues;
	}
	
	/**
	 * Gets the AgentType of this agent.
	 * 
	 * This is only public so that {@link MetricsCollector} can see it but it ought to be protected.
	 * Instead, {@link MarketController} should tell the MetricsCollector what type the agent is.
	 * Or, as {@link MetricsCollector} only deals with BUYER agents (in ConBiNE) it could just assume that.
	 * 
	 * TODO: Set {@link MetricsCollector} so that it's for BUYERS only.
	 */
	@Override
    public final AgentType getType() {
        return type;
    }
	
	@Override
	public List<Action> executeStep() throws Exception {
		List<Action> actionsToPerform = new ArrayList<>();
	    List<Percept> percepts = getBrain().getAllPerceptions();
	    
	    if (!announcedArrival) {
	        actionsToPerform.add(new AnnounceEntranceAction.Builder()
	                .setAgentId(getBrain().getAgentId())
	                .setAgentType(type.toString()).setProductId(product)
	                .build());
	
	        announcedArrival = true;
	        onArrivalToMarket();
	        return actionsToPerform;
	    }
	
	    // Create an Action response for each message
	    if (trading) {
	    	// even if no percepts, the agent should be given chance to choose action
	    	if (percepts.size() == 0) {
	    		actionsToPerform.addAll(decideAction(null));
	    	} else {
	    		for (Percept percept : percepts) {
		    		List<Action> decidedAction = decideAction(percept
			                .getPerceptContent());
			        actionsToPerform.addAll(decidedAction);	
		    	}
		    	percepts.clear();	
	    	}
	    }
	    
	    int actionCount = 1;
	    for (Action action:actionsToPerform) {
	    	log(getBrain().getAgentId() + " to perform: " +
	    			actionCount++ + ". " + action.toString());
	    }
	    return actionsToPerform;
	}
	
	/**
	 * Performs initialisation actions on arriving in the market.
	 * 
	 * Called just before the {@link MarketAgent} has announces arrival.
	 * The default implementation sets the agent's trading flag to true.
	 * 
	 */
	protected void onArrivalToMarket() {
		trading = true;
	}
	
	/**
	 * Sets the trading flag.
	 * 
	 * The {@link MarketAgent} only checks {@link Percept Percepts} while it is trading.
	 * 
	 * @param trading true if the {@link MarketAgent} is trading
	 */
	protected final void setTrading(boolean trading) {
		this.trading = trading;
	}
	
	/**
	 * Decide on the next course of action.
	 * 
	 * Checks action's type against list of action types it expects, and constructs response.
	 * Return a {@link List} of {@link Action Actions} as a response to the given {@link Action}.
	 * 
	 * Override this to add further action types, remembering to add these actions too:
	 * 		actionsToPerform.addAll(super.decideAction(action));
	 * 
	 * @param action  the perceived {@link Action}
	 * @return actionsToPerform  the set of {@link Action Actions} to perform
	 */
	protected List<Action> decideAction(Action action) {
		if (!trading) {
            return new ArrayList<>();
        }

        if (action != null) {
			if (action instanceof NotifyLeaveMarketAction) {
                log("Received notify leave market, exiting all.");
                return exitAll();
			}
				
			if (action.getActionType().equals(ConbineActionType.OFFER.toString())) {
				return decideActionBasedOnOffer((NegotiationAction) action);
			}

			if (action.getActionType().equals(ConbineActionType.DECOMMIT.toString())) {
				return decideActionBasedOnDecommit((NegotiationAction) action);			
			}
			
			if (action.getActionType().equals(ConbineActionType.ACCEPT.toString())) {
				return decideActionBasedOnAccept((NegotiationAction) action);			
			}
			
			if (action.getActionType().equals(ConbineActionType.EXIT.toString())) {
				return decideActionBasedOnExit((NegotiationAction) action);			
			}	
		}
		
		// return an empty list
		return new ArrayList<>();
	}
	
	/**
	 * Decide how to respond when the opponent makes an offer.
	 * 
	 * @param offer
	 * @return
	 */
	protected abstract List<Action> decideActionBasedOnOffer(NegotiationAction offer);
	
	/**
	 * Decide how to respond when the opponent decommits.
	 * 
	 * Default implementation removes the dialogue, without sending a response.
	 * 
	 * @param decommit
	 * @return
	 */
	protected List<Action> decideActionBasedOnDecommit(NegotiationAction decommit) {
		logMicro(getBrain().getAgentId() + " received decommit. Negotiation unsuccessful.");
		dialogues.remove(decommit.getDialogueId());
		return new ArrayList<>();
	}
	
	protected abstract double generateNextOffer(String dialogueId);
	
	/**
     * Returns the normalised time in the current negotiation for this agent.
     * 
     * The normalised time is a value from 0 to 1, where 0 represents the start
     * of the negotiation and 1 represents the maximum time the agent is willing
     * to spend on this negotiation (its deadline).
     * 
     * @return normalisedTime the normalised time in the current negotiation
     */
    protected double getNormalisedTime(long startTime) {
    	double timePassed = (double) (System.currentTimeMillis() - startTime);
        double normalised = timePassed / deadline;


    	log("startTime: " + startTime);
    	log("timePassed: " + timePassed);
    	log("deadline: " + deadline);
    	log("normalised: " + normalised);
    	
        return normalised;
    }
	
	
	/**
	 * Decide how to respond when the opponent accepts your offer.
	 * 
	 * @param accept
	 * @return
	 */
	protected abstract List<Action> decideActionBasedOnAccept(NegotiationAction accept);
	
	/**
	 * Decide how to respond when the opponent exits from the dialogue.
	 * 
	 * Default implementation removes the dialogue, without sending a response.
	 * 
	 * @param exit
	 * @return
	 */
	protected List<Action> decideActionBasedOnExit(NegotiationAction exit) {
		logMicro(getBrain().getAgentId() + " received exit. Negotiation unsuccessful.");
		dialogues.remove(exit.getDialogueId());
		return new ArrayList<>();
	}
	
	/**
	 * Send counter offer.
	 * 
	 * Produces an offer {@link Action} to send back to the opponent.
	 * 
	 * @param offer
	 * @return
	 */
	protected List<Action> sendCounterOffer(NegotiationAction offer, double counterOffer) {
		List<Action> actionsToPerform = new ArrayList<>();
		
		actionsToPerform.add(new NegotiationAction.Builder()
                .setType(ConbineActionType.OFFER)
                .setDialogueId(offer.getDialogueId())
                .setProtocol(offer.getProtocol())
                .setRecipient(offer.getReplyToId())
                .setReplyToId(getBrain().getAgentId())
                .setProductId(offer.getProductId())
                .setValue("" + counterOffer)
                .build());
		
		return actionsToPerform;
	}
	
	/**
	 * Decommit from the specified dialogue.
	 * 
	 * @param dialogueId
	 * @return
	 */
	protected List<Action> decommitFromDialogue(String dialogueId) {
		List<Action> actionsToPerform = new ArrayList<>();
		DialogueState dialogue = getDialogues().get(dialogueId);
		
		// only decommit from committed dialogues
		if (!dialogue.isCommitted()) {
			return actionsToPerform;
		}
		
		
		actionsToPerform.add(new NegotiationAction.Builder()
				.setType(ConbineActionType.DECOMMIT)
				.setDialogueId(dialogueId)
				.setProtocol(dialogue.getProtocol())
	            .setRecipient(dialogue.getOpponent())
	            .setReplyToId(getBrain().getAgentId())
	            .setProductId(dialogue.getProduct())
	            .build());
		
		getDialogues().remove(dialogueId);
		
		return actionsToPerform;
	}
	
	/**
	 * Accept the opponent's offer.
	 * 
	 * Produces an accept {@link Action} to send back to the opponent.
	 * Subclasses should override and extend this to alter functionality based on their {@link AgentType}.
	 * 
	 * @param offer
	 * @return
	 */
	protected List<Action> acceptOpponentOffer(NegotiationAction offer) {
		List<Action> actionsToPerform = new ArrayList<>();
		
		actionsToPerform.add(new NegotiationAction.Builder()
                .setType(ConbineActionType.ACCEPT)
                .setDialogueId(offer.getDialogueId())
                .setProtocol(offer.getProtocol())
                .setRecipient(offer.getReplyToId())
                .setReplyToId(getBrain().getAgentId())
                .setProductId(offer.getProductId())
                .build());
		
		return actionsToPerform;
	}
	
	/**
	 * Accept a previously made offer (that is still active).
	 * 
	 * @param dialogueId
	 * @return
	 */
	protected List<Action> acceptOpponentOffer(String dialogueId) {
		List<Action> actionsToPerform = new ArrayList<>();
		DialogueState dialogue = getDialogues().get(dialogueId);
		
		actionsToPerform.add(new NegotiationAction.Builder()
				.setType(ConbineActionType.ACCEPT)
				.setDialogueId(dialogueId)
				.setProtocol(dialogue.getProtocol())
	            .setRecipient(dialogue.getOpponent())
	            .setReplyToId(getBrain().getAgentId())
	            .setProductId(dialogue.getProduct())
	            .build());

		return actionsToPerform;
	}
	
	protected abstract DialogueState makeNewDialogueState(NegotiationAction offer);
	
	protected List<Action> exitFromDialogue(String dialogueId) {
		List<Action> actionsToPerform = new ArrayList<>();
		DialogueState dialogue = dialogues.get(dialogueId);
		
		if (dialogue.isCommitted()) {
            actionsToPerform.addAll(decommitFromDialogue(dialogue.getId()));

		} else {
			actionsToPerform.add(new NegotiationAction.Builder()
					.setType(ConbineActionType.EXIT)
					.setDialogueId(dialogue.getId())
		            .setProtocol(dialogue.getProtocol())
		            .setRecipient(dialogue.getOpponent())
		            .setProductId(dialogue.getProduct())
		            .setReplyToId(getBrain().getAgentId())
					.build());

            dialogues.remove(dialogueId);
		}

		return actionsToPerform;
	}
	
	protected List<Action> exitAll() {
		List<Action> actionsToPerform = new ArrayList<>();
		log(getBrain().getAgentId() + " | ip: " + getInitialPrice() + ", rp: " + getReservationPrice() + ", deadline: " + getDeadline());

        List<String> dialogueIds = new ArrayList<>(dialogues.size());

        for (String id : dialogues.keySet()) {
            dialogueIds.add(id);
        }

		// Exit from all dialogues
		for (String dialogueId : dialogueIds) {
            actionsToPerform.addAll(exitFromDialogue(dialogueId));
		}

		logMicro("Is dialogues clear? (It should be true): " + (dialogues.size() == 0));
		trading = false;
		
		// Notify MarketBroker that we're no longer negotiating
		// TODO: shouldn't _really_ have to say what type of agent this is, nor product
		actionsToPerform.add(new ExitAllAction.Builder()
		        .setAgentId(getBrain().getAgentId())
		        .setAgentType(type.toString())
		        .setProductId(product)
		        .build());
		
		return actionsToPerform;
	}
	
	/**
	 * Calculate the utility of a given offer.
	 * 
	 * @param offer
	 * @return
	 */
	protected abstract double getUtility(double offer);

    
   

}
