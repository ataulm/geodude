package uk.ac.rhul.cs.dice.golem.agent;

import java.io.Serializable;

import uk.ac.rhul.cs.dice.golem.util.Logger;

@SuppressWarnings("serial")
public abstract class AbstractAgentMind extends Logger implements AgentMind, Serializable {

    private AgentBrain brain;

    protected AbstractAgentMind(AgentBrain brain) {
        this.brain = brain;
    }

    /**
     * Returns the brain associated with this {@link AgentMind}.
     * 
     * @return brain the {@link AgentBrain} associated with this AgentMind
     */
    @Override
    public AgentBrain getBrain() {
        return brain;
    }

    /**
     * Sets a reference to the brain associated with this {@link AgentMind}.
     * 
     * @param brain
     *            the {@link AgentBrain} associated with this AgentMind
     */
    protected void setBrain(AgentBrain brain) {
        this.brain = brain;
    }

    /**
     * Note, this precludes log from being called before executeStep(); if it's called in the constructor, it'll cause
     * an exception, as the mind isn't guaranteed to have a reference to the brain at this point.
     *
     * @param out  the message to log
     */
    @Override
    protected void log(String out) {
        if (getLogLevel() >= Logger.STANDARD) {
            super.log(getClass().getSimpleName() + " (" + getBrain().getAgentId() + "): ", out);
        }
    }
}
