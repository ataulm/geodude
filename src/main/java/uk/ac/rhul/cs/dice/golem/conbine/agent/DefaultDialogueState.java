package uk.ac.rhul.cs.dice.golem.conbine.agent;

/**
 * DialogueState maintains the state of an individual dialogue.
 * 
 */
public class DefaultDialogueState implements DialogueState {
	private final String id;
	private final String protocol;
	private final String opponent;
	private final String product;
	private boolean committed;

	protected DefaultDialogueState(String id, String protocol, String opponent, String product) {
        this.id = id;
        this.protocol = protocol;
        this.product = product;
        this.opponent = opponent;
    }
    
    /**
     * Get dialogue ID.
     * 
     * @return id the unique ID for this dialogue
     */
    @Override
    public final String getId() {
    	return id;
    }
    
    /**
     * Get opponent's ID.
     * 
     * @return opponent the ID of the opponent agent
     */
    @Override
    public final String getOpponent() {
    	return opponent;
    }
    
    /**
     * Get product ID that this dialogue concerns.
     * 
     * @return product the name or ID of the product
     */
    @Override
    public final String getProduct() {
    	return product;
    }
    
    /**
     * Get protocol that this dialogue is operating under.
     * 
     * @return protocol the name of the negotiation protocol
     */
    @Override
    public final String getProtocol() {
    	return protocol;
    }
    
    /**
     * Get committed status.
     * 
     * Returns true if the buyer has committed to the last offer sent in this dialogue.
     * 
     * @return committed  the flag describing whether the buyer has committed or not
     */
    @Override
    public boolean isCommitted() {
    	return committed;
    }
    
    /**
     * Changes the status of the dialogue to committed.
     */
    @Override
    public void setCommitted() {
    	committed = true;
    }
}
