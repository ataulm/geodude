package uk.ac.rhul.cs.dice.golem.util;

public class PathAlias {

    private String path;
    private String alias;

    /**
     * 
     * @param path
     *            Full path to the file
     * @param alias
     */
    public PathAlias(String path, String alias) {
        this.path = path;
        this.alias = alias;
    }

    @Override
    public boolean equals(Object e) {
        if (e == null)
            return false;
        if (e instanceof PathAlias) {
            return (((PathAlias) e).getPath().equalsIgnoreCase(path) && ((PathAlias) e)
                    .getAlias().equalsIgnoreCase(alias));
        }
        return false;
    }

    public String getAlias() {
        return alias;
    }

    public String getPath() {
        return path;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Returns the alias to display
     */
    @Override
    public String toString() {
        return getAlias();
    }

}
