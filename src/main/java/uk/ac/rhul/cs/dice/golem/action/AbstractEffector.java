package uk.ac.rhul.cs.dice.golem.action;

import java.util.ArrayList;

import uk.ac.rhul.cs.dice.golem.agent.AgentBody;

public abstract class AbstractEffector implements Effector {
    private final ArrayList<String> types = new ArrayList<>();
    private String id;
    private AgentBody context;

    protected AbstractEffector(String id, AgentBody context) {
        this.context = context;
        this.id = id + "_" + context.getId();
    }

    @Override
    public boolean act(Action action) {
        getBody().getEnvironment().attempt(getBody(), action);
        return true;
    }

    /**
     * Adds an action type that this {@link Effector} can produce
     * 
     * @param type
     *            the action type to add
     */
    protected void addType(String type) {
        if (!types.contains(type))
            types.add(type);
    }

    @Override
    public ArrayList<String> getActionTypes() {
        return types;
    }

    @Override
    public AgentBody getBody() {
        return context;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public boolean handlesType(String type) {
        return types.contains(type);
    }

    /**
     * Sets the context for this effector.
     * 
     * @param context
     *            the {@link AgentBody} this effector is attached to
     */
    protected void setBody(AgentBody context) {
        this.context = context;
    }

    /**
     * Sets the identifier for this effector.
     * 
     * @param id
     *            the value for the id to set
     */
    protected void setId(String id) {
        this.id = id;
    }
}
