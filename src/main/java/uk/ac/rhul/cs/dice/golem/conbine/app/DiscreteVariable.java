package uk.ac.rhul.cs.dice.golem.conbine.app;

/**
 * Represents a variable where the value must be one of a predetermined set.
 * 
 * @author ataulm
 * 
 */
public class DiscreteVariable extends ConbineVariable {
    public DiscreteVariable(String id) {
        super(id);
    }

    /**
     * As {@link ConbineVariable#addBucket(ValueBucket)} but forces
     * {@link DiscreteValueBucket}.
     * 
     * @param bucket
     *            a {@link DiscreteValueBucket} with a set of allowed values
     */
    public void addBucket(DiscreteValueBucket bucket) {
        super.addBucket(bucket);
    }
}