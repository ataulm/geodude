package uk.ac.rhul.cs.dice.golem.conbine.app;

public final class SimulatedUserInput implements ConbineUserInput {
    private static final DiscreteVariable marketChangeTime;
    static {
        marketChangeTime = new DiscreteVariable("market-change-time");
        marketChangeTime.addBucket(new DiscreteValueBucket("quick", 2));
        marketChangeTime.addBucket(new DiscreteValueBucket("average", 5));
        marketChangeTime.addBucket(new DiscreteValueBucket("slow", 10));
    }

    private static final DiscreteVariable marketChangeRateDensity;
    static {
        marketChangeRateDensity = new DiscreteVariable(
                "change-rate-market-density");
        marketChangeRateDensity.addBucket(new DiscreteValueBucket("quick", 30,
                40, 50));
        marketChangeRateDensity.addBucket(new DiscreteValueBucket("average",
                18, 23, 28));
        marketChangeRateDensity.addBucket(new DiscreteValueBucket("slow", 8,
                10, 12));
    }

    private static final DiscreteVariable marketChangeRateRatio;
    static {
        marketChangeRateRatio = new DiscreteVariable("change-rate-ratio");
        marketChangeRateRatio.addBucket(new DiscreteValueBucket("high",
                new Ratio(10, 1), new Ratio(1, 1), new Ratio(1, 10)));
        marketChangeRateRatio.addBucket(new DiscreteValueBucket("average",
                new Ratio(5, 1), new Ratio(1, 1), new Ratio(1, 5)));
        marketChangeRateRatio.addBucket(new DiscreteValueBucket("low",
                new Ratio(2, 1), new Ratio(1, 1), new Ratio(1, 2)));
    }

    private static final ContinuousVariable deadline;
    static {
        deadline = new ContinuousVariable("deadline");
        deadline.addBucket(new ContinuousValueBucket("short", 30, 90));
        deadline.addBucket(new ContinuousValueBucket("average", 91, 150));
        deadline.addBucket(new ContinuousValueBucket("long", 151, 210));
    }

    private static final ContinuousVariable buyerInitial;
    static {
        buyerInitial = new ContinuousVariable("buyer-initial");
        buyerInitial.addBucket(new ContinuousValueBucket("average", 300, 350));
    }

    private static final ContinuousVariable sellerInitial;
    static {
        sellerInitial = new ContinuousVariable("seller-initial");
        sellerInitial
                .addBucket(new ContinuousValueBucket("average", 800, 750));
    }

    private static final ContinuousVariable buyerReservation;
    static {
        buyerReservation = new ContinuousVariable("buyer-res");
        buyerReservation.addBucket(new ContinuousValueBucket("average", 500, 550));
    }

    private static final ContinuousVariable sellerReservation;
    static {
        sellerReservation = new ContinuousVariable("seller-res");
        sellerReservation.addBucket(new ContinuousValueBucket("average", 450, 500));
    }

    private static final String[] selectedBuyers = new String[] { "SimpleBuyer", "WilliamsIAmHagglerBuyer", "Alrayes3.pl" };

    private static final String[] buyerTypePool = new String[] { "SimpleBuyer", "FaratinTimeDependentLinearBuyer", "FaratinTimeDependentConcederBuyer", "FaratinTimeDependentBoulwareBuyer", "FaratinResourceDependentBuyer", "FaratinResourceTimeDependentBuyer", "FaratinRelativeTitForTatBuyer", "FaratinRandomAbsoluteTitForTatBuyer", "FaratinAverageTitForTatBuyer"};

    private static final String[] sellerTypePool = new String[] { "SimpleSeller", "FaratinTimeDependentLinearSeller", "FaratinTimeDependentConcederSeller", "FaratinTimeDependentBoulwareSeller", "FaratinResourceDependentSeller", "FaratinResourceTimeDependentSeller", "FaratinRelativeTitForTatSeller", "FaratinRandomAbsoluteTitForTatSeller", "FaratinAverageTitForTatSeller" };

    private static final String experimentLabel = "MultipleConBiNeMarket";
    
    private static final int runsPerCombo = 100;

    @Override
    public ContinuousVariable getBuyerInitial() {
        return buyerInitial;
    }

    @Override
    public ContinuousVariable getBuyerReservation() {
        return buyerReservation;
    }

    @Override
    public String[] getBuyerTypePool() {
        return buyerTypePool;
    }

    @Override
    public ContinuousVariable getDeadline() {
        return deadline;
    }

    @Override
    public DiscreteVariable getMarketChangeRateDensity() {
        return marketChangeRateDensity;
    }

    @Override
    public DiscreteVariable getMarketChangeRateRatio() {
        return marketChangeRateRatio;
    }

    @Override
    public DiscreteVariable getMarketChangeTime() {
        return marketChangeTime;
    }

    @Override
    public String[] getSelectedBuyers() {
        return selectedBuyers;
    }

    @Override
    public ContinuousVariable getSellerInitial() {
        return sellerInitial;
    }

    @Override
    public ContinuousVariable getSellerReservation() {
        return sellerReservation;
    }

    @Override
    public String[] getSellerTypePool() {
        return sellerTypePool;
    }

    @Override
    public String getExperimentLabel() {
        return experimentLabel;
    }
    
    @Override
    public int getRunsPerCombination() {
        return runsPerCombo;
    }

}
