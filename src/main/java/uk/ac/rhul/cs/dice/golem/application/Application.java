package uk.ac.rhul.cs.dice.golem.application;

import javax.swing.JTabbedPane;

import uk.ac.rhul.cs.dice.golem.gui.GolemGUI;

/**
 * Interface for Application classes.
 * 
 * All applications wishing to run on GOLEM should implement this class.
 * 
 * @author ataulm
 * 
 */
public interface Application {
    /**
     * Returned from {@link Application#createAppDir()} if the application
     * directory was successfully created.
     */
    public static final int APP_DIR_CREATED = 0;

    /**
     * Returned from {@link Application#createAppDir()} if the application
     * directory was not created, because it failed for some reason.
     */
    public static final int APP_DIR_NOT_CREATED = 1;

    /**
     * Returned from {@link Application#createAppDir()} if the application
     * directory was not created because it already exists.
     */
    public static final int APP_DIR_EXISTS = 2;

    /**
     * Creates application specific directory in the correct folder in the GOLEM
     * hierarchy.
     */
    public int createAppDir();

    /**
     * Returns the absolute path to this application's data directory
     * 
     * @return
     */
    public String getAppDir();

    /**
     * Returns a reference to the JTabbedPane which contains the tabs of running
     * Applications
     * 
     * @return
     */
    public JTabbedPane getDesktopManager();

    /**
     * Returns a reference to the instance of the container GolemGUI
     * 
     * @return
     */
    public GolemGUI getGolemGui();

    /**
     * Returns the human-readable name for this application
     * 
     * @return
     */
    public String getName();

    /**
     * Main entry point for this application.
     */
    public void init(GolemGUI golemGui);

    /**
     * Creates and adds an instance of this application as a tab to the GOLEM
     * window.
     */
    public void launch();

    /**
     * Sets the reference to the instance of the container GolemGUI
     * 
     * @param golemGui
     */
    public void setGolemGui(GolemGUI golemGui);
}