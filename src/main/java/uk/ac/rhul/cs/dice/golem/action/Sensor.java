package uk.ac.rhul.cs.dice.golem.action;

import java.util.ArrayList;

import uk.ac.rhul.cs.dice.golem.agent.AgentBody;
import uk.ac.rhul.cs.dice.golem.entity.Entity;

public interface Sensor extends Entity {

    /**
     * Return a list of action types that this sensor can handle.
     * 
     * @return
     */
    public ArrayList<String> getActionTypes();

    /**
     * Return a reference to the {@link AgentBody} this sensor is attached to.
     * 
     * @return body the associated AgentBody
     */
    public AgentBody getBody();

    /**
     * Gets the oldest {@link Percept} from the queue maintained by the sensor
     * or null, if the queue is empty.
     * 
     * @return percept the oldest percept still on the sensor's percept queue
     */
    public Percept getPerception();

    /**
     * Returns true if it can process events of the given type.
     * 
     * @param type
     *            event type.
     * @return canHandle true if the Sensor can process {@link Action} objects
     *         of this type, else false
     */
    public boolean handlesType(String type);

    /**
     * Passes an {@link Action} to the Sensor, of a type that the Sensor reports
     * it can handle (via {@link #handlesType(String))}).
     * 
     * @param action
     *            information sent to the sensor
     */
    public void sense(Action action);

    /**
     * REQDOC
     * 
     * @param listener
     */
    public void setSensorHasPerceptListener(SensorHasPerceptListener listener);

}
