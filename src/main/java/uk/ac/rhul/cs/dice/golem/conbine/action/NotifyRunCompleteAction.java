package uk.ac.rhul.cs.dice.golem.conbine.action;

import org.javatuples.Pair;

import uk.ac.rhul.cs.dice.golem.action.Action;

/**
 * As per {@link ConbineActionType#NOTIFY_RUN_COMPLETE}.
 * 
 * @author ataulm
 * 
 */
public class NotifyRunCompleteAction extends Action {
    public static final int COMBINATION_NUMBER = 0;
    public static final int RUN_NUMBER = 1;
    
    public static class Builder {
        private int combination;
        private int run;

        public NotifyRunCompleteAction build() {
            if (combination < 1) {
                throw new IllegalStateException(
                        "Combination Number must be set (combination number starts at 1).");
            }

            if (run < 1) {
                throw new IllegalStateException(
                        "Run Number must be set (run number starts at 1).");
            }

            Pair<Integer, Integer> payload = new Pair<>(combination, run);

            return new NotifyRunCompleteAction(null, payload);
        }

        public Builder setRunNumber(int run) {
            this.run = run;
            return this;
        }

        public Builder setCombinationNumber(int combination) {
            this.combination = combination;
            return this;
        }
    }

    protected NotifyRunCompleteAction(String recipient, Pair<Integer, Integer> payload) {
        super(ConbineActionType.NOTIFY_RUN_COMPLETE.toString(), recipient, payload);
    }

    /**
     * Returns the combination that the completed run was from.
     *
     * @return
     */
    public int getCombinationNumber() {
        return (int) getPayload().getValue(COMBINATION_NUMBER);
    }

    /**
     * Returns the run that was just completed.
     *
     * @return
     */
    public int getRunNumber() {
        return (int) getPayload().getValue(RUN_NUMBER);
    }

    @Override
    public String toString() {
        return getActionType() + "(" + getCombinationNumber() + ", " + getRunNumber() + ")";
    }
}
