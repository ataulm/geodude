package uk.ac.rhul.cs.dice.golem.conbine.agent.williams.utils;

/**
 * @author Colin Williams
 * 
 */
public class WeightHypothesis extends Hypothesis {

    double weight;

    public double getWeight() {
        return this.weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return String.format("%1.2f", this.weight) + ";";
    }
}
