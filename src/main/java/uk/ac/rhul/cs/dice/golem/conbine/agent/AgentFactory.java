package uk.ac.rhul.cs.dice.golem.conbine.agent;

import uk.ac.rhul.cs.dice.golem.action.Effector;
import uk.ac.rhul.cs.dice.golem.action.Sensor;
import uk.ac.rhul.cs.dice.golem.agent.*;
import uk.ac.rhul.cs.dice.golem.conbine.agent.MarketAgent.AgentType;
import uk.ac.rhul.cs.dice.golem.conbine.agent.faratin.FaratinAverageTitForTatBuyer;
import uk.ac.rhul.cs.dice.golem.conbine.agent.faratin.FaratinAverageTitForTatSeller;
import uk.ac.rhul.cs.dice.golem.conbine.agent.faratin.FaratinRandomAbsoluteTitForTatBuyer;
import uk.ac.rhul.cs.dice.golem.conbine.agent.faratin.FaratinRandomAbsoluteTitForTatSeller;
import uk.ac.rhul.cs.dice.golem.conbine.agent.faratin.FaratinRelativeTitForTatBuyer;
import uk.ac.rhul.cs.dice.golem.conbine.agent.faratin.FaratinRelativeTitForTatSeller;
import uk.ac.rhul.cs.dice.golem.conbine.agent.faratin.FaratinResourceDependentBuyer;
import uk.ac.rhul.cs.dice.golem.conbine.agent.faratin.FaratinResourceDependentSeller;
import uk.ac.rhul.cs.dice.golem.conbine.agent.faratin.FaratinResourceTimeDependentBuyer;
import uk.ac.rhul.cs.dice.golem.conbine.agent.faratin.FaratinResourceTimeDependentSeller;
import uk.ac.rhul.cs.dice.golem.conbine.agent.faratin.FaratinTimeDependentBoulwareBuyer;
import uk.ac.rhul.cs.dice.golem.conbine.agent.faratin.FaratinTimeDependentBoulwareSeller;
import uk.ac.rhul.cs.dice.golem.conbine.agent.faratin.FaratinTimeDependentConcederBuyer;
import uk.ac.rhul.cs.dice.golem.conbine.agent.faratin.FaratinTimeDependentConcederSeller;
import uk.ac.rhul.cs.dice.golem.conbine.agent.faratin.FaratinTimeDependentLinearBuyer;
import uk.ac.rhul.cs.dice.golem.conbine.agent.faratin.FaratinTimeDependentLinearSeller;
import uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure.MarketBroker;
import uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure.MarketBrokerEar;
import uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure.MarketBrokerMouth;
import uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure.MarketController;
import uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure.MarketControllerEar;
import uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure.MarketControllerMouth;
import uk.ac.rhul.cs.dice.golem.conbine.agent.simple.SimpleBuyer;
import uk.ac.rhul.cs.dice.golem.conbine.agent.simple.SimpleSeller;
import uk.ac.rhul.cs.dice.golem.conbine.agent.williams.WilliamsIAmHagglerBuyer;
import uk.ac.rhul.cs.dice.golem.conbine.app.ConbineUserInput;
import uk.ac.rhul.cs.dice.golem.container.Container;
import uk.ac.rhul.cs.dice.golem.container.DefaultContainer;

public class AgentFactory {
    public static PoolableAgentBrain createConbinePrologAgent(String id,
            String prologMind, AgentType type, Container env,
            AgentParameters params, String product) {

        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(id);

        // brain has reference to both mind and body
        brain.setBody(body);

        AgentMind mind = new ConbinePrologAgentMind(type, brain, prologMind,
                params, product);

        brain.setMind(mind);

        // body needs to know about the environment so it can perform actions
        body.setEnvironment(env);
        env.makePresent(body);

        // body needs to know about the brain so it can notify when it receives
        // a new percept
        body.setBrain(brain);

        // All ConbineAgents need an ear to listen to the 5 standard actions
        Sensor regularEar = new MarketAgentEar("ear", body);
        // and a mouth that can produce the 7 standard actions
        Effector mouth = new MarketAgentMouth("mouth", body);

        // the sensors and effectors must be registered with the body
        body.registerEffector(mouth);
        // the "false" says this sensor will not receive broadcast messages
        // - only direct messages (whispers)
        body.registerSensor(regularEar, false);

        return brain;
    }

    public static AgentBrain createMarketBroker(String agentId,
            Container container) {
        AgentBrain brain = new DefaultAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new MarketBroker(brain);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(container);
        container.makePresent(body);

        // special ear for broker only listens for ANNOUNCE/EXIT_ALL
        Sensor eagleEar = new MarketBrokerEar("ear", body);
        Effector mouth = new MarketBrokerMouth("mouth", body);

        body.registerEffector(mouth);
        // broker's ear listens for broadcast messages
        body.registerSensor(eagleEar, true);

        return brain;
    }

    public static AgentBrain createMarketController(String agentId,
            Container container, String pathToAppData,
            ConbineUserInput userInput) {
        AgentBrain brain = new DefaultAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new MarketController(brain,
                (DefaultContainer) container, pathToAppData, userInput);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(container);
        container.makePresent(body);

        Sensor controllerEar = new MarketControllerEar("ear", body);
        Effector controllerMouth = new MarketControllerMouth("mouth", body);
        
        body.registerEffector(controllerMouth);
        // listens to broadcasts about exit all
        body.registerSensor(controllerEar, true);
        
        return brain;
    }

    public static PoolableAgentBrain createSimpleBuyerAgent(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new SimpleBuyer(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }
    
    public static PoolableAgentBrain createFaratinTimeDependentLinearBuyer(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new FaratinTimeDependentLinearBuyer(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }
    
    public static PoolableAgentBrain createFaratinTimeDependentLinearSeller(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new FaratinTimeDependentLinearSeller(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }
    
    public static PoolableAgentBrain createFaratinTimeDependentConcederBuyer(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new FaratinTimeDependentConcederBuyer(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }
    
    public static PoolableAgentBrain createFaratinTimeDependentConcederSeller(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new FaratinTimeDependentConcederSeller(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }

    public static PoolableAgentBrain createFaratinTimeDependentBoulwareBuyer(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new FaratinTimeDependentBoulwareBuyer(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }
    
    public static PoolableAgentBrain createFaratinTimeDependentBoulwareSeller(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new FaratinTimeDependentBoulwareSeller(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }
    
    public static PoolableAgentBrain createFaratinResourceDependentBuyer(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new FaratinResourceDependentBuyer(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }
    
    public static PoolableAgentBrain createFaratinResourceDependentSeller(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new FaratinResourceDependentSeller(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }
    
    public static PoolableAgentBrain createFaratinResourceTimeDependentBuyer(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new FaratinResourceTimeDependentBuyer(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }
    
    public static PoolableAgentBrain createFaratinResourceTimeDependentSeller(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new FaratinResourceTimeDependentSeller(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }
    
    public static PoolableAgentBrain createFaratinRelativeTitForTatBuyer(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new FaratinRelativeTitForTatBuyer(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }
    
    public static PoolableAgentBrain createFaratinRelativeTitForTatSeller(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new FaratinRelativeTitForTatSeller(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }
    
    public static PoolableAgentBrain createFaratinRandomAbsoluteTitForTatBuyer(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new FaratinRandomAbsoluteTitForTatBuyer(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }
    
    public static PoolableAgentBrain createFaratinRandomAbsoluteTitForTatSeller(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new FaratinRandomAbsoluteTitForTatSeller(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }
    
    public static PoolableAgentBrain createFaratinAverageTitForTatBuyer(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new FaratinAverageTitForTatBuyer(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }
    
    public static PoolableAgentBrain createFaratinAverageTitForTatSeller(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new FaratinAverageTitForTatSeller(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }

    public static PoolableAgentBrain createSimpleSellerAgent(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new SimpleSeller(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }

    public static PoolableAgentBrain createWilliamsBuyerAgent(String agentId,
            Container env, AgentParameters params, String product) {
        PoolableAgentBrain brain = new DefaultPoolableAgentBrain();
        AgentBody body = new DefaultAgentBody(agentId);
        AgentMind mind = new WilliamsIAmHagglerBuyer(brain, params, product);

        brain.setMind(mind);
        brain.setBody(body);
        body.setEnvironment(env);
        env.makePresent(body);
        body.setBrain(brain);

        Sensor regularEar = new MarketAgentEar("ear", body);
        Effector mouth = new MarketAgentMouth("mouth", body);

        body.registerEffector(mouth);
        body.registerSensor(regularEar, false);

        return brain;
    }

}
