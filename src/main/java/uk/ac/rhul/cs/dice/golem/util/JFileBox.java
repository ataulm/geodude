package uk.ac.rhul.cs.dice.golem.util;

import java.awt.FileDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;

import uk.ac.rhul.cs.dice.golem.gui.ApplicationTab;
import uk.ac.rhul.cs.dice.golem.util.FileList.FileListChanged;

/**
 * JFileBox is an extension of JComboBox which facilitates the management of a
 * drop-down list that can hold PathAlias items. Each PathAlias item consists of
 * an alias (the filename) which is displayed on the drop-down list and a path
 * (the absolute path to the file).
 * 
 * When a JFileBox is registered with a FileList, any changes to the underlying
 * FileList will trigger an update in the JFileBox.
 * 
 * @author ataulm
 * 
 */
@SuppressWarnings("serial")
public class JFileBox extends JComboBox<PathAlias> implements FileListChanged,
        ActionListener {
    private final ApplicationTab context;
    private FileList list;
    private String defaultDirectory;
    private boolean includeUnselectedOption = false;
    private String unselectedText = "Select an item";
    private JButton browseBtn;

    /**
     * Creates a new JFileBox with an associated JButton with the label,
     * "Browse".
     */
    public JFileBox(ApplicationTab context) {
        super();
        this.context = context;
        init(true, "Browse", null);
    }

    /**
     * Use JFileBox(false) to avoid the automatic creation of a browse button.
     * 
     * @param makeBtn
     */
    public JFileBox(ApplicationTab context, boolean makeBtn) {
        super();
        this.context = context;
        init(makeBtn, null, null);
    }

    /**
     * Creates a new JFileBox with an associated JButton with the label,
     * "Browse", and sets the specified FileList.
     */
    public JFileBox(ApplicationTab context, FileList list) {
        super();
        this.context = context;
        init(true, "Browse", list);
    }

    /**
     * Specify the label on the browse button by passing it here.
     * 
     * @param btnLabel
     */
    public JFileBox(ApplicationTab context, String btnLabel) {
        super();
        this.context = context;
        init(true, btnLabel, null);
    }

    /**
     * Specify the label on the browse button by passing it here, and sets the
     * specified FileList.
     * 
     * @param btnLabel
     */
    public JFileBox(ApplicationTab context, String btnLabel, FileList list) {
        super();
        this.context = context;
        init(true, btnLabel, list);
    }

    /**
     * Used primarily to handle the onClick of the browse JButton
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        Object src = event.getSource();

        if (src == browseBtn) {
            FileDialog dialog = new FileDialog(context.getContext().getGolemGui()
                    .getMainFrame(), "Choose file", FileDialog.LOAD);
            dialog.setDirectory(defaultDirectory);
            dialog.setVisible(true);

            Logger.i(this, "directory:" + dialog.getDirectory());
            Logger.i(this, "file:" + dialog.getFile());

            File file = new File(dialog.getDirectory() + dialog.getFile());
            if (dialog.getFile() == null)
                Logger.micro(this, "File selection cancelled.");
            else {
                PathAlias alias = new PathAlias(file.getPath(), file.getName());
                if (!list.contains(alias))
                    list.add(alias);
                setSelectedItem(alias);
            }
        }
    }

    public JButton getBrowseButton() {
        return browseBtn;
    }

    public String getDefaultDirectory() {
        return defaultDirectory;
    }

    public boolean getIncludeUnselectedOption() {
        return includeUnselectedOption;
    }

    /**
     * Returns the reference to the FileList this JFileBox is registered to.
     * 
     * @return
     */
    public FileList getList() {
        return list;
    }

    public String getUnselectedText() {
        return unselectedText;
    }

    /**
     * Some offset initialisation after construction
     * 
     * @param btnLabel
     */
    private void init(boolean makeBtn, String btnLabel, FileList list) {
        if (makeBtn) {
            browseBtn = new JButton(btnLabel);
            browseBtn.addActionListener(this);
        }
        if (list != null)
            setList(list);
        defaultDirectory = context.getContext().getAppDir();
    }

    @Override
    public void onFileListChanged(FileList list) {
        Logger.micro(this, "Received notification FileList has changed, updating.");

        // keep a reference to the currently selected item
        Object selected = getSelectedItem();
        // update the JFileBox's list with the updated FileList
        setModel(new DefaultComboBoxModel<>(
                list.toArray(new PathAlias[list.size()])));
        setSelectedItem(selected);

        // if there should be an item indicating a lack of selection, add it
        if (includeUnselectedOption) {
            Logger.i(this, "Inserting unselected item with text: "
                    + unselectedText);
            PathAlias unselectedOp = new PathAlias("_", unselectedText);
            insertItemAt(unselectedOp, 0);
        }

        if (list.size() == 0)
            setSelectedItem(null);
        else if (selected == null) {
            setSelectedIndex(0);
        } else {
            if (!getSelectedItem().equals(selected)) {
                Logger.i(this, "selected item is not equal to previous selection");
                setSelectedIndex(0);
            }
        }
    }

    public void setBrowseButton(JButton btn) {
        if (browseBtn != null)
            browseBtn.removeActionListener(this);

        browseBtn = btn;
        browseBtn.addActionListener(this);
    }

    /**
     * Path at which to open the FileDialog when the browse button is clicked.
     * 
     * @param defaultDirectory
     */
    public void setDefaultDirectory(String defaultDirectory) {
        this.defaultDirectory = defaultDirectory;
    }

    /**
     * If set to true, the JFileBox will be created with an extra option at the
     * start, to indicate no item has been selected.
     * 
     * @param includeUnselectedOption
     */
    public void setIncludeUnselectedOption(boolean includeUnselectedOption) {
        this.includeUnselectedOption = includeUnselectedOption;
        onFileListChanged(list);
    }

    /**
     * Registers this JFileBox with the specified list, and retains the
     * reference to that list.
     * 
     * @param list
     */
    public void setList(FileList list) {
        this.list = list;
        list.register(this);
        onFileListChanged(list);
    }

    /**
     * Set the text to show if there should be an option for unselected. If
     * includeUnselectedOption is false, setting the text will change it to
     * true.
     * 
     * @param unselectedText
     */
    public void setUnselectedText(String unselectedText) {
        this.unselectedText = unselectedText;
        if (!includeUnselectedOption)
            setIncludeUnselectedOption(true);
    }
}
