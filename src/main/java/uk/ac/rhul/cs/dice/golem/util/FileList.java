package uk.ac.rhul.cs.dice.golem.util;

import java.util.ArrayList;
import java.util.Collection;

/**
 * FileList provides a mechanism to notify registered entities when the
 * underlying collection of PathAlias objects may have changed.
 * 
 * See {@link PathAlias}. See {@link ArrayList}.
 * 
 * @author ataulm
 * 
 */
@SuppressWarnings("serial")
public class FileList extends ArrayList<PathAlias> {
    /**
     * Classes wishing to register for notifications must implement this
     * interface.
     * 
     * @author ataulm
     * 
     */
    public interface FileListChanged {
        /**
         * Called when a FileList has been updated.
         * 
         * @param list
         *            The file list that was updated.
         */
        void onFileListChanged(FileList list);
    }

    private final ArrayList<FileListChanged> registeredEntities;

    public FileList() {
        super();
        registeredEntities = new ArrayList<>();
    }

    @Override
    public void add(int index, PathAlias pathAlias) {
        super.add(index, pathAlias);
        fileListChanged();
    }

    @Override
    public boolean add(PathAlias pathAlias) {
        boolean changed = super.add(pathAlias);
        if (changed)
            fileListChanged();
        return changed;
    }

    @Override
    public boolean addAll(Collection<? extends PathAlias> c) {
        boolean changed = super.addAll(c);
        if (changed)
            fileListChanged();
        return changed;
    }

    @Override
    public boolean addAll(int index, Collection<? extends PathAlias> c) {
        boolean changed = super.addAll(index, c);
        if (changed)
            fileListChanged();
        return changed;
    }

    @Override
    public void clear() {
        super.clear();
        fileListChanged();
    }

    /**
     * Notifies all registered entities that the file list may have changed.
     */
    protected void fileListChanged() {
        Logger.micro(this, "FileList has changed, notifying registered entities.");

        for (FileListChanged registeredEntity : registeredEntities) {
            registeredEntity.onFileListChanged(this);
        }
    }

    /**
     * Returns true if the specified entity is already registered.
     * 
     * @param entity
     * @return
     */
    public boolean isRegistered(FileListChanged entity) {
        if (registeredEntities.contains(entity))
            return true;
        return false;
    }

    /**
     * Register an object to receive notifications when this FileList is
     * updated.
     * 
     * @param entity
     */
    public void register(FileListChanged entity) {
        if (!isRegistered(entity))
            registeredEntities.add(entity);
    }

    @Override
    public PathAlias remove(int index) {
        PathAlias removed = super.remove(index);
        fileListChanged();
        return removed;
    }

    @Override
    public boolean remove(Object o) {
        boolean changed = super.remove(o);
        if (changed)
            fileListChanged();
        return changed;
    }

    @Override
    protected void removeRange(int fromIndex, int toIndex) {
        super.removeRange(fromIndex, toIndex);
        fileListChanged();
    }

    @Override
    public PathAlias set(int index, PathAlias element) {
        PathAlias replaced = super.set(index, element);
        fileListChanged();
        return replaced;
    }
}
