package uk.ac.rhul.cs.dice.golem.conbine.action;

import org.javatuples.Quartet;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.conbine.agent.AgentParameters;

/**
 * As per {@link ConbineActionType#COLLECT_METRICS}.
 * 
 * @author ataulm
 * 
 */
public class CollectMetricsAction extends Action {
    public static class Builder {
        private String state;
        private String recipientId;
        private String subjectId;
        private String replyToId;
        private AgentParameters params;

        public CollectMetricsAction build() {
            if (state == null || (!state.equals(COLLECT) && !state.equals(COLLECTED))) {
                throw new IllegalStateException(
                        "State must be either CollectMetricsAction.COLLECT or CollectMetricsAction.COLLECTED. (It was: "+ state+ ")");
            }

            if (recipientId == null || recipientId.length() == 0) {
                throw new IllegalStateException("The recipient ID must be set.");
            }

            if (subjectId == null || subjectId.length() == 0) {
                throw new IllegalStateException(
                        "The subject (agent for which stats are being collected) must be set.");
            }

            if (replyToId == null || replyToId.length() == 0) {
                throw new IllegalStateException("The reply-to ID must be set.");
            }

            if (params == null) {
                throw new IllegalStateException(
                        "Agent parameters must be passed (so MetricsCollector can calculate utility).");
            }

            Quartet<String, String, String, AgentParameters> payload = new Quartet<>(
                    state, subjectId, replyToId, params);

            return new CollectMetricsAction(recipientId, payload);
        }

        public Builder setParams(AgentParameters params) {
            this.params = params;
            return this;
        }

        public Builder setRecipient(String recipientId) {
            this.recipientId = recipientId;
            return this;
        }

        public Builder setReplyToId(String replyToId) {
            this.replyToId = replyToId;
            return this;
        }

        public Builder setState(String state) {
            this.state = state;
            return this;
        }

        public Builder setSubject(String subjectId) {
            this.subjectId = subjectId;
            return this;
        }
    }
    public static final int STATE = 0;
    public static final int SUBJECT = 1;
    public static final int REPLY_TO_ID = 2;

    public static final int AGENT_PARAMS = 3;
    public static final String COLLECT = "collectMetrics";

    public static final String COLLECTED = "metricsCollected";

    protected CollectMetricsAction(String recipient,
            Quartet<String, String, String, AgentParameters> payload) {
        super(ConbineActionType.COLLECT_METRICS.toString(), recipient, payload);
    }

    public AgentParameters getParams() {
        return (AgentParameters) getPayload().getValue(AGENT_PARAMS);
    }

    public String getReplyToId() {
        return (String) getPayload().getValue(REPLY_TO_ID);
    }

    public String getState() {
        return (String) getPayload().getValue(STATE);
    }

    public String getSubject() {
        return (String) getPayload().getValue(SUBJECT);
    }

    @Override
    public String toString() {
        return getActionType() + "(" + getState() + " for subject: "
                + getSubject() + ")";
    }
}
