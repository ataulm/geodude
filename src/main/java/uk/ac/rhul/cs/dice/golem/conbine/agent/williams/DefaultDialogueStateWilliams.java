package uk.ac.rhul.cs.dice.golem.conbine.agent.williams;

import java.util.ArrayList;
import java.util.List;

import uk.ac.rhul.cs.dice.golem.conbine.agent.DefaultDialogueState;
import uk.ac.soton.ecs.gp4j.bmc.GaussianProcessRegressionBMC;
import Jama.Matrix;

/**
 * DialogueStateWilliams maintains the state of an individual dialogue,
 * including the agent's last bid.
 * 
 */
public class DefaultDialogueStateWilliams extends DefaultDialogueState implements DialogueStateWilliams {
    private double bid = 0;
    private double opponentBid = 0;    
    private double bestReceivedBid = 0;
    private double lastRegressionTime = 0;
    private double lastRegressionUtility = 1;
    private double maxUtility = 0;
    private double maxUtilityInTimeSlot = 0;
    private double previousTargetUtility = 1;
    private int lastTimeSlot = -1;
    private double committedOffer = 0;
    private final double probabilityOpponentWillStay;
    private final List<Double> opponentTimes;
    private final List<Double> opponentUtilities;
    
    private GaussianProcessRegressionBMC regression;
    private Matrix means;
    private Matrix utility;
    private Matrix variances;
    
    public DefaultDialogueStateWilliams(String id, String protocol, String opponent, String product, double probabilityOpponentWillStay) {
        super(id, protocol, opponent, product);
        this.probabilityOpponentWillStay = probabilityOpponentWillStay;
        opponentTimes = new ArrayList<>();
        opponentUtilities = new ArrayList<>();
    }
    
    @Override
    public final double getMyLastBid() {
        return bid;
    }
    
    @Override
    public final void updateMyLastBid(double bid) {
        this.bid = bid;            
    }

    @Override
    public double getOpponentsLastBid() {
        return opponentBid;
    }

    @Override
    public void updateOpponentsLastBid(double opponentBid) {
        this.opponentBid = opponentBid;
    }

    @Override
    public List<Double> getOpponentTimes() {
        return opponentTimes;
    }

    @Override
    public List<Double> getOpponentUtilities() {
        return opponentUtilities;
    }

    @Override
    public double getBestReceivedBid() {
        return bestReceivedBid;
    }
    
    @Override
    public void updateBestReceivedBid(double bestReceivedBid) {
        this.bestReceivedBid = bestReceivedBid;
    }

    @Override
    public double getLastRegressionTime() {
        return lastRegressionTime;
    }
    
    @Override
    public void updateLastRegressionTime(double lastRegressionTime) {
        this.lastRegressionTime = lastRegressionTime;        
    }

    @Override
    public double getLastRegressionUtility() {
        return lastRegressionUtility;
    }
    
    @Override
    public void updateLastRegressionUtility(double lastRegressionUtility) {
        this.lastRegressionUtility = lastRegressionUtility;
    }

    @Override
    public int getLastTimeSlot() {
        return lastTimeSlot;
    }
    
    @Override
    public void updateLastTimeSlot(int lastTimeSlot) {
        this.lastTimeSlot = lastTimeSlot;
    }

    @Override
    public double getMaxUtility() {
        return maxUtility;
    }
    
    @Override
    public void setMaxUtility(double maxUtility) {
        this.maxUtility = maxUtility;
    }

    @Override
    public double getMaxUtilityInTimeSlot() {
        return maxUtilityInTimeSlot;
    }
    
    @Override
    public void setMaxUtilityInTimeSlot(double maxUtilityInTimeSlot) {
        this.maxUtilityInTimeSlot = maxUtilityInTimeSlot;        
    }

    @Override
    public double getPreviousTargetUtility() {
        return previousTargetUtility;
    }
    
    @Override
    public void updatePreviousTargetUtility(double previousTargetUtility) {
        this.previousTargetUtility = previousTargetUtility;
    }
    
    @Override
    public GaussianProcessRegressionBMC getRegression() {
        return regression;
    }
    
    @Override
    public void setRegression(GaussianProcessRegressionBMC regression) {
        this.regression = regression;        
    }
    
    @Override
    public Matrix getUtility() {
        return utility;
    }
    
    @Override
    public void setUtility(Matrix utility) {
        this.utility = utility;
    }

    @Override
    public Matrix getMeans() {
        return means;
    }
    
    @Override
    public void setMeans(Matrix means) {
        this.means = means;
    }

    @Override
    public Matrix getVariances() {
        return variances;
    }

    @Override
    public void setVariances(Matrix variances) {
        this.variances = variances;
    }
    
    @Override
    public double getCommittedOffer() {
    	return committedOffer;
    }
    
    @Override
    public void setCommittedOffer(double committedOffer) {
    	this.committedOffer = committedOffer;    	
    }

	@Override
	public double getProbabilityOpponentWillStay() {
		return probabilityOpponentWillStay;
	}
}
