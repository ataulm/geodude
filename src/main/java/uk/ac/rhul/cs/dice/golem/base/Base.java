package uk.ac.rhul.cs.dice.golem.base;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.SwingUtilities;

import uk.ac.rhul.cs.dice.golem.application.Application;
import uk.ac.rhul.cs.dice.golem.gui.GolemGUI;
import uk.ac.rhul.cs.dice.golem.util.Logger;
import au.com.bytecode.opencsv.CSVReader;

public class Base {
    /**
     * Application title
     */
    public static final String APP_NAME = "GOLEM lite 0.9";

    /**
     * Name of the directory with application specific data.
     */
    public static final String APP_DATA = "AppData";

    /**
     * Name of the plugins directory
     */
    public static final String PLUGINS = "plugins";

    /**
     * Use the native operating system's theme
     */
    public static final Boolean NATIVE_SYSTEM_THEMING = true;

    /**
     * Main application window
     */
    private static GolemGUI golemWindow;

    /**
     * A map of available applications determined by reading the manifest file
     * to see which plugins are present on the filesystem. The key is the
     * application name, and string array is the path to the jar file, and the
     * fully qualified class name, respectively.
     */
    private Map<String, String[]> availableApps;

    public static final int MAP_JAR = 0;
    public static final int MAP_CLASS = 1;

    public static final int MANIFEST_NAME = 0;
    public static final int MANIFEST_JAR = 1;
    public static final int MANIFEST_CLASS = 2;

    /**
     * Creates and instantiates a new Base object
     * 
     * @param args
     */
    public static void main(String[] args) throws Exception {

        if (args.length > 0) {
            for (String arg : args)
                System.out.println("GOLEM is in: " + arg);
            new Base(args[0]);
        } else {
            System.out.println("No arguments found.");
            new Base(null);
        }
    }

    /**
     * An up-to-date, unordered list of current instances of Applications
     */
    private ArrayList<Application> runningApps;

    /**
     * Filepath to the root directory (which contains folders like plugins/,
     * bin/ (where GOLEM is run from), etc.)
     */
    private final File baseDir;

    /**
     * Initialises the GUI
     */
    public Base(String baseDir) throws Exception {
        if (baseDir != null) {
            // this is the expected result when running from a jar
            // the directory of the jar should be passed in as String
            this.baseDir = new File(baseDir);
        } else {
            System.out.println("Fiddlings with base directory");
            // this is used when running GOLEM directly from the IDE (i.e. dev)
            ClassLoader classLoader = Base.class.getClassLoader();
            File temp = new File(classLoader.getResource("").getPath());

            File targetDir = new File(temp.getParent());
            this.baseDir = new File(targetDir.getParent());

            System.out.println("baseDir path: " + this.baseDir.getPath());
            getBaseDir();
        }

        golemWindow = new GolemGUI(this);

        // Get the UI up first to prevent impression of unresponsiveness
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                golemWindow.getMainFrame().setVisible(true);
            }
        });

        // Check the plugin directory for any available plugins
        initApplications();
        golemWindow.setAvailableApplications(availableApps.keySet());
    }

    /**
     * Launches the specified application in the main window.
     * 
     * @param app
     */
    public void createApplication(Application app) {
        app.init(golemWindow);
    }

    /**
     * Creates an instance of the requested application and adds it to a list of
     * currently active applications.
     * 
     * @param appName
     *            name of the application to be instanced
     */
    public void createApplication(String appName) throws Exception {
        File jar = new File(getPluginDir() + File.separator
                + availableApps.get(appName)[MAP_JAR]);
        Application app = null;
        ClassLoader loader = null;

        loader = URLClassLoader
                    .newInstance(new URL[] { jar.toURI().toURL() });
            app = (Application) loader.loadClass(
                    availableApps.get(appName)[MAP_CLASS]).newInstance();

        if (app != null) {
            if (runningApps == null)
                runningApps = new ArrayList<>();
            runningApps.add(app);
            app.init(golemWindow);
        }
    }

    /**
     * Returns the path to the directory containing application specific data
     * 
     * @return
     */
    public String getAppDataDir() {
        Logger.micro(this, "AppData directory: " + getBaseDir() + File.separator
                + APP_DATA);
        return getBaseDir() + File.separator + APP_DATA;
    }

    /**
     * Returns the path to the directory above where the application started.
     * The GOLEM application is in the /bin directory, so this returns the path
     * to the root directory (what ever that is named).
     * 
     * @return
     */
    public String getBaseDir() {
        Logger.micro(this, "Base directory: " + baseDir.getPath());
        return baseDir.getPath();
    }

    /**
     * Returns the path to the directory where the plugins are expected to be
     * found.
     * 
     * @return
     */
    public String getPluginDir() {
        Logger.micro(this, "Plugin directory: " + getBaseDir() + File.separator
                + PLUGINS);
        return getBaseDir() + File.separator + PLUGINS;
    }

    /**
     * Checks the manifest file for a list of applications (plugins). If the
     * application is available, adds it to a list of available apps, and passes
     * it to the GOLEM GUI which should update its menu.
     */
    private void initApplications() {
        // should store application name, name of jar and qualified class name
        availableApps = new HashMap<>();

        // Path to manifest file
        String pluginDirPath = getBaseDir() + File.separator + PLUGINS;
        String manifestFilepath = pluginDirPath + File.separator
                + "manifest.golem";

        // If there's a manifest file
        if (new File(manifestFilepath).exists()) {
            CSVReader reader = null;
            List<String[]> allLines;
            try {
                // no escape characters ('\0')
                reader = new CSVReader(new FileReader(manifestFilepath), '\t',
                        '\"', '\0');
                allLines = reader.readAll();

                Logger.i(this, "Reading manifest.golem...");
                // iterate through each line in the manifest
                for (String[] line : allLines) {
                    Logger.micro(
                            this,
                            "\t\tprocessing line "
                                    + String.valueOf(allLines.indexOf(line) + 1));

                    // Ignore empty lines, or lines which start with #
                    if (line[0].trim().length() == 0 || line[0].startsWith("#"))
                        continue;

                    // Ignore lines which duplicate Application names
                    if (availableApps.containsKey(line[MANIFEST_NAME])) {
                        Logger.w(this,
                                "Manifest file contains lines with duplicate Application names ("
                                        + line[MANIFEST_NAME]
                                        + "), ignoring non-initial instances");
                        continue;
                    }

                    // Ignore lines where the specified jar file doesn't exist
                    // in the plugins dir
                    if (!new File(pluginDirPath + File.separator
                            + line[MANIFEST_JAR]).exists())
                        continue;

                    // Otherwise add it
                    String[] appData = new String[2];
                    appData[MAP_JAR] = line[MANIFEST_JAR];
                    appData[MAP_CLASS] = line[MANIFEST_CLASS];

                    availableApps.put(line[MANIFEST_NAME], appData);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
