package uk.ac.rhul.cs.dice.golem.gui;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

/**
 * StatusBar is added to the bottom of your tabs to display short, contextual
 * messages about what's happening to the user.
 * 
 * @author ataulm
 * 
 */
@SuppressWarnings("serial")
public class StatusBar extends JLabel {
    private String status;
    private boolean added;

    public StatusBar() {
        super();
        setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
        setText("Placeholder text");
    }

    /**
     * Adds the statusbar to the host JPanel, docked to the bottom The host
     * JPanel must use MigLayout as its layout manager.
     * 
     * @param host
     */
    public void addToHost(JPanel host) {
        if (!added) {
            host.add(this, "south");
            added = true;
        }
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
        // TODO: Check length of status, break off after 100 chars
        // add rest to tool tip with line breaks.
        setText(status);
    }
}
