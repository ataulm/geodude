package uk.ac.rhul.cs.dice.golem.conbine.agent;

import java.util.ArrayList;
import java.util.List;

public class DefaultOfferHistory implements OfferHistory {
	private final List<Double> opponentHistory;
	private double bid = 0;

	public DefaultOfferHistory() {
	    opponentHistory = new ArrayList<>();
	}

	@Override
	public final double getMyLastBid() {
	    return bid;
	}
	
	@Override
	public final void updateMyLastBid(double bid) {
	    this.bid = bid;            
	}
	
	@Override
	public final double getMinusNthOpponentBid(int n) {
	    return opponentHistory.get(n);
	}
	
	@Override
	public final void addOpponentBid(double bid) {
	    opponentHistory.add(0, bid);            
	}

	@Override
	public double getOldestOpponentBid() {
		return opponentHistory.get(opponentHistory.size() - 1);
	}
}
