package uk.ac.rhul.cs.dice.golem.util;

/**
 * Base class to allow logging to standard output.
 * 
 * Use {@link #log(String)} for standard output, this will be printed at all times (typically
 * used to debug small sections) if logging is turned on.
 * 
 * Use {@link #logMicro(String)} for more granular logging. This is only enabled when the logging
 * level is set to {@link #MICRO}.
 *
 */
public class Logger {    
    public static final int OFF = 0;
    public static final int STANDARD = 1;
    public static final int MICRO = 2;
    private int level;
    
    /**
     * Set the level of logging for this object.
     * 
     * Filters which logs make it to standard out. Valid parameters include:
     * - {@link #OFF}
     * - {@link #STANDARD}
     * - {@link #MICRO}
     * 
     * @param level  the logging level
     */
    public void setLogLevel(int level) {
        level = (level < OFF) ? OFF : level;
        level = (level > MICRO) ? MICRO : level;
        this.level = level;
    }
    
    protected int getLogLevel() {
        return level;
    }
    
    /**
     * Prints logs to standard out.
     * 
     * Only printed if the logging level is set to {@link #STANDARD}
     * or higher.
     * 
     * @param out  the message to log
     */
    protected void log(String out) {
        if (level >= STANDARD) {
            log(getClass().getSimpleName(), out);    
        }        
    }
    
    /**
     * Prints logs to standard out.
     * 
     * Only printed if the logging level is set to {@link #MICRO}
     * or higher.
     * 
     * @param out  the message to log
     */
    protected final void logMicro(String out) {
        if (level >= MICRO) {
            log(out);
        }
    }
    
    /**
     * Prints the log to standard out, prefixing the message with the specified string.
     * 
     * @param prefix  the string to prepend to the message
     * @param out  the message to log
     */
    protected final void log(String prefix, String out) {
        System.out.println(prefix + ": " + out);
    }
    
    
    
    
    
    
    
    
    @Deprecated
    public static void e(Object src, String msg) {
        if (Debug.SHOW_ERROR_LOGS)
            print(src, "error", msg);
    }

    @Deprecated
    public static void i(Object src, String msg) {
        if (Debug.SHOW_INFO_LOGS)
            print(src, "info", msg);
    }

    @Deprecated
    public static void micro(Object src, String msg) {
        if (Debug.SHOW_MICRO_LOGS)
            print(src, "micro", msg);
    }

    @Deprecated
    public static void print(Object src, String msg) {
        print(src, "out", msg);
    }

    @Deprecated
    private static void print(Object src, String flag, String message) {
        if (src != null) {
            System.out.println(flag + ": " + src.getClass().getSimpleName()
                    + ":: " + message);
        } else {
            System.out.println(flag + ": " + message);
        }
    }

    @Deprecated
    public static void w(Object src, String msg) {
        if (Debug.SHOW_WARNING_LOGS)
            print(src, "warning", msg);
    }

}
