package uk.ac.rhul.cs.dice.golem.conbine.agent;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.agent.AgentBrain;
import uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType;
import uk.ac.rhul.cs.dice.golem.conbine.action.NegotiationAction;
import uk.ac.rhul.cs.dice.golem.conbine.action.NotifyAboutChangeInDemandSupplyRatioAction;
import uk.ac.rhul.cs.dice.golem.conbine.action.NotifyAboutChangeInNumberOfCompetitors;
import uk.ac.rhul.cs.dice.golem.conbine.action.NotifyAboutNewSellerAction;
import uk.ac.rhul.cs.dice.golem.conbine.app.Ratio;

/**
 * Covers basic setup for a {@link MarketAgent} of type buyer.
 * 
 * Handles notifications from infrastructure agents. 
 *
 */
@SuppressWarnings("serial")
public abstract class AbstractBuyerAgent extends AbstractMarketAgent {
	private long startTime;

	protected AbstractBuyerAgent(AgentBrain brain, AgentParameters params,
			String product) {
		super(brain, params, product, AgentType.BUYER);
	}
	
	protected AbstractBuyerAgent(AgentBrain brain, AgentParameters params,
			String product, Map<String, DialogueState> dialogues) {
		super(brain, params, product, AgentType.BUYER, dialogues);
	}
	
	/**
	 * Get the time that the buyer entered the market.
	 * 
	 * @return start the time the buyer entered the market 
	 */
	protected long getStartTime() {
		return startTime;
	}
	
	@Override
	protected void onArrivalToMarket() {
		super.onArrivalToMarket();
		startTime = System.currentTimeMillis();
	}
	
	@Override
	protected List<Action> decideAction(Action action) {
		// check deadline	
		if (System.currentTimeMillis() > (startTime + getDeadline())) {
			// if there are dialogues with commitments, choose one and accept
			String dialogueId = getChosenCommit();
			if (dialogueId != null) {
				logMicro("should accept opponent offer: " + dialogueId);
				return acceptOpponentOffer(dialogueId);
			} else {
				logMicro("No committed offers, exiting as deadline.");
			}
			return super.exitAll();			
		}
		
		if (action != null) {
			if (action instanceof NotifyAboutNewSellerAction) {
				return makeOfferToNewSeller((NotifyAboutNewSellerAction) action);			
			}
			
			if (action instanceof NotifyAboutChangeInNumberOfCompetitors) {
				NotifyAboutChangeInNumberOfCompetitors notification = (NotifyAboutChangeInNumberOfCompetitors) action;
				return onNotifyAboutChangeInNumberOfCompetitors(notification.getNumberOfCompetitors());
			}
			
			if (action instanceof NotifyAboutChangeInDemandSupplyRatioAction) {
				NotifyAboutChangeInDemandSupplyRatioAction notification = (NotifyAboutChangeInDemandSupplyRatioAction) action;
				return onNotifyAboutChangeInDemandSupplyRatioAction(notification.getRatio());
			}
		}		
		
		// call super to handle default actions
		return super.decideAction(action);
	}
	
    protected List<Action> onNotifyAboutChangeInNumberOfCompetitors(int newNumberOfCompetitors) {
        return new ArrayList<>();
    }

    protected List<Action> onNotifyAboutChangeInDemandSupplyRatioAction(Ratio newRatio) {
        return new ArrayList<>();
    }
	
	/**
	 * Returns the dialogue for which the buyer wants to accept.
	 * 
	 * Iterate through all dialogues with a status of committed, and select one.
	 * The default implementation chooses one at random.
	 * If there are no committed dialogues, return null.
	 *  
	 * @return dialogueId  the ID of the dialogue with the chosen committed offer
	 */
	protected String getChosenCommit() {
	    List<String> committedOffers = new ArrayList<>();
        for (DialogueState dialogue : getDialogues().values()) {
            if (dialogue.isCommitted()) {
                committedOffers.add(dialogue.getId());
            }
        }
        if (committedOffers.size() > 0) {
            return committedOffers.get(getRandom().nextInt(committedOffers.size())); 
        }
        
        logMicro(getBrain().getAgentId() + " has no offers in committed state");
        return null;	    
	}
	
	protected List<Action> makeOfferToNewSeller(NotifyAboutNewSellerAction notification) {
        // create first offer action
	    NegotiationAction offer = new NegotiationAction.Builder()
	            .setType(ConbineActionType.OFFER)
	            .setProtocol("ao")
	            .setRecipient(notification.getNewSellerId())
	            .setDialogueId(notification.getDialogueId())	            
	            .setProductId(super.getProduct())	            
	            .setValue("" + super.getInitialPrice())
	            .setReplyToId(getBrain().getAgentId())
	            .build();
	
	    // add new record
	    super.getDialogues().put(offer.getDialogueId(), makeNewDialogueState(offer));
	
	    List<Action> actionsToPerform = new ArrayList<>();
	    actionsToPerform.add(offer);
	    
	    return actionsToPerform;
	}
	
	@Override
	protected DialogueState makeNewDialogueState(NegotiationAction offer) {
		return new DefaultDialogueState(
				offer.getDialogueId(),
				offer.getProtocol(),
				offer.getRecipient(),
				offer.getProductId());
	}
	
	@Override
	protected double getUtility(double offer) {
		return (getReservationPrice() - offer) / (getReservationPrice() - getInitialPrice());
	}
	
	@Override
	protected List<Action> acceptOpponentOffer(NegotiationAction offer) {
		List<Action> actionsToPerform = new ArrayList<>();
		
		actionsToPerform.addAll(super.acceptOpponentOffer(offer));

		// when the buyer sends an ACCEPT, the dialogue is finished
		getDialogues().remove(offer.getDialogueId());
        setTrading(false);

        actionsToPerform.addAll(super.exitAll());

        return actionsToPerform;
	}
	
	@Override
	protected List<Action> acceptOpponentOffer(String dialogueId) {
		List<Action> actionsToPerform = new ArrayList<>();
		
		actionsToPerform.addAll(super.acceptOpponentOffer(dialogueId));
        // when the buyer sends an ACCEPT, the dialogue is finished
        getDialogues().remove(dialogueId);
        setTrading(false);

        actionsToPerform.addAll(super.exitAll());
		
		return actionsToPerform;
	}
	
	/**
	 * Commits to the last bid devised by this agent.
	 * 
	 * The seller has accepted this agent's bid, and this agent now expresses
	 * commitment to it.
	 * 
	 * @see #commitToOpponentOffer(NegotiationAction)
	 * @param opponentAccept
	 * @return
	 */
	protected List<Action> commitToMyLastBid(NegotiationAction opponentAccept) {
		return commit(opponentAccept);
	}
	
	/**
	 * Commits to the bid received from the seller.
	 * 
	 * @see #commitToMyLastBid(NegotiationAction)
	 * @param opponentOffer
	 * @param offer
	 * @return
	 */
	protected List<Action> commitToOpponentOffer(NegotiationAction opponentOffer) {
		return commit(opponentOffer);
	}
	
	private List<Action> commit(NegotiationAction message) {
		List<Action> actionsToPerform = new ArrayList<>();
		
		actionsToPerform.add(new NegotiationAction.Builder()
				.setType(ConbineActionType.COMMIT)
				.setDialogueId(message.getDialogueId())
				.setProtocol(message.getProtocol())
		        .setRecipient(message.getReplyToId())
		        .setReplyToId(getBrain().getAgentId())
		        .setProductId(message.getProductId())
		        .build());
		
		getDialogues().get(message.getDialogueId()).setCommitted();
		
		return actionsToPerform;
	}
}
