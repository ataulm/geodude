package uk.ac.rhul.cs.dice.golem.agent;

import java.util.concurrent.Future;

/**
 * Allows storing the future returned when submitting this task (a Runnable)
 * to the ExecutorService. This allows {@link #stopCycle()} to chain to
 * {@link Future#cancel(boolean) Future#cancel(true)} which stops the agent.
 *
 */
public interface PoolableAgentBrain extends AgentBrain {
    public void setFuture(Future future);
}
