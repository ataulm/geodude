package uk.ac.rhul.cs.dice.golem.conbine.agent;
public interface MarketAgent {
    public enum AgentType {
        BUYER, SELLER;

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
    }
    
    public AgentType getType();
}
