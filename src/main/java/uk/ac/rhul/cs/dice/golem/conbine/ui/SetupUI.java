package uk.ac.rhul.cs.dice.golem.conbine.ui;

import javax.swing.JButton;

import net.miginfocom.swing.MigLayout;
import uk.ac.rhul.cs.dice.golem.application.AbstractTabPanel;
import uk.ac.rhul.cs.dice.golem.application.Application;
import uk.ac.rhul.cs.dice.golem.gui.Component;
import uk.ac.rhul.cs.dice.golem.util.Logger;

@SuppressWarnings("serial")
public class SetupUI extends AbstractTabPanel {

    protected SetupUI(Application context, String name) {
        super(context, name, true);
    }

    @Override
    protected void initSetup() {
        getContentPanel().setLayout(new MigLayout("fillx", "[fill]", "[nogrid]"));
        Component.createSeparator(this, "ConBiNe");
        getContentPanel().add(new JButton("Select"));
        Logger.i(this, "wat");
        
        
        
    }
    
    @Override
    protected void cleanup() { /* no-op */ }
}
