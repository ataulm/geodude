package uk.ac.rhul.cs.dice.golem.conbine.agent.williams;

import java.util.List;

import uk.ac.rhul.cs.dice.golem.conbine.agent.DialogueState;
import uk.ac.soton.ecs.gp4j.bmc.GaussianProcessRegressionBMC;
import Jama.Matrix;

public interface DialogueStateWilliams extends DialogueState {
    /**
     * Returns this agent's last bid.
     * 
     * @return bid  the agent's last bid
     */
    public double getMyLastBid();
    public void updateMyLastBid(double bid);
    
    public double getOpponentsLastBid();    
    public void updateOpponentsLastBid(double bid);
    
    public List<Double> getOpponentTimes();
    
    public List<Double> getOpponentUtilities();
    
    public double getBestReceivedBid();    
    public void updateBestReceivedBid(double bestReceivedBid);
    
    public double getLastRegressionTime();    
    public void updateLastRegressionTime(double lastRegressionTime);
    
    public double getLastRegressionUtility();    
    public void updateLastRegressionUtility(double lastRegressionUtility);
    
    public int getLastTimeSlot();    
    public void updateLastTimeSlot(int lastTimeSlot);
    
    public double getMaxUtility();    
    public void setMaxUtility(double maxUtility);
    
    public double getMaxUtilityInTimeSlot();    
    public void setMaxUtilityInTimeSlot(double maxUtilityInTimeSlot);
    
    public double getPreviousTargetUtility();    
    public void updatePreviousTargetUtility(double previousTargetUtility);
    
    public GaussianProcessRegressionBMC getRegression();
    public void setRegression(GaussianProcessRegressionBMC regression);
    
    public Matrix getUtility();
    public void setUtility(Matrix utility);
    
    public Matrix getMeans();
    public void setMeans(Matrix means);
    
    public Matrix getVariances();
    public void setVariances(Matrix variances);
    
    /**
     * Returns the value of the committed offer.
     * 
     * If the dialogue is not in a committed state, this defaults to 0.
     * 
     * @return
     */
    public double getCommittedOffer();
    /**
     * Sets the value of the offer at the point of commitment.
     * 
     * @param committedOffer
     */
    public void setCommittedOffer(double committedOffer);
    
    public double getProbabilityOpponentWillStay();
}
