package uk.ac.rhul.cs.dice.golem.container;

import uk.ac.rhul.cs.dice.golem.action.Event;
import uk.ac.rhul.cs.dice.golem.entity.Entity;
import uk.ac.rhul.cs.dice.golem.physics.Physics;

/**
 * A Container is a logical collection of entities in OpenGOLEM, governed by a
 * {@link Physics} object.
 * 
 * @author ataulm
 * 
 */
public interface Container extends Environment {
    /**
     * Register an {@link Entity} within the Container.
     *
     * When an entity is made "present", other entities can interact with it.
     *
     * @param entity
     *            the entity to register with the Container.
     */
    public void makePresent(Entity entity);

    /**
     * Unregister an {@link Entity} from the Container.
     * 
     * @param id  ID of the Entity to remove
     */
    public void removeEntity(String id);

    public Entity getEntity(String id);

    /**
     * Delete all events in container history.
     * 
     * @return cleared true if the specified collection was cleared
     */
    public boolean clearHistory();
    
    /**
     * Deletes Container history, removes all {@link Entity} objects in the
     * Container, removes all subscriptions.
     * 
     * @return cleared true if all specified collections have been cleared
     */
    public boolean clearAll();

    /**
     * When a proposed {@link uk.ac.rhul.cs.dice.golem.action.Event} is successfully asserted into the history
     * of the {@link Container}, it's as if that Event occurred at the time
     * specified.
     *
     * @param event
     *            the event to be asserted
     * @return success true if the proposed Event was successfully asserted
     */
    public boolean assertEventInHistory(Event event);
}
