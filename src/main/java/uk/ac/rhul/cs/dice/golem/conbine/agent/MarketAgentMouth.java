package uk.ac.rhul.cs.dice.golem.conbine.agent;

import uk.ac.rhul.cs.dice.golem.action.AbstractEffector;
import uk.ac.rhul.cs.dice.golem.agent.AgentBody;
import uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType;

public class MarketAgentMouth extends AbstractEffector {

    public MarketAgentMouth(String id, AgentBody context) {
        super(id, context);

        addType(ConbineActionType.ACCEPT.toString());
        addType(ConbineActionType.DECOMMIT.toString());
        addType(ConbineActionType.EXIT.toString());
        addType(ConbineActionType.COMMIT.toString());
        addType(ConbineActionType.OFFER.toString());
        addType(ConbineActionType.ANNOUNCE_ENTRANCE.toString());
        addType(ConbineActionType.EXIT_ALL.toString());
    }
}
