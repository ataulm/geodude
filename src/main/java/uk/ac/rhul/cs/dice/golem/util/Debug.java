package uk.ac.rhul.cs.dice.golem.util;

/**
 * Provides various levels of debug, to filter Log messages and UI
 * modifications.
 * 
 * This class is currently only used during application development. It is left
 * to the developer to wrap their debug code in these flags.
 * 
 * TODO: Thought should be given to how the end-user could help application
 * developers by allowing the user to set flags, and send the developer a copy
 * of the output, if they encounter an error.
 * 
 * @author ataulm
 * 
 */
public class Debug {

    /**
     * Set this flag show UI components with distinctly coloured backgrounds
     */
    public final static boolean SHOW_COLORS_UI = false;

    /**
     * Flag to show error messages
     */
    public final static boolean SHOW_ERROR_LOGS = true;

    /**
     * Flag to show warning messages
     */
    public final static boolean SHOW_WARNING_LOGS = true;

    /**
     * Flag to show important messages about operations
     */
    public final static boolean SHOW_INFO_LOGS = true;

    /**
     * Flag to show finely granular messages about operations
     */
    public final static boolean SHOW_MICRO_LOGS = false;
}
