package uk.ac.rhul.cs.dice.golem.conbine.agent.faratin;

import java.util.List;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.agent.AgentBrain;
import uk.ac.rhul.cs.dice.golem.conbine.action.NegotiationAction;
import uk.ac.rhul.cs.dice.golem.conbine.agent.AbstractSellerAgent;
import uk.ac.rhul.cs.dice.golem.conbine.agent.AgentParameters;
import uk.ac.rhul.cs.dice.golem.conbine.agent.DefaultDialogueState;
import uk.ac.rhul.cs.dice.golem.conbine.agent.OfferHistory;

@SuppressWarnings("serial")
public abstract class AbstractFaratinWithOpponentHistorySeller extends AbstractSellerAgent {
    public AbstractFaratinWithOpponentHistorySeller(AgentBrain brain, AgentParameters params, String product) {
        super(brain, params, product);        
    }
    
    @Override
	protected List<Action> sendCounterOffer(NegotiationAction offer,
			double counterOffer) {
		DefaultDialogueStateFratinSellerOfferHistory dialogueState =
				(DefaultDialogueStateFratinSellerOfferHistory) getDialogues().get(offer.getDialogueId());
		
		dialogueState.updateMyLastBid(counterOffer);
		
		return super.sendCounterOffer(offer, counterOffer);
	}
    
    @Override
    protected DefaultDialogueState makeNewDialogueState(NegotiationAction offer) {
		DefaultDialogueState state = new DefaultDialogueStateFratinSellerOfferHistory(
                offer.getDialogueId(),
                offer.getProtocol(),
                offer.getReplyToId(),
                offer.getProductId()); 
        
		((OfferHistory) state).addOpponentBid(Double.parseDouble(offer.getValue()));
		
		return state;
	}
}
