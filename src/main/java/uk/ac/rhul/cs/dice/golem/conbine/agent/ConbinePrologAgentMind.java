package uk.ac.rhul.cs.dice.golem.conbine.agent;

import static uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType.NOTIFY_ABOUT_CHANGE_IN_DEMAND_SUPPLY_RATIO;
import static uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType.NOTIFY_ABOUT_CHANGE_IN_NUMBER_OF_COMPETITORS;
import static uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType.NOTIFY_ABOUT_NEW_SELLER;
import static uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType.valueOf;

import java.util.ArrayList;
import java.util.List;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.action.Percept;
import uk.ac.rhul.cs.dice.golem.agent.AgentBrain;
import uk.ac.rhul.cs.dice.golem.agent.PrologAgentMind;
import uk.ac.rhul.cs.dice.golem.conbine.action.AnnounceEntranceAction;
import uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType;
import uk.ac.rhul.cs.dice.golem.conbine.action.ExitAllAction;
import uk.ac.rhul.cs.dice.golem.conbine.action.NegotiationAction;
import uk.ac.rhul.cs.dice.golem.conbine.action.NotifyAboutChangeInDemandSupplyRatioAction;
import uk.ac.rhul.cs.dice.golem.conbine.action.NotifyAboutChangeInNumberOfCompetitors;
import uk.ac.rhul.cs.dice.golem.conbine.action.NotifyAboutNewSellerAction;
import alice.tuprolog.MalformedGoalException;
import alice.tuprolog.SolveInfo;

@SuppressWarnings("serial")
public class ConbinePrologAgentMind extends PrologAgentMind implements MarketAgent {
    private final AgentType type;
    private final int initialPrice;
    private final int reservationPrice;
    private final int deadlineInSeconds;
    private final String productId;
    private boolean announcedArrival;

    public ConbinePrologAgentMind(AgentType type, AgentBrain brain,
            String theoryFilePath, AgentParameters params, String productId) {
        super(theoryFilePath, brain);

        this.type = type;
        if (type.equals(AgentType.BUYER)) {
            this.initialPrice = params.get(AgentParameters.BUYER_INITIAL);
            this.reservationPrice = params
                    .get(AgentParameters.BUYER_RESERVATION);
        } else {
            this.initialPrice = params.get(AgentParameters.SELLER_INITIAL);
            this.reservationPrice = params
                    .get(AgentParameters.SELLER_RESERVATION);
        }

        this.deadlineInSeconds = params.get(AgentParameters.DEADLINE);
        this.productId = productId;

        try {
            assertInitialFacts();
        } catch (MalformedGoalException e) {
            log("Unable to assert initial facts.");
            e.printStackTrace();
        }
        
        setLogLevel(MICRO);
    }

    protected void assertInitialFacts() throws MalformedGoalException {
        String resPrice = "sit(ao, neg, 0), rp(" + reservationPrice + ")";
        String iniPrice = "sit(ao, neg, 0), ip(" + initialPrice + ")";
        String deadline = "sit(ao, neg, 0), deadline(" + deadlineInSeconds
                * 1000 + ")";
        String product = "sit(ao, neg, 0), name(i1, " + productId + ")";
        String timeJoinedMarket = "sit(ao, neg, 0), startTime(" + System.currentTimeMillis() + ")";

        super.assertInitially(resPrice);
        super.assertInitially(iniPrice);
        super.assertInitially(deadline);
        super.assertInitially(product);
        super.assertInitially(timeJoinedMarket);
    }

    /**
     * Converts a {@link Percept} into a form that is ready to be asserted in
     * the Prolog mind.
     * 
     * ConBiNe prolog mind are only notified of {@link NegotiationAction} and
     * {@link NotifyAboutNewSellerAction} percepts.
     */
    @Override
    protected String convertPerceptToTerm(Percept percept) {
        Action action = percept.getPerceptContent();
        long time = percept.getTimeStamp();
        String actionStruct;
        String situationStruct;
        String happensStruct;
        String typeStruct;

        if (action instanceof NotifyAboutNewSellerAction) {
            NotifyAboutNewSellerAction notification = (NotifyAboutNewSellerAction) action;

            typeStruct = NOTIFY_ABOUT_NEW_SELLER.toString()
                    + "(" + notification.getNewSellerId() + ")";

            situationStruct = "sit(ao, " + notification.getDialogueId() + ", "
                    + time + ")";

        } else if (action instanceof NotifyAboutChangeInDemandSupplyRatioAction) {
        	NotifyAboutChangeInDemandSupplyRatioAction notification = (NotifyAboutChangeInDemandSupplyRatioAction) action;
        	typeStruct = NOTIFY_ABOUT_CHANGE_IN_DEMAND_SUPPLY_RATIO.toString()
                    + "(" + notification.getRatio().value() + ")";

            situationStruct = "sit(ao, neg, " + time + ")";
        	
        } else if (action instanceof NotifyAboutChangeInNumberOfCompetitors) {
        	NotifyAboutChangeInNumberOfCompetitors notification = (NotifyAboutChangeInNumberOfCompetitors) action;
        	typeStruct = NOTIFY_ABOUT_CHANGE_IN_NUMBER_OF_COMPETITORS.toString()
                    + "(" + notification.getNumberOfCompetitors() + ")";

            situationStruct = "sit(ao, neg, " + time + ")";
        	
        } else {
            NegotiationAction opponentAction = (NegotiationAction) action;
            typeStruct = opponentAction.getActionStruct();

            situationStruct = "sit(" + opponentAction.getProtocol() + ", "
                    + opponentAction.getDialogueId() + ", " + time + ")";

        }

        actionStruct = "action(" + typeStruct + "), " + situationStruct;
        happensStruct = "happens(" + actionStruct + ")";
        return happensStruct;
    }

    /**
     * Converts Actions returned from the Prolog interpreter into an
     * {@link Action}.
     * 
     * The prolog mind in ConBiNe will only be returning actions of type
     * {@link NegotiationAction}.
     */
    @Override
    protected Action convertTermToAction(String action) {
    	log("Agent returned action: " + action);
    	
    	Action convertedAction = null;
    	
    	String type = action.substring(0, action.indexOf("("));
    	String params = action.substring(type.length() + 1, action.length() - 1);
    	String[] terms = params.split(",");
    	        
    	if (type.equals(ConbineActionType.OFFER.toString()) 
    			|| type.equals(ConbineActionType.COMMIT.toString())
    			|| type.equals(ConbineActionType.DECOMMIT.toString())
    			|| type.equals(ConbineActionType.ACCEPT.toString())
    			|| type.equals(ConbineActionType.EXIT.toString())) {
    		
    		NegotiationAction.Builder builder = new NegotiationAction.Builder()
    			.setType(valueOf(type.toUpperCase()))
    			.setProtocol(terms[NegotiationAction.PROTOCOL])
				.setDialogueId(terms[NegotiationAction.DIALOGUE_ID])
				.setProductId(terms[NegotiationAction.PRODUCT_ID])
				.setRecipient(terms[NegotiationAction.REPLY_TO_ID])
				.setReplyToId(getBrain().getAgentId());
    		
    		if (terms.length - 1 == NegotiationAction.VALUE) {
    			builder.setValue(terms[NegotiationAction.VALUE]);
    		}
    		
    		return builder.build();
    	} else if (type.equals(ConbineActionType.EXIT_ALL.toString())) {
    		return new ExitAllAction.Builder()
					.setAgentId(getBrain().getAgentId())
					.setAgentType(this.type.toString())
					.setProductId(terms[ExitAllAction.PRODUCT_ID])
					.build();
    	}
        return convertedAction;
    }

    /**
     * Announces arrival to the market once, then does as
     * {@link PrologAgentMind#executeStep()}.
     * 
     */
    @Override
    public List<Action> executeStep() throws Exception {
        List<Action> actionsToPerform = new ArrayList<>();
        if (!announcedArrival) {
            Action announce = new AnnounceEntranceAction.Builder()
                    .setAgentId(getBrain().getAgentId())
                    .setAgentType(type.toString()).setProductId(productId)
                    .build();

            announcedArrival = true;
            actionsToPerform.add(announce);
            return actionsToPerform;
        }
        return super.executeStep();
    }

    @Override
    protected SolveInfo requestListOfActions(String term)
            throws MalformedGoalException {
    	return super.requestListOfActions("select_all(Actions, "
               + System.currentTimeMillis() + ").");
    }

    @Override
    public AgentType getType() {
        return type;
    }

}
