package uk.ac.rhul.cs.dice.golem.container;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.action.Event;
import uk.ac.rhul.cs.dice.golem.action.Sensor;
import uk.ac.rhul.cs.dice.golem.agent.AgentBody;
import uk.ac.rhul.cs.dice.golem.entity.Entity;

public interface Environment extends Entity {
    /**
     * Attempt to assert the {@link Action} in the environment as an
     * {@link Event}.
     * 
     * @param agentBody
     *            the {@link Entity} responsible for the Action
     * @param action
     *            the Action to be asserted
     */
    public void attempt(Entity entity, Action action);

    /**
     * Subscribe to a particular action type..
     * 
     * When an {@link Action} of the specified action type occurs, and the
     * Action is a broadcast action (doesn't specify a particular recipient) the
     * Environment will notify the specified {@link AgentBody}.
     * 
     * @param agent
     *            the agent subscribing to the action type
     * @param sensor
     *            the sensor the agent is subscribing to receive notifications
     */
    public void subscribe(AgentBody agent, Sensor sensor);
}
