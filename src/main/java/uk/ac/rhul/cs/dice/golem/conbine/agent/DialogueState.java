package uk.ac.rhul.cs.dice.golem.conbine.agent;

/**
 * Represents the state of a basic dialogue.
 * 
 * @author ataulm
 *
 */
public interface DialogueState {

	/**
	 * Returns the dialogue ID.
	 * 
	 * @return
	 */
	public String getId();

	/**
	 * Returns the opponent ID.
	 * 
	 * @return
	 */
	public String getOpponent();

	/**
	 * Returns the product ID, or name.
	 * 
	 * @return
	 */
	public String getProduct();

	/**
	 * Returns the name of the protocol being used in this dialogue.
	 * 
	 * @return
	 */
	public String getProtocol();

	/**
	 * Checks to see if the dialogue is in a committed state.
	 * 
	 * @return
	 */
	public boolean isCommitted();
	
	/**
	 * Sets the dialogue to a committed state.
	 * 
	 */
	public void setCommitted();
}