package uk.ac.rhul.cs.dice.golem.gui;

import javax.swing.JPanel;

import uk.ac.rhul.cs.dice.golem.application.Application;

/**
 * Forms the GUI for an instance of an application in GOLEM.
 * 
 * @author ataulm
 * 
 */
public interface ApplicationTab {
    /**
     * Perform as much cleanup as possible before the tab is removed from the
     * user interface.
     */
    void destroy();

    JPanel getContentPanel();

    /**
     * Return the application context of this tab.
     */
    Application getContext();

    void revalidate();

    /**
     * Initialise the contents (components) of the tab.
     */
    void setup();
}
