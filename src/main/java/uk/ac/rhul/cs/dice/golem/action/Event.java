package uk.ac.rhul.cs.dice.golem.action;

public final class Event {
    private final String initiatorId;
    private final Action action;
    private final long timestamp;

    public Event(String initiatorId, Action action, long timestamp) {
        this.initiatorId = initiatorId;
        this.action = action;
        this.timestamp = timestamp;
    }

    public Action getAction() {
        return action;
    }

    public String getActionType() {
        return action.getActionType();
    }

    public String getInitiatorId() {
        return initiatorId;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
