package uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;

import uk.ac.rhul.cs.dice.golem.conbine.app.ConbineVariable;
import uk.ac.rhul.cs.dice.golem.conbine.app.ValueBucket;

/**
 * Clicker is used to iterate through possible combinations of
 * {@link ConbineVariable ConbineVariables}.
 * 
 * A combination is defined as a set of {@link ValueBucket ValueBuckets}, where the size of
 * the set is equal to the number of ConbineVariables in this Clicker, and the
 * set contains exactly one ValueBucket per ConbineVariable. The intention is to
 * get every possible permutation.
 * 
 * It's called Clicker because the algorithm to find the next permutation is
 * similar to how mechanical clicker-counters work.
 * 
 * @author ataulm
 * 
 */
public class Clicker {
    private ConbineVariable[] variables;
    private int[] currentIndex;
    private int currentCombo;
    private Map<String, Integer> mapIdToIndexer;
    private boolean reachedEnd;
    private static final int START_INDEX = 1;
    private static final int END_AT_INFINITY = -1;
    private final int endAt;

    public Clicker(ConbineVariable... variables) {
        this(START_INDEX, END_AT_INFINITY, variables);
    }

    public Clicker(int start, int endAt, ConbineVariable... variables) {
        if (start < START_INDEX) {
            throw new IllegalArgumentException(
                    "Combination to start with must be greater than 0.");
        }

        if (endAt < START_INDEX && endAt != END_AT_INFINITY) {
            throw new IllegalArgumentException(
                    "Combination to end on (inclusive) must be greater than 0. If it's set to -1 (default), it will " +
                            "run all the combinations.");
        }

        this.endAt = endAt;
        this.variables = variables;
        currentIndex = new int[variables.length];
        mapIdToIndexer = new HashMap<>();

        for (int i = 0; i < variables.length; i++) {
            mapIdToIndexer.put(variables[i].getId(), i);
        }

        skipTo(start);
    }

    /**
     * Advances to the next permutation of variables.
     *
     * Returns true if the advancement was successful, otherwise false (all permutations covered).
     *
     * @return advancedToNewSet  the success of the attempt to advance to the next set of variables
     */
    public final boolean click() {
        if (!reachedEnd) {
            if (increment(variables.length - 1)) {
                currentCombo++;
                if (endAt != END_AT_INFINITY && currentCombo > endAt) {
                    return false;
                }
                return true;
            }
            reachedEnd = true;
        }
        return false;
    }

    public int getCurrentCombination() {
        return currentCombo;
    }

    public int getCurrentIndex(ConbineVariable variable) {
        if (mapIdToIndexer.containsKey(variable.getId())) {
            return currentIndex[mapIdToIndexer.get(variable.getId())];
        }

        throw new IllegalArgumentException(variable.getId()
                + " is not known in this Clicker.");
    }

    /**
     * Returns the current combination of {@link ValueBucket}s from the
     * {@link ConbineVariable}s in this Clicker.
     * 
     * @return
     */
    public String[] getVariableBuckets() {
        String[] currentCombination = new String[variables.length];
        for (int i = 0; i < currentCombination.length; i++) {
            currentCombination[i] = variables[i].get(
                    getCurrentIndex(variables[i])).getId();
        }

        return currentCombination;
    }

    /**
     * Returns a list of the variables used in this clicker.
     * 
     * @return
     */
    public String[] getVariableNames() {
        String[] variableNames = new String[variables.length];
        for (int i = 0; i < variableNames.length; i++) {
            variableNames[i] = variables[i].getId();
        }

        return variableNames;
    }

    /**
     * Increments the index of the specified column.
     * 
     * If the column cannot be incremented any further, reset index (for this
     * column) to 0, and try to increment the column to the left (like a
     * clicker-counter).
     * 
     * @param index
     * @return
     */
    private boolean increment(int index) {
        // Trying to access the non-existent column _before_ the first column
        if (index < 0) {
            return false;
        }
        
        ConbineVariable variable = variables[index];
        int bucketIndex = currentIndex[index];

        // if you can't increment this digit any further, reset to 0 and try to
        // increment the digit to the left
        if (bucketIndex == variable.size() - 1) {
            currentIndex[index] = 0;
            return increment(index - 1);
        }

        // otherwise increment this digit, and return
        currentIndex[index]++;
        return true;
    }

    /**
     * Prints the number of the combination with the combination set values to a
     * file.
     * 
     * Given that there are a fixed number of combinations (of variables'
     * {@link ValueBucket}s) using {@link Clicker}, it's possible to split an
     * experiment up onto several machines, by indicating the combination at
     * which to begin from (TODO: and end at).
     * 
     * The Clicker calculates all possible combinations, and depending on the
     * start value (TODO: and stop value), prints the combination of variables
     * and the number of that combination (where the number represents the order
     * that combination would be calculated in the complete list).
     * 
     * @throws IOException
     */
    public void printCombinationMapToFile(Path output) throws IOException {
        output.toFile().getParentFile().mkdirs();
        BufferedWriter writer = Files.newBufferedWriter(output,
                StandardCharsets.UTF_8, StandardOpenOption.TRUNCATE_EXISTING,
                StandardOpenOption.CREATE, StandardOpenOption.WRITE);

        writer.write("combo:::");

        for (String name : getVariableNames()) {
            writer.write(name + ":::");
        }

        writer.write("\n");

        do {
            writer.write(getCurrentCombination() + ":::");

            for (String valueBucket : getVariableBuckets()) {
                writer.write(valueBucket + ":::");
            }

            writer.write("\n");
        } while (click());

        writer.close();
        reset();
    }

    public void reset() {
        reachedEnd = false;
        currentCombo = START_INDEX;
        for (int i = 0; i < currentIndex.length; i++) {
            currentIndex[i] = 0;
        }
    }

    /**
     * Iterate through the clicker until the specified combination is reached.
     * 
     * @param iteration
     */
    public void skipTo(int iteration) {
        for (int i = 1; i < iteration; i++) {
            if (reachedEnd) {
                break;
            }
            click();
        }
        currentCombo = iteration;
    }
}
