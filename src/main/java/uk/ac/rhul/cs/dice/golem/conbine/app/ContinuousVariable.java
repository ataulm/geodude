package uk.ac.rhul.cs.dice.golem.conbine.app;

/**
 * Represents a variable where the value can be any within the specified range,
 * grouped in buckets (of various ranges).
 * 
 * @author ataulm
 * 
 */
public class ContinuousVariable extends ConbineVariable {
    public ContinuousVariable(String id) {
        super(id);
    }

    /**
     * As {@link ConbineVariable#addBucket(ValueBucket)} but forces
     * {@link ContinuousValueBucket}.
     * 
     * @param bucket
     *            a ContinuousValueBucket with a specified min and max value
     */
    public void addBucket(ContinuousValueBucket bucket) {
        super.addBucket(bucket);
    }
}