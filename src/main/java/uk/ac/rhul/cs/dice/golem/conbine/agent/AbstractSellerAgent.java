package uk.ac.rhul.cs.dice.golem.conbine.agent;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.agent.AgentBrain;
import uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType;
import uk.ac.rhul.cs.dice.golem.conbine.action.NegotiationAction;

/**
 * Covers basic setup for a {@link MarketAgent} of type seller.
 * 
 * Handles notifications from infrastructure agents. 
 *
 */
@SuppressWarnings("serial")
public abstract class AbstractSellerAgent extends AbstractMarketAgent {
	protected AbstractSellerAgent(AgentBrain brain, AgentParameters params,
			String product) {
		super(brain, params, product, AgentType.SELLER);
	}
	
	protected AbstractSellerAgent(AgentBrain brain, AgentParameters params,
			String product, Map<String, DialogueState> dialogues) {
		super(brain, params, product, AgentType.SELLER, dialogues);
	}
	
	@Override
	public List<Action> executeStep() throws Exception {
		List<Action> actionsToPerform = new ArrayList<>();
	    
		actionsToPerform.addAll(super.executeStep());
		
	    return actionsToPerform;
	}
		
	@Override
	protected List<Action> decideAction(Action action) {
		List<String> exitFromDialogues = new ArrayList<>();
		// check deadline
		for (DialogueState dialogue : getDialogues().values()) {
			if (System.currentTimeMillis() > (((DialogueStateSeller) dialogue).getStart() + getDeadline())) {
				exitFromDialogues.add(dialogue.getId());
			}
		}
		
		for (String dialogue : exitFromDialogues) {
			super.exitFromDialogue(dialogue);
		}
		
		if (action != null) {
			if (action.getActionType().equals(ConbineActionType.COMMIT.toString())) {
				return decideActionBasedOnCommit((NegotiationAction) action);			
			}	
		}
		
		// call super to handle default actions
		return super.decideAction(action);
	}
	
	@Override
	protected double getUtility(double offer) {
		return (offer - getReservationPrice()) / (getInitialPrice() - getReservationPrice());
	}
	
	/**
	 * Decide how to respond when the opponent commits.
	 * 
	 * Default implementation does nothing (waits).
	 * 
	 * @param commit
	 * @return
	 */
	protected List<Action> decideActionBasedOnCommit(NegotiationAction commit) {
		String id = commit.getDialogueId();
		log("Received commit for dialogue: " + id);
        if (getDialogues().get(id) != null) {
            getDialogues().get(id).setCommitted();
        }
		return new ArrayList<>();
	}
	
	@Override
	protected List<Action> decideActionBasedOnOffer(NegotiationAction offer) {		
		// if it's a first offer, add reference to dialogue
		if (!getDialogues().containsKey(offer.getDialogueId())) {
        	getDialogues().put(offer.getDialogueId(), makeNewDialogueState(offer));	        		
	    }
		
		return new ArrayList<>();
	}
	
	@Override
	protected DialogueState makeNewDialogueState(NegotiationAction offer) {
		return new DefaultDialogueStateSeller(
				offer.getDialogueId(),
				offer.getProtocol(),
				offer.getReplyToId(),
				offer.getProductId());
	}
	
	/**
	 * When the seller receives an accept message from a buyer, it ends the negotiation.
	 */
	@Override
	protected List<Action> decideActionBasedOnAccept(NegotiationAction accept) {
		logMicro("received accept. Negotiation successful.");
		getDialogues().remove(accept.getDialogueId());
		return new ArrayList<>();
	}
}
