package uk.ac.rhul.cs.dice.golem.conbine.agent.faratin;

import java.util.List;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.agent.AgentBrain;
import uk.ac.rhul.cs.dice.golem.conbine.action.NegotiationAction;
import uk.ac.rhul.cs.dice.golem.conbine.agent.AbstractSellerAgent;
import uk.ac.rhul.cs.dice.golem.conbine.agent.AgentParameters;
import uk.ac.rhul.cs.dice.golem.conbine.agent.DialogueStateSeller;

@SuppressWarnings("serial")
public class FaratinTimeDependentBoulwareSeller extends AbstractSellerAgent {
	public FaratinTimeDependentBoulwareSeller(AgentBrain brain,
			AgentParameters params, String product) {
		super(brain, params, product);
	}	

	@Override
	protected List<Action> decideActionBasedOnOffer(NegotiationAction offer) {
		List<Action> actionsToPerform = super.decideActionBasedOnOffer(offer);
		
		double utilityOpponentOffer = getUtility(Double.parseDouble(offer.getValue()));
		double counterOffer = generateNextOffer(offer.getDialogueId());
		double utilityMyCounterOffer = getUtility(counterOffer);
		
		if (utilityOpponentOffer >= utilityMyCounterOffer) {
			actionsToPerform.addAll(super.acceptOpponentOffer(offer));
		} else {
			actionsToPerform.addAll(super.sendCounterOffer(offer, counterOffer));
		}
		
		return actionsToPerform;
	}
	
	
	@Override
	protected double generateNextOffer(String dialogueId) {
		long startTime = ((DialogueStateSeller) getDialogues().get(dialogueId)).getStart();
		
		return getInitialPrice() - 
				(getInitialPrice() - getReservationPrice()) * (Math.pow(getNormalisedTime(startTime) * 1000, 0.5));
	}
}
