package uk.ac.rhul.cs.dice.golem.application;

import java.io.File;

import javax.swing.JTabbedPane;

import uk.ac.rhul.cs.dice.golem.gui.GolemGUI;
import uk.ac.rhul.cs.dice.golem.util.Logger;

/**
 * Implements the methods required by the Application API.
 * 
 * Application developers can use this class as well as the sample
 * PacketWorldApplication as a reference when implementing the Application
 * interface.
 * 
 * @author ataulm
 * @see uk.ac.rhul.cs.dice.golem.plugins.packetworld.basicapp.PacketWorldApplication
 * 
 */
public abstract class AbstractApplication implements Application {

    /**
     * Name for the application. This is the string GOLEM will query to get the
     * name for the application to show to the end-user.
     */
    private final String name;
    /**
	 * 
	 */
    private final String appDir;

    /**
     * Reference to the main GOLEM window.
     */
    private GolemGUI golemGui;

    /**
     * Absolute path to the directory for this application's data
     */
    private String pathAppDir;

    /**
     * Constructor
     * 
     * @param name
     *            Application name
     * @param appDir
     *            Name of the application's data directory (under AppData)
     */
    public AbstractApplication(String name, String appDir) {
        if (name == null)
            Logger.w("DefaultApplication", "Application name not set");
        else
            Logger.i(this, "name: " + name);
        if (appDir == null)
            Logger.w("DefaultApplication", "Data directory not set");

        this.name = name;
        this.appDir = appDir;
    }

    /**
     * Creates application data directory.
     * 
     * @return result The result from trying to create the directory as
     *         specified from the constants given in {@link Application}:
     *         {@link Application#APP_DIR_CREATED},
     *         {@link Application#APP_DIR_EXISTS} and
     *         {@link Application#APP_DIR_NOT_CREATED}.
     */
    @Override
    public int createAppDir() {
        String absoluteAppDirName = golemGui.getBase().getAppDataDir()
                + File.separator + appDir;

        File dir = new File(absoluteAppDirName);

        Logger.micro(this, "Checking if dir: " + dir.toPath() + " exists.");
        if (!dir.exists()) {
            Logger.i(this, "Creating dir: " + dir.toPath());
            if (dir.mkdirs())
                return Application.APP_DIR_CREATED;
            else
                return Application.APP_DIR_NOT_CREATED;
        }
        return Application.APP_DIR_EXISTS;
    }

    @Override
    public String getAppDir() {
        return pathAppDir;
    }

    @Override
    public JTabbedPane getDesktopManager() {
        return golemGui.getDesktopManager();
    }

    @Override
    public GolemGUI getGolemGui() {
        return golemGui;
    }

    @Override
    public String getName() {
        return name;
    }

    /**
     * Stores a reference of the main window, and initialises the path to the
     * data folder.
     */
    @Override
    public void init(GolemGUI golemGui) {
        this.golemGui = golemGui;
        if (appDir != null && appDir.length() > 0) {
            pathAppDir = golemGui.getBase().getAppDataDir() + File.separator
                    + appDir;
            createAppDir();
        }
        postInit();
    }

    /**
     * Optional, post-initialisation tasks on a per-application basis should be
     * executed here
     */
    protected abstract void postInit();

    @Override
    public void setGolemGui(GolemGUI golemGui) {
        this.golemGui = golemGui;
    }
}
