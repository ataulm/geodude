package uk.ac.rhul.cs.dice.golem.conbine.app;

public interface ConbineUserInput {
    public ContinuousVariable getBuyerInitial();

    public ContinuousVariable getBuyerReservation();

    public String[] getBuyerTypePool();

    public ContinuousVariable getDeadline();

    public DiscreteVariable getMarketChangeRateDensity();

    public DiscreteVariable getMarketChangeRateRatio();

    public DiscreteVariable getMarketChangeTime();

    public String[] getSelectedBuyers();

    public ContinuousVariable getSellerInitial();

    public ContinuousVariable getSellerReservation();

    public String[] getSellerTypePool();
    
    public String getExperimentLabel();
    
    public int getRunsPerCombination();
}
