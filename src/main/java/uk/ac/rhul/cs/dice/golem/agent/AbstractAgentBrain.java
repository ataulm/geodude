package uk.ac.rhul.cs.dice.golem.agent;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.action.Percept;
import uk.ac.rhul.cs.dice.golem.action.Sensor;
import uk.ac.rhul.cs.dice.golem.util.Logger;

@SuppressWarnings("serial")
public abstract class AbstractAgentBrain implements AgentBrain, Serializable {

    private static final long THREAD_DELAY_MILLIS = 200;
    private long threadDelay = THREAD_DELAY_MILLIS;

    private AgentMind mind;
    private AgentBody body;

    private boolean agentRunning;
    private final Queue<Percept> perceptions;

    public AbstractAgentBrain() {
        perceptions = new ConcurrentLinkedQueue<>();
    }

    @Override
    public String getAgentId() {
        return body.getId();
    }



    @Override
    public List<Percept> getAllPerceptions() {
    	synchronized (perceptions) {
			List<Percept> percepts = new ArrayList<>();
			
			for (Percept percept : perceptions) {
				if (percept != null) percepts.add(percept);
			}
			
			perceptions.clear();
	        return percepts;
    	}
    }

    /**
     * Returns a reference to the {@link AgentBody} associated with this
     * {@link AgentBrain}.
     * 
     * @return body the IAgentBody associated with this AgentBrain
     */
    public AgentBody getBody() {
        return body;
    }

    /**
     * Returns a reference to the {@link AgentMind} associated with this
     * {@link AgentBrain}.
     * 
     * @return mind the mind associated with this brain
     */
    public AgentMind getMind() {
        return mind;
    }

    @Override
    public Percept getPerception() {
        return perceptions.poll();
    }

    @Override
    public void onSensorHasPercept(Sensor sensor) {
    	synchronized(perceptions) {
    	    perceptions.add(sensor.getPerception());
    	}
    }

    /**
     * Sets a reference to the body associated with this {@link AgentBrain}.
     * 
     * @param body
     *            the {@link AgentBody} associated with this AgentBrain
     */
    @Override
    public void setBody(AgentBody body) {
        this.body = body;
        body.setBrain(this);
    }

    /**
     * Sets a reference to an {@link AgentMind} with this {@link AgentBrain}.
     * 
     * It suspends the execution of the agent during the setting of the mind, if
     * it was alive prior to the start of the operation.
     * 
     * @param mind
     *            the mind to associate
     */
    @Override
    public void setMind(AgentMind mind) {
        boolean wasAlive = agentRunning;
        if (wasAlive)
            suspendCycle();
        this.mind = mind;
        if (wasAlive)
            resumeCycle();
    }

    protected boolean isAgentRunning() {
        return agentRunning;
    }

    protected void setAgentRunning(boolean agentRunning) {
        this.agentRunning = agentRunning;
    }

    @Override
    public void setThreadDelay(long threadDelay) {
        this.threadDelay = threadDelay;
    }



    /**
     * Driver behind the agent cycle, this method tells the {@link AgentMind} to
     * execute a cycle step (perceive, reason, act).
     */
    protected void stepForward() {
        if (mind != null) {
            try {
                List<Action> actions = mind.executeStep();
                if (actions != null) {
                    for (Action action : actions) {
                        act(action);
                    }
                }
                Thread.sleep(threadDelay);
            } catch (InterruptedException e) {
                // an interrupt is used to stop an Agent ( when removed by MarketController)
            } catch (Exception e) {
                Logger.e(this, "mind.executeStep() threw an Exception");
                e.printStackTrace();
            }
        }
    }



    @Override
    public void act(Action action) throws Exception {
        if (action != null)
            getBody().act(action);
    }
}
