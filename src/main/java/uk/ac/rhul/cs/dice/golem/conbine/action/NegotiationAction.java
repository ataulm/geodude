package uk.ac.rhul.cs.dice.golem.conbine.action;

import org.javatuples.Quartet;
import org.javatuples.Quintet;
import org.javatuples.Tuple;

import uk.ac.rhul.cs.dice.golem.action.Action;

/**
 * A NegotiationAction is the implementation of a subset of the types listed in
 * {@link ConbineActionType}:
 * 
 * - {@link ConbineActionType#OFFER} - {@link ConbineActionType#COMMIT} -
 * {@link ConbineActionType#DECOMMIT} - {@link ConbineActionType#ACCEPT} -
 * {@link ConbineActionType#EXIT}
 * 
 * @author ataulm
 * 
 */
public class NegotiationAction extends Action {
    public static class Builder {
        private String recipientId;
        private String protocol;
        private String dialogueId;
        private String replyToId;
        private String productId;
        private String value;
        private ConbineActionType type;

        public NegotiationAction build() {
            if (type == null
                    ^ (!type.equals(ConbineActionType.OFFER)
                            ^ !type.equals(ConbineActionType.COMMIT)
                            ^ !type.equals(ConbineActionType.DECOMMIT)
                            ^ !type.equals(ConbineActionType.ACCEPT) ^ !type
                                .equals(ConbineActionType.EXIT))) {

                throw new IllegalStateException(
                        "NegotiationActions must be one of the following types: offer, commit, decommit, accept or exit, as specified in ");
            }

            if (protocol == null || protocol.length() == 0) {
                throw new IllegalStateException("Protocol type must be set.");
            }

            if (dialogueId == null || dialogueId.length() == 0) {
                throw new IllegalStateException(
                        "Dialogue id must be set, in case there are multiple dialogues with the same recipient.");
            }

            if (replyToId == null || replyToId.length() == 0) {
                throw new IllegalStateException(
                        "A reply-to id must be set, otherwise the recipient won't know who to respond to.");
            }

            if (productId == null || productId.length() == 0) {
                throw new IllegalStateException(
                        "Product id must be set so the agent knows which product is being discussed.");
            }

            if (type.equals(ConbineActionType.OFFER)) {
                if (value == null || value.length() == 0) {
                    throw new IllegalStateException(
                            "Value must be set for NegotiationActions if it's of type offer.");
                }

                try {
                    Double.parseDouble(value);
                } catch (NumberFormatException e) {
                    throw new IllegalStateException(
                            "Value must be a valid double for NegotiationActions if it's of type offer.");
                }
            }

            Tuple payload = null;
            if (type.equals(ConbineActionType.OFFER)) {
                payload = new Quintet<>(
                        protocol, dialogueId, replyToId, productId, value);
            } else {
                payload = new Quartet<>(protocol,
                        dialogueId, replyToId, productId);
            }

            return new NegotiationAction(type.toString(), recipientId, payload);
        }

        public Builder setDialogueId(String dialogueId) {
            this.dialogueId = dialogueId;
            return this;
        }

        public Builder setProductId(String productId) {
            this.productId = productId;
            return this;
        }

        public Builder setProtocol(String protocol) {
            this.protocol = protocol;
            return this;
        }

        public Builder setRecipient(String recipientId) {
            this.recipientId = recipientId;
            return this;
        }

        public Builder setReplyToId(String replyToId) {
            this.replyToId = replyToId;
            return this;
        }

        public Builder setType(ConbineActionType type) {
            this.type = type;
            return this;
        }

        public Builder setValue(String value) {
            this.value = value;
            return this;
        }
    }
    public static final int PROTOCOL = 0;
    public static final int DIALOGUE_ID = 1;
    public static final int REPLY_TO_ID = 2;
    public static final int PRODUCT_ID = 3;

    public static final int VALUE = 4;

    protected NegotiationAction(String type, String recipient, Tuple payload) {
        super(type, recipient, payload);
    }

    /**
     * Returns the term that will go inside the action struct when converting
     * the percept to a prolog term.
     * 
     * Format, where uppercase indicates variables. Note VALUE is optional; only
     * given if TYPE == "offer":
     * 
     * TYPE(REPLY_TO_ID, PRODUCT_ID[, VALUE]), sit(PROTOCOL, DIALOGUE_ID, TIME)
     * 
     * e.g.:
     * 
     * "offer(sender_agent, laptop, 500), sit(ao, d1, 239482042934)"
     * "accept(sender_agent, laptop), sit(ao, d1, 239482042934)"
     * 
     * @return
     */
    public String getActionStruct() {
        String type = getActionType();

        String typeStruct;
        if (type.equals(ConbineActionType.OFFER.toString())) {
            typeStruct = type + "(" + getReplyToId() + ", " + getProductId()
                    + "," + getValue() + ")";
        } else {
            typeStruct = type + "(" + getReplyToId() + ", " + getProductId()
                    + ")";
        }
        return typeStruct;
    }

    public String getDialogueId() {
        return (String) getPayload().getValue(NegotiationAction.DIALOGUE_ID);
    }

    public String getProductId() {
        return (String) getPayload().getValue(NegotiationAction.PRODUCT_ID);
    }

    public String getProtocol() {
        return (String) getPayload().getValue(NegotiationAction.PROTOCOL);
    }

    public String getReplyToId() {
        return (String) getPayload().getValue(NegotiationAction.REPLY_TO_ID);
    }

    public String getValue() {
        if (getActionType().equals(ConbineActionType.OFFER.toString())) {
            return (String) getPayload().getValue(NegotiationAction.VALUE);
        } else
            return null;
    }

    @Override
    public String toString() {
        if (getActionType().equals(ConbineActionType.OFFER.toString())) {
            String all = getActionType() + "(" + getProtocol() + ", "
                    + getDialogueId() + ", " + getReplyToId() + ", "
                    + getProductId() + ", " + getValue() + ")";

            return all;
        }

        return getActionType() + "(" + getProtocol() + ", " + getDialogueId()
                + ", " + getReplyToId() + ", " + getProductId() + ")";
    }

}
