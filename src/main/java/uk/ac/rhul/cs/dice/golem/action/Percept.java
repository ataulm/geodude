package uk.ac.rhul.cs.dice.golem.action;

public interface Percept {
    /**
     * Returns the content of the Percept.
     * 
     * @return action the content of the Percept
     */
    public Action getPerceptContent();

    /**
     * Get the time that this Percept was captured by the sensor.
     * 
     * @return time time (milliseconds since epoch) that the percept was
     *         recorded
     */
    public long getTimeStamp();
}
