package uk.ac.rhul.cs.dice.golem.action;

import org.javatuples.Unit;

/**
 * Corresponds to the ActionType as specified in {@link ActionType#SPEAK}.
 * 
 * @author ataulm
 */
public class SpeakAction extends Action {

    /**
     * SpeakAction - Whisper
     * 
     * @param recipient
     * @param payload
     */
    public SpeakAction(String recipient, Unit<String> payload) {
        super(Action.ActionType.SPEAK.toString(), recipient, payload);
    }

    /**
     * SpeakAction - Broadcast
     * 
     * @param payload
     */
    public SpeakAction(Unit<String> payload) {
        this(null, payload);
    }

    /**
     * Get the content of the SpeakAction.
     * 
     * @return payload the message content
     */
    @SuppressWarnings("unchecked")
    public String getWords() {
        Unit<String> payload = (Unit<String>) super.getPayload();
        return payload.getValue0();
    }
}
