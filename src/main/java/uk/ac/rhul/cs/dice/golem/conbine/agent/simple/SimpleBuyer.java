package uk.ac.rhul.cs.dice.golem.conbine.agent.simple;

import java.util.ArrayList;
import java.util.List;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.agent.AgentBrain;
import uk.ac.rhul.cs.dice.golem.conbine.action.NegotiationAction;
import uk.ac.rhul.cs.dice.golem.conbine.agent.AbstractBuyerAgent;
import uk.ac.rhul.cs.dice.golem.conbine.agent.AgentParameters;

@SuppressWarnings("serial")
public class SimpleBuyer extends AbstractBuyerAgent {
    private final static int ACCEPT = 0;
    private final static int COMMIT = 1;
    private final static int EXIT = 2;
    private final static int NUM_RANDOM_ACTIONS = EXIT; 
        
    public SimpleBuyer(AgentBrain brain, AgentParameters params, String product) {
        super(brain, params, product);
        setLogLevel(OFF);
    }

    @Override
    protected List<Action> decideActionBasedOnOffer(NegotiationAction offer) {
        List<Action> actionsToPerform = new ArrayList<>();
        
        double utilityOpponentOffer = getUtility(Double.parseDouble(offer.getValue()));
        double counterOffer = generateNextOffer(offer.getDialogueId());
        double utilityMyCounterOffer = getUtility(counterOffer);
        
        boolean isOfferAcceptable = isOfferAcceptable(
                utilityOpponentOffer,
                utilityMyCounterOffer,
                getNormalisedTime(getStartTime()));
        
        if (isOfferAcceptable) {
            actionsToPerform.addAll(super.acceptOpponentOffer(offer));
        } else {
            actionsToPerform.addAll(super.sendCounterOffer(offer, counterOffer));
        }
        
        return actionsToPerform;
    }
    
    @Override
    protected List<Action> decideActionBasedOnAccept(NegotiationAction accept) {
        switch (getRandom().nextInt(NUM_RANDOM_ACTIONS)) {
            case ACCEPT:
                return super.acceptOpponentOffer(accept);
                
            case COMMIT:
                return super.commitToMyLastBid(accept);
                
            case EXIT:
                return super.exitFromDialogue(accept.getDialogueId());              
        }
        
        return new ArrayList<>();
    }

    @Override
    protected double generateNextOffer(String dialogueId) {
        return getInitialPrice() +
                getRandom().nextInt(getReservationPrice() - getInitialPrice());
    }
    
    private boolean isOfferAcceptable(double utilityOpponentOffer,
            double utilityMyCounterOffer, double time) {
        
        double probabilityAccept = probabilityAccept(utilityOpponentOffer, time);
        double random = Math.random();

        if (probabilityAccept > random) {
            return true;
        }

        return false;
    }
    
    private static double probabilityAccept(double utility, double time) {
        if (utility < 0 || utility > 1) {
            utility = (utility < 0) ? 0 : 1;
        }

        double t = time * time * time; // steeper increase when deadline
                                       // approaches.

        if (t < 0 || t > 1) {
            throw new IllegalStateException("Time (" + t + ") outside [0,1]");
        }

        if (t == 0.5)
            return utility;

        return (utility - 2.0 * utility * t + 2.0 * (-1.0 + t + Math
                .sqrt(square(-1.0 + t) + utility * (-1.0 + 2 * t))))
                / (-1.0 + 2 * t);
    }
    
    private static double square(double x) {
        return x * x;
    } 
}
