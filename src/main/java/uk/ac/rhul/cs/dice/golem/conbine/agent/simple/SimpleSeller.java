package uk.ac.rhul.cs.dice.golem.conbine.agent.simple;

import java.util.List;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.agent.AgentBrain;
import uk.ac.rhul.cs.dice.golem.conbine.action.NegotiationAction;
import uk.ac.rhul.cs.dice.golem.conbine.agent.AbstractSellerAgent;
import uk.ac.rhul.cs.dice.golem.conbine.agent.AgentParameters;
import uk.ac.rhul.cs.dice.golem.conbine.agent.DialogueStateSeller;

@SuppressWarnings("serial")
public class SimpleSeller extends AbstractSellerAgent {
	public SimpleSeller(AgentBrain brain,
			AgentParameters params, String product) {
		super(brain, params, product);
	}	

	@Override
	protected List<Action> decideActionBasedOnOffer(NegotiationAction offer) {
		List<Action> actionsToPerform = super.decideActionBasedOnOffer(offer);
		
		double utilityOpponentOffer = getUtility(Double.parseDouble(offer.getValue()));
		double counterOffer = generateNextOffer(offer.getDialogueId());
		double utilityMyCounterOffer = getUtility(counterOffer);
		
		long startTime = ((DialogueStateSeller) getDialogues().get(offer.getDialogueId())).getStart();
		
		boolean isOfferAcceptable = isOfferAcceptable(
                utilityOpponentOffer,
                utilityMyCounterOffer,
                getNormalisedTime(startTime));
        
        if (isOfferAcceptable) {
            actionsToPerform.addAll(super.acceptOpponentOffer(offer));
        } else {
            actionsToPerform.addAll(super.sendCounterOffer(offer, counterOffer));
        }
		
		return actionsToPerform;
	}
	
	
	@Override
	protected double generateNextOffer(String dialogueId) {		
		return getInitialPrice() -
                getRandom().nextInt(getInitialPrice() - getReservationPrice());
	}
	
	private boolean isOfferAcceptable(double utilityOpponentOffer,
            double utilityMyCounterOffer, double time) {
        
        double probabilityAccept = probabilityAccept(utilityOpponentOffer, time);
        double random = Math.random();

        if (probabilityAccept > random) {
            return true;
        }

        return false;
    }
    
    private static double probabilityAccept(double utility, double time) {
        if (utility < 0 || utility > 1) {
            utility = (utility < 0) ? 0 : 1;
        }

        double t = time * time * time; // steeper increase when deadline
                                       // approaches.

        if (t < 0 || t > 1) {
            throw new IllegalStateException("Time (" + t + ") outside [0,1]");
        }

        if (t == 0.5)
            return utility;

        return (utility - 2.0 * utility * t + 2.0 * (-1.0 + t + Math
                .sqrt(square(-1.0 + t) + utility * (-1.0 + 2 * t))))
                / (-1.0 + 2 * t);
    }
    
    private static double square(double x) {
        return x * x;
    }
}
