package uk.ac.rhul.cs.dice.golem.agent;

import java.util.concurrent.Future;

/**
 * DefaultPoolableAgentBrain can be given to thread pooling service like
 * ExecutorService.
 *
 * To start the Agent, call {@link #startCycle()} either before or after
 * submitting the AgentBrain to the executor service as a task.
 *
 */
public class DefaultPoolableAgentBrain extends AbstractAgentBrain implements PoolableAgentBrain {
    private Future<?> future;

    @Override
    public void setFuture(Future future) {
        this.future = future;
    }

    @Override
    public void startCycle() {
        if (future == null) {
            throw new IllegalStateException("setFuture(Future) must be called with the Future returned from ExecutorService.submit(PoolableAgentBrain)");
        }
        setAgentRunning(true);
    }

    @Override
    public void startSuspended() {
        // no-op
    }

    @Override
    public void resumeCycle() {
        setAgentRunning(true);
    }

    @Override
    public void suspendCycle() {
        setAgentRunning(false);
    }

    /**
     * Essentially kills the thread.
     *
     * @see {@link #run()}
     */
    @Override
    public void stopCycle() {
        setAgentRunning(false);
        try {
            future.cancel(true);
        } catch (NullPointerException e) {
            System.out.println(getAgentId() + " couldn't find future.");
        }
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            if (isAgentRunning()) {
                super.stepForward();
            }
        }
    }
}

