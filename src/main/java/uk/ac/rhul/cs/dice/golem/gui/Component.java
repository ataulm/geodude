package uk.ac.rhul.cs.dice.golem.gui;

import java.awt.Color;
import java.awt.Container;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import uk.ac.rhul.cs.dice.golem.application.AbstractTabPanel;

/**
 * A collection of standard components for GOLEM. Use components from here to
 * keep application design consistent.
 * 
 * @author ataulm
 * 
 */
public final class Component {

    /**
     * Provides an input hint to a regular JTextField
     * 
     * @author Bart Kiers - http://stackoverflow.com/a/1739037/494879
     * 
     */
    @SuppressWarnings("serial")
    public static class HintTextField extends JTextField implements
            FocusListener {

        private final String hint;

        public HintTextField(final String hint) {
            super(hint);
            this.hint = hint;
            super.setForeground(HINT_COLOR);
            super.addFocusListener(this);
        }

        @Override
        public void focusGained(FocusEvent e) {
            if (this.getText().isEmpty()) {
                super.setText("");
                super.setForeground(Color.black);
            }
            getParent().revalidate();
        }

        @Override
        public void focusLost(FocusEvent e) {
            if (this.getText().isEmpty()) {
                super.setText(hint);
                super.setForeground(HINT_COLOR);
            }
        }

        @Override
        public String getText() {
            String typed = super.getText();
            return typed.equals(hint) ? "" : typed;
        }

        /**
         * If we set the text manually - i.e. from configuration file, then we
         * want this to appear as a user input, so we set it black
         */
        @Override
        public void setText(String text) {
            super.setText(text);
            super.setForeground(Color.black);
        }
    }
    static final Color LABEL_COLOR = new Color(0, 70, 213);

    static final Color HINT_COLOR = new Color(200, 200, 200);

    public static HintTextField createHintTextField(String hint) {
        final HintTextField h = new HintTextField(hint);
        return h;
    }

    /**
     * Returns a formatted ImageButton
     * 
     * @param icon
     * @param listener
     * @param altText
     * @return
     */
    public static final JButton createImageButton(BufferedImage icon,
            ActionListener listener, String altText) {
        JButton temp = new JButton(new ImageIcon(icon.getScaledInstance(15, 15,
                Image.SCALE_SMOOTH)));
        temp.setBorderPainted(false);
        temp.setContentAreaFilled(false);

        temp.addActionListener(listener);
        temp.setText(altText);

        return temp;
    }

    /**
     * Creates a JLabel with default alignment, and the chosen text
     * 
     * @param text
     *            the text to display
     * @return
     */
    public static JLabel createLabel(String text) {
        return createLabel(text, SwingConstants.LEADING);
    }

    /**
     * Creates a JLabel with the chosen alignment, and the chosen text
     * 
     * @param text
     *            the text to display
     * @param align
     *            The alignment of the text in the JLabel
     * @return
     */
    public static JLabel createLabel(String text, int align) {
        if (align != SwingConstants.LEFT
                || align != SwingConstants.CENTER
                || align != SwingConstants.RIGHT
                || align != SwingConstants.LEADING
                || align != SwingConstants.TRAILING) {
            throw new IllegalArgumentException("align must be one of: SwingConstants.LEFT, SwingConstants.CENTER, SwingConstants.RIGHT, SwingConstants.LEADING, SwingConstants.TRAILING");
        }
        final JLabel b = new JLabel(text, align);
        return b;
    }

    /**
     * Creates a sectional separator, with a heading.
     * 
     * @param panel
     *            the JPanel to which the separator should be added
     * @param title
     *            the heading to be applied to the section
     */
    public static void createSeparator(AbstractTabPanel panel, String title) {
        JLabel l = createLabel(title);
        l.setForeground(LABEL_COLOR);

        panel.getContentPanel().add(l, "gapbottom 1, gaptop 10, grow 0, span");
        panel.getContentPanel().add(new JSeparator(),
                "gapleft rel, gaptop 10, span, wrap");
    }

    /**
     * TODO: The docs state getComponentCount() should be used with
     * getTreeLock()
     * 
     * 
     * Gets the index of the component's position in its parent Container
     * 
     * @param component
     *            the component for which the position is required
     * @return the index of the component's position in its parent
     */
    public static final int getComponentIndex(java.awt.Component component) {
        if (component != null && component.getParent() != null) {
            Container c = component.getParent();
            for (int i = 0; i < c.getComponentCount(); i++) {
                if (c.getComponent(i) == component)
                    return i;
            }
        }
        return -1;
    }
}
