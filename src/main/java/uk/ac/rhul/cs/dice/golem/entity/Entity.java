package uk.ac.rhul.cs.dice.golem.entity;

import uk.ac.rhul.cs.dice.golem.container.Container;

/**
 * Entity represents an object or agent inside a {@link Container}.
 * 
 */
public interface Entity {

    /**
     * Return the identifier for this Entity.
     * 
     * @return id a String identifier for this Entity
     */
    public String getId();
}
