package uk.ac.rhul.cs.dice.golem.action;

public interface SensorHasPerceptListener {

    /**
     * Called by the sensor when it has a new percept
     * 
     * @param sensor
     *            the sensor which has the percept
     */
    public void onSensorHasPercept(Sensor sensor);

}
