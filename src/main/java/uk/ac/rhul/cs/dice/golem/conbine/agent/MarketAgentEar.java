package uk.ac.rhul.cs.dice.golem.conbine.agent;

import uk.ac.rhul.cs.dice.golem.action.AbstractSensor;
import uk.ac.rhul.cs.dice.golem.agent.AgentBody;
import uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType;

/**
 * The primary sensor for a market agent (buyer or seller).
 * 
 * @author ataulm
 * 
 */
public class MarketAgentEar extends AbstractSensor {
    public MarketAgentEar(String id, AgentBody context) {
        super(id, context);
        addType(ConbineActionType.ACCEPT.toString());
        addType(ConbineActionType.COMMIT.toString());
        addType(ConbineActionType.DECOMMIT.toString());
        addType(ConbineActionType.EXIT.toString());
        addType(ConbineActionType.OFFER.toString());
        addType(ConbineActionType.NOTIFY_ABOUT_NEW_SELLER.toString());
        addType(ConbineActionType.NOTIFY_ABOUT_CHANGE_IN_NUMBER_OF_COMPETITORS.toString());
        addType(ConbineActionType.NOTIFY_ABOUT_CHANGE_IN_DEMAND_SUPPLY_RATIO.toString());
        addType(ConbineActionType.NOTIFY_LEAVE_MARKET.toString());
    }
}
