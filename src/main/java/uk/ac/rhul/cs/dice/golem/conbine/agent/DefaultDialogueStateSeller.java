package uk.ac.rhul.cs.dice.golem.conbine.agent;

public class DefaultDialogueStateSeller extends DefaultDialogueState implements DialogueStateSeller {

	private final long startTime;
	
	protected DefaultDialogueStateSeller(String id, String protocol,
			String opponent, String product) {
		super(id, protocol, opponent, product);
		startTime = System.currentTimeMillis();
	}
	
	public long getStart() {
		return startTime;
	}
}
