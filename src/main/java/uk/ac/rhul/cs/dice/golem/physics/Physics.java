package uk.ac.rhul.cs.dice.golem.physics;

import uk.ac.rhul.cs.dice.golem.action.Event;

public interface Physics {

    /**
     * Returns true if the {@link Event} is safe to assert in the history.
     * 
     * @param event
     *            the event which is to be asserted in the history
     * @return isPossible whether or not the event can be asserted
     */
    public boolean attempt(Event event);

    /**
     * Returns true if it is possible to assert the {@link Event}.
     * 
     * @param event
     *            the event which is to be asserted in the history
     * @return isPossible whether or not the event can be asserted
     */
    public boolean isPossible(Event event);
}
