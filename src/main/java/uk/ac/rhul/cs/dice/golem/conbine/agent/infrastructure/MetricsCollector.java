package uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;

import org.javatuples.Triplet;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.action.Event;
import uk.ac.rhul.cs.dice.golem.action.Percept;
import uk.ac.rhul.cs.dice.golem.agent.AbstractAgentMind;
import uk.ac.rhul.cs.dice.golem.agent.AgentBrain;
import uk.ac.rhul.cs.dice.golem.conbine.agent.MarketAgent;
import uk.ac.rhul.cs.dice.golem.conbine.agent.MarketAgent.AgentType;
import uk.ac.rhul.cs.dice.golem.conbine.action.AnnounceEntranceAction;
import uk.ac.rhul.cs.dice.golem.conbine.action.CollectMetricsAction;
import uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType;
import uk.ac.rhul.cs.dice.golem.conbine.action.NegotiationAction;
import uk.ac.rhul.cs.dice.golem.conbine.action.NotifyRunCompleteAction;
import uk.ac.rhul.cs.dice.golem.conbine.agent.AgentParameters;
import uk.ac.rhul.cs.dice.golem.container.Container;
import uk.ac.rhul.cs.dice.golem.container.ContainerHistory;
import uk.ac.rhul.cs.dice.golem.container.DefaultContainer;

/**
 * MetricsCollector is an agent that, given the ID of an agent (of type
 * {@link AgentType#BUYER}) will traverse the {@link Container} history
 * (up to the point of that buyer finalising the deal).
 * @author ataulm
 *
 */
@SuppressWarnings("serial")
public class MetricsCollector extends AbstractAgentMind {
    private final ExecutorService executor;
    private final DefaultContainer container;
    private final Queue<Action> doneMessages;
    private final Map<String, Triplet<Integer, Long, Double>> allMetrics;
    private final int runsPerCombo;
    private final String experimentLabel;
    private final String appData;
    private int deadline;
    private int currentRun;
    private int combinationNumber;

    /**
     * Constructs a {@link MetricsCollector}.
     * 
     * @param brain
     *            the {@link AgentBrain} which calls {@link #executeStep()} in a
     *            loop
     * @param container
     *            the {@link Container} this agent is located in
     * @param runsPerCombo
     *            number of repetitions per given combination
     * @param experimentLabel
     *            a label for the experiment
     * @param appData
     *            the absolute filepath to the ConBiNe app data directory
     */
    public MetricsCollector(AgentBrain brain, DefaultContainer container,
            int runsPerCombo, String experimentLabel, String appData, ExecutorService executor) {
        super(brain);
        this.container = container;
        this.appData = Paths.get(appData).toString();
        this.experimentLabel = experimentLabel;
        this.runsPerCombo = runsPerCombo;
        this.executor = executor; 
        doneMessages = new ConcurrentLinkedQueue<>();
        allMetrics = new ConcurrentHashMap<>();
    }

    @Override
    public List<Action> executeStep() throws Exception {
    	List<Percept> percepts = getBrain().getAllPerceptions();
        List<Action> actionsToPerform = new ArrayList<>();
    
        for (Percept percept : percepts) {
            Action action = percept.getPerceptContent();
            String type = action.getActionType();
    
            if (type.equals(ConbineActionType.COLLECT_METRICS.toString())) {
                collectRecords((CollectMetricsAction) action);
                continue;
            }
    
            /**
             * Will only be received once per run, and only when the
             * MetricsCollector has responded to ALL messages of type
             * COLLECT_METRICS.
             */
            if (type.equals(ConbineActionType.NOTIFY_RUN_COMPLETE.toString())) {
                currentRun++;
                if (currentRun == runsPerCombo) {
                    currentRun = 0;
                    combinationNumber = ((NotifyRunCompleteAction) action).getCombinationNumber();
                    try {
                        flushToDisk();                        
                    } catch (IOException e) {
                        log("Unable to save logs for combo: " + combinationNumber);
                    }
                }
            }
        }
    
        actionsToPerform.addAll(doneMessages);
        doneMessages.clear();
    
        return actionsToPerform;
    }

    /**
     * Processes the history of the {@link Container} to save metrics for the
     * specified MarketAgent.
     * 
     * Spawns a new Thread so that metrics for multiple MarketAgents can be
     * collected simultaneously. The {@link MarketController} should send a
     * {@link CollectMetricsAction} order to the MetricsCollector as soon as an
     * agent for which metrics should be collected leaves the marketplace; it
     * should not wait until all three have left.
     * 
     * @param order
     */
    private void collectRecords(final CollectMetricsAction order) {
    	log(getBrain().getAgentId() + " is collecting metrics for agent " + order.getSubject());
        executor.execute(new Collector(order));
    }

    /**
     * Writes the contents of the {@link #allMetrics} {@link Map} to disk, in a
     * new file.
     * 
     * For each MarketAgent (where a successful run is one where the buyer sends
     * an ACCEPT):
     * 
     * - average overall utility - average utility in successful runs - number
     * of successful runs - average time in market (for successful runs)
     * 
     * The metrics will be formatted using spaces as delimiters, and a line
     * break separating the metrics for different MarketAgents. The filename
     * will the current combination number {@link #combinationNumber} so if there's
     * an error, you can rerun the experiment starting from this number.
     * 
     * The file will be saved to a directory in:
     * /AppData/ConBiNe_data/<experimentLabel> and the path will be created if
     * it isn't already there.
     * 
     */
    private void flushToDisk() throws IOException {
        log(experimentLabel + " | Saving runs to file: " + combinationNumber + ".txt");
        Path output = Paths.get(appData, experimentLabel,
                String.valueOf(combinationNumber) + ".txt");
        output.toFile().getParentFile().mkdirs();
        output.toFile().createNewFile();
        BufferedWriter writer = Files.newBufferedWriter(output,
                StandardCharsets.UTF_8, StandardOpenOption.TRUNCATE_EXISTING,
                StandardOpenOption.CREATE, StandardOpenOption.WRITE);

        for (String subjectId : allMetrics.keySet()) {
            Triplet<Integer, Long, Double> metrics = allMetrics.get(subjectId);
                        
            int successfulRuns = metrics.getValue0();
            long totalTimeForSuccessfulRuns = metrics.getValue1();
            double totalUtility = metrics.getValue2();
            
            long avgTimePerSuccessfulRun = totalTimeForSuccessfulRuns / successfulRuns;
            double normalisedAvgTime = (double) avgTimePerSuccessfulRun / (deadline * 1000);
            double avgOverallUtility = totalUtility / (runsPerCombo);
            double avgUtilityForSuccessfulRuns = totalUtility / successfulRuns;

            writer.write(avgOverallUtility + " " + avgUtilityForSuccessfulRuns
                    + " " + successfulRuns + " "
                    + normalisedAvgTime + "\n");
        }

        writer.close();
        allMetrics.clear();
    }

    /**
     * A Collector's lifetime lasts per buyer, per run.
     * 
     * @author ataulm
     * 
     */
    private class Collector implements Runnable {
        private final CollectMetricsAction order;
        private final String subjectId;
        private final ContainerHistory relevantHistory;
    
        private double utility;
        private boolean successful;
    
        private long startTime;
        private long acceptTime;
        private final AgentType agentType;
        private double acceptedOffer;
        private final AgentParameters params;
        
        private boolean agentAnnounced;
    
        public Collector(CollectMetricsAction order) {
            this.order = order;
            subjectId = order.getSubject();
            agentType = MarketAgent.AgentType.BUYER;
            relevantHistory = new ContainerHistory(container.getHistory());
            params = order.getParams();
        }
    
        @Override
        public void run() {
            log("Processing history for agent: " + subjectId);

            // Work forwards through the history 
            for (int i = 0; i < relevantHistory.size(); i++) {
                Event event = relevantHistory.get(i);
                if (event == null) continue;
                String actionType = event.getActionType();
                Action action = event.getAction();
        
                if (actionType.equals(ConbineActionType.ANNOUNCE_ENTRANCE
                        .toString())
                        && ((AnnounceEntranceAction) action).getAgentId()
                                .equals(subjectId)) {
        
                    startTime = event.getTimestamp();
                    agentAnnounced = true;
                    continue;
                }
                
                if (actionType.equals(ConbineActionType.ACCEPT.toString())) {
                    // workaround for incomplete history (v. rare)
                    if (!agentAnnounced) break;
                    
                    NegotiationAction accept = (NegotiationAction) action;
                    if (!accept.getReplyToId().equals(subjectId)) continue; 
                    
                    successful = true;
                    acceptTime = event.getTimestamp();
                    utility = findOfferAndCalculateUtility(
                            (NegotiationAction) action, i);
                    updateMetrics();
                    break;
                }
            }
        
            doneMessages.add((new CollectMetricsAction.Builder()
                    .setState(CollectMetricsAction.COLLECTED)
                    .setSubject(order.getSubject())
                    .setRecipient(order.getReplyToId())
                    .setReplyToId(getBrain().getAgentId())
                    .setParams(order.getParams()).build()));

            log("Processed history for agent: " + subjectId);
        }
        
        

        /**
         * Calculates the utility of an offer, based on the {@link AgentType},
         * their initial and reservation prices, and the offer itself.
         * @param accept 
         * 
         * @param agentType
         * @param params
         * @param acceptedOffer
         * @return utility
         */
        private double calculateUtility() {
            deadline = params.get(AgentParameters.DEADLINE);
            
            if (agentType.equals(AgentType.BUYER)) {
                int initialPrice = params.get(AgentParameters.BUYER_INITIAL);
                int resPrice = params.get(AgentParameters.BUYER_RESERVATION);
                double utility = (resPrice - acceptedOffer) / (resPrice - initialPrice);
                log("calculated utility for " + subjectId + " at run " + currentRun + " is " + utility);
                log(subjectId + " iP: " + initialPrice + " rP " + resPrice + " acceptedOffer: " + acceptedOffer);
                return utility;
            }
    
            int initialPrice = params.get(AgentParameters.SELLER_INITIAL);
            int resPrice = params.get(AgentParameters.SELLER_RESERVATION);
            return (acceptedOffer - resPrice) / (initialPrice - resPrice);
        }
    
        /**
         * Returns the utility of the agreement reached in the dialogue where
         * the passed ACCEPT action was given.
         * 
         * It iterates backwards through the container history from the ACCEPT
         * message until it finds an OFFER message with the same dialogue ID.
         * 
         * @param accept
         *            the {@link NegotiationAction} corresponding to the ACCEPT
         *            message in the dialogue
         * @param historyIndex
         *            the index of this {@link Action}'s corresponding
         *            {@link Event} in the {@link DefaultContainer}'s history
         */
        private double findOfferAndCalculateUtility(NegotiationAction accept,
                int historyIndex) {
            if (!accept.getActionType().equals(
                    ConbineActionType.ACCEPT.toString())) {
                return 0;
            }
    
            String dialogue = accept.getDialogueId();
    
            for (int i = historyIndex; i >= 0; i--) {
                Event event = relevantHistory.get(i);
                if (event == null) continue;
                if (event.getActionType().equals(ConbineActionType.OFFER.toString())
                        && ((NegotiationAction) event.getAction()).getDialogueId().equals(dialogue)) {
                    
                    acceptedOffer = Double.parseDouble(((NegotiationAction) event
                            .getAction()).getValue());
                    
                    return calculateUtility();
                }
            }
    
            return 0;
        }
        
    
        private void updateMetrics() {
            if (!allMetrics.containsKey(subjectId)) {
                // add a new entry for this agent
                Triplet<Integer, Long, Double> metrics;
                metrics = new Triplet<>(
                        (successful) ? 1 : 0, acceptTime - startTime, utility);
                allMetrics.put(subjectId, metrics);
            } else {
                // update the running stats for agent
                Triplet<Integer, Long, Double> existing = allMetrics.get(subjectId);
                existing.setAt0(existing.getValue0() + ((successful) ? 1 : 0));
                existing.setAt1(existing.getValue1() + (acceptTime - startTime));
                // FIXME: Utility is sometimes coming back as negative values - at least for SMBC
                existing.setAt2(existing.getValue2() + ((utility > 0) ? utility : 0));
            }
        }
    }
}
