package uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure;

import uk.ac.rhul.cs.dice.golem.action.AbstractEffector;
import uk.ac.rhul.cs.dice.golem.agent.AgentBody;
import uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType;

public class MetricsCollectorMouth extends AbstractEffector {

    public MetricsCollectorMouth(String id, AgentBody context) {
        super(id, context);
        addType(ConbineActionType.COLLECT_METRICS.toString());
    }

}
