package uk.ac.rhul.cs.dice.golem.agent;

import java.util.List;

import uk.ac.rhul.cs.dice.golem.action.Action;

public interface AgentMind {
    /**
     * Executes a single step of the agent cycle.
     * 
     * The cycle step should consist of perceiving, reasoning and acting.
     * 
     * @throws Exception
     */
    public List<Action> executeStep() throws Exception;

    public AgentBrain getBrain();
}
