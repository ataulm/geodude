package uk.ac.rhul.cs.dice.golem.physics;

import uk.ac.rhul.cs.dice.golem.action.Event;

public class DefaultPhysics extends AbstractPhysics {

    /**
     * Everything is allowed (worst. physics. ever.)
     */
    @Override
    public boolean isPossible(Event event) {
        return true;
    }

}
