package uk.ac.rhul.cs.dice.golem.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public abstract class ResourceHelper {

    @SuppressWarnings("resource")
    public static InputStream getInputStream(Object context, String path) {
        InputStream inputStream;
        Logger.micro(context, "Loading resource... - " + path);

        try {
            inputStream = new FileInputStream(path);
        } catch (FileNotFoundException e) {
            Logger.micro(context, "Resource not found on filesystem. - " + path
                    + "\nChecking compiled resources...");
            // It wasn't a file on the local system
            inputStream = context.getClass().getResourceAsStream(path);
            if (inputStream == null) {
                Logger.e(context, "No resources found. - " + path);
                return null;
            }
            Logger.micro(context, "(Compiled) resource found. - " + path);
        }
        return inputStream;
    }

    /**
     * Returns a path for the file system constructed of the file path segments
     * 
     * @param startsWithSeparator
     *            True if returned path starts with separator
     * @param endsWithSeparator
     *            True if returned path ends with separator
     * @param strings
     *            file path segments (in order) separated by commas
     * @return
     */
    public static String getPath(boolean startsWithSeparator,
            boolean endsWithSeparator, String... segments) {
        String path = "";
        if (startsWithSeparator)
            path = File.separator;

        for (int i = 0; i < segments.length; i++) {
            path += segments[i] + File.separator;
        }

        if (!endsWithSeparator)
            path = path.substring(0, path.length() - 1);

        String signature = "";
        for (String segment : segments) {
            if (signature.length() == 0) {
                signature = segment;
            } else {
                signature = signature + ", " + segment;
            }
        }

        Logger.micro(ResourceHelper.class, "ResourceHelper.getPath("
                + startsWithSeparator + ", " + endsWithSeparator + ", "
                + signature + ") returned: " + path);

        return path;
    }

    /**
     * To be used instead of alice.util.Tools.loadText(String filename) when
     * loading theories. Grabbing the file name from inside a JAR was not
     * proving cooperative, so instead files in JARs are returned as streams,
     * which are returned as strings here.
     * 
     * @param is
     * @return
     */
    public static String loadText(InputStream is) {
        byte[] info = null;
        try {
            info = new byte[is.available()];
        } catch (IOException e) {
            System.out.println("is.available() fail");
            e.printStackTrace();
        }
        try {
            is.read(info);
        } catch (IOException e) {
            System.out.println("is.read() fail");
            e.printStackTrace();
        }
        return new String(info);
    }

    public static String loadText(Object context, String pathToResource) {
        InputStream stream = ResourceHelper.getInputStream(context,
                pathToResource);
        return ResourceHelper.loadText(stream);
    }

}
