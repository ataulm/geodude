package uk.ac.rhul.cs.dice.golem.conbine.app;

import uk.ac.rhul.cs.dice.golem.action.Event;
import uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType;
import uk.ac.rhul.cs.dice.golem.physics.AbstractPhysics;

public class MarketRules extends AbstractPhysics {

    /**
     * We can go deeper than this with strictness. At the moment, it just checks
     * for valid action types, not whether those actions are allowed in the
     * given situation.
     */
    @Override
    public boolean isPossible(Event event) {
        String type = event.getActionType();

        if (type.equals(ConbineActionType.ACCEPT.toString())) {
            return true;
        } else if (type.equals(ConbineActionType.OFFER.toString())) {
            return true;
        } else if (type.equals(ConbineActionType.COMMIT.toString())) {
            return true;
        } else if (type.equals(ConbineActionType.DECOMMIT.toString())) {
            return true;
        } else if (type.equals(ConbineActionType.EXIT.toString())) {
            return true;
        } else if (type.equals(ConbineActionType.ANNOUNCE_ENTRANCE.toString())) {
            return true;
        } else if (type.equals(ConbineActionType.EXIT_ALL.toString())) {
            return true;
        } else if (type.equals(ConbineActionType.NOTIFY_ABOUT_NEW_SELLER
                .toString())) {
            return true;
        } else if (type.equals(ConbineActionType.NOTIFY_RUN_COMPLETE.toString())) {
            return true;
        } else if (type.equals(ConbineActionType.NOTIFY_ABOUT_CHANGE_IN_DEMAND_SUPPLY_RATIO.toString())) {
            return true;
        } else if (type.equals(ConbineActionType.NOTIFY_ABOUT_CHANGE_IN_NUMBER_OF_COMPETITORS.toString())) {
            return true;
        } else if (type.equals(ConbineActionType.NOTIFY_LEAVE_MARKET.toString())) {
            return true;
        } else if (type.equals(ConbineActionType.COLLECT_METRICS.toString())) {
            return true;
        }

        return false;
    }

}
