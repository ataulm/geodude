package uk.ac.rhul.cs.dice.golem.conbine.agent.faratin;

import uk.ac.rhul.cs.dice.golem.conbine.agent.DefaultDialogueStateSeller;
import uk.ac.rhul.cs.dice.golem.conbine.agent.DefaultOfferHistory;
import uk.ac.rhul.cs.dice.golem.conbine.agent.OfferHistory;

public class DefaultDialogueStateFratinSellerOfferHistory extends DefaultDialogueStateSeller implements OfferHistory {
	private final OfferHistory offers;
	
	protected DefaultDialogueStateFratinSellerOfferHistory(String id, String protocol,
			String opponent, String product) {
		super(id, protocol, opponent, product);
		offers = new DefaultOfferHistory();
	}
	
	@Override
    public double getMyLastBid() {
    	return offers.getMyLastBid();
    }

	@Override
    public void updateMyLastBid(double bid) {
    	offers.updateMyLastBid(bid);
    }
    
	@Override
    public double getMinusNthOpponentBid(int n) {
    	return offers.getMinusNthOpponentBid(n);
    }
    
	@Override
    public void addOpponentBid(double bid) {
    	offers.addOpponentBid(bid);
    }

	@Override
	public double getOldestOpponentBid() {
		return offers.getOldestOpponentBid();
	}
}
