package uk.ac.rhul.cs.dice.golem.conbine.action;

import org.javatuples.Pair;

import uk.ac.rhul.cs.dice.golem.action.Action;

/**
 * As per {@link ConbineActionType#NOTIFY_ABOUT_NEW_SELLER}.
 * 
 * @author ataulm
 * 
 */
public class NotifyAboutNewSellerAction extends Action {
    public static class Builder {
        private String recipientId;
        private String newSellerId;
        private String dialogueId;

        public NotifyAboutNewSellerAction build() {
            if (newSellerId == null || newSellerId.length() == 0) {
                throw new IllegalStateException(
                        "The new seller's ID must be set.");
            }

            if (recipientId == null || recipientId.length() == 0) {
                throw new IllegalStateException(
                        "Recipient must be set for notify actions.");
            }

            if (dialogueId == null || dialogueId.length() == 0) {
                throw new IllegalStateException(
                        "The (uniquely generated) dialogue ID must be set.");
            }

            Pair<String, String> payload = new Pair<>(
                    newSellerId, dialogueId);

            return new NotifyAboutNewSellerAction(recipientId, payload);
        }

        public Builder setDialogueId(String dialogueId) {
            this.dialogueId = dialogueId;
            return this;
        }

        public Builder setNewSellerId(String newSellerid) {
            this.newSellerId = newSellerid;
            return this;
        }

        public Builder setRecipient(String recipientId) {
            this.recipientId = recipientId;
            return this;
        }
    }
    public static final int NEW_SELLER_ID = 0;

    public static final int DIALOGUE_ID = 1;

    protected NotifyAboutNewSellerAction(String recipient,
            Pair<String, String> payload) {
        super(ConbineActionType.NOTIFY_ABOUT_NEW_SELLER.toString(), recipient,
                payload);
    }

    public String getDialogueId() {
        return (String) getPayload().getValue(DIALOGUE_ID);
    }

    public String getNewSellerId() {
        return (String) getPayload().getValue(NEW_SELLER_ID);
    }

    @Override
    public String toString() {
        return getActionType() + "(" + getNewSellerId() + ", "
                + getDialogueId() + ")";
    }
}
