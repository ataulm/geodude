package uk.ac.rhul.cs.dice.golem.conbine.app;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public abstract class ConbineVariable {
    private final String id;
    private final Map<String, ValueBucket> buckets;
    private final Map<Integer, String> bucketByIndex;
    private final Random random;

    protected ConbineVariable(String id) {
        this.id = id;
        buckets = new HashMap<>();
        bucketByIndex = new HashMap<>();
        random = new Random();
    }

    /**
     * Adds a new {@link ValueBucket} for this ConbineVariable.
     * 
     * @param bucket
     *            the ValueBucket to add
     */
    protected void addBucket(ValueBucket bucket) {
        buckets.put(bucket.getId(), bucket);
        bucketByIndex.put(buckets.size() - 1, bucket.getId());
    }

    /**
     * Returns the {@link ValueBucket} at the specified index.
     * 
     * @param index
     *            the index of the ValueBucket to return
     * @return bucket the ValueBucket at the specified index
     * @throws IndexOutOfBoundsException
     *             if the index is outside the range
     */
    public ValueBucket get(int index) {
        if (bucketByIndex.containsKey(index)) {
            return buckets.get(bucketByIndex.get(index));
        }

        throw new IndexOutOfBoundsException();
    }

    /**
     * Returns the ID of this ConbineVariable.
     * 
     * @return id the ID of this ConbineVariable
     */
    public String getId() {
        return id;
    }

    /**
     * Gets a random value from a randomly selected {@link ValueBucket}.
     * 
     * @return value the random value
     */
    public Object pickValue() {
        if (buckets.size() > 0) {
        	int index = random.nextInt(buckets.size());
        	String bucketId = bucketByIndex.get(index);
        	ValueBucket bucket = buckets.get(bucketId);
        	
        	return bucket.pickValue();
        }
        return 0;
    }

    /**
     * Gets a random value from the specified {@link ValueBucket} if it exists.
     * 
     * @param bucketId
     *            the ID of the ValueBucket
     * @return value the random value, if the ValueBucket exists, else 0
     */
    public Object pickValue(String bucketId) {
        if (buckets.containsKey(bucketId)) {
            return buckets.get(bucketId).pickValue();
        }
        return 0;
    }

    /**
     * Returns the number of {@link ValueBucket}s in this
     * {@link ConbineVariable}.
     * 
     * @return size the number of ValueBuckets in this ConbineVariable
     */
    public int size() {
        return buckets.size();
    }
}
