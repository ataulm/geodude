package uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.action.Event;
import uk.ac.rhul.cs.dice.golem.action.Percept;
import uk.ac.rhul.cs.dice.golem.agent.AbstractAgentMind;
import uk.ac.rhul.cs.dice.golem.agent.AgentBody;
import uk.ac.rhul.cs.dice.golem.agent.AgentBrain;
import uk.ac.rhul.cs.dice.golem.conbine.agent.MarketAgent.AgentType;
import uk.ac.rhul.cs.dice.golem.agent.PoolableAgentBrain;
import uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType;
import uk.ac.rhul.cs.dice.golem.conbine.action.ExitAllAction;
import uk.ac.rhul.cs.dice.golem.conbine.action.NotifyAboutChangeInDemandSupplyRatioAction;
import uk.ac.rhul.cs.dice.golem.conbine.action.NotifyAboutChangeInNumberOfCompetitors;
import uk.ac.rhul.cs.dice.golem.conbine.action.NotifyLeaveMarketAction;
import uk.ac.rhul.cs.dice.golem.conbine.action.NotifyRunCompleteAction;
import uk.ac.rhul.cs.dice.golem.conbine.agent.AgentFactory;
import uk.ac.rhul.cs.dice.golem.conbine.agent.AgentParameters;
import uk.ac.rhul.cs.dice.golem.conbine.app.*;
import uk.ac.rhul.cs.dice.golem.container.ContainerHistory;
import uk.ac.rhul.cs.dice.golem.container.DefaultContainer;

@SuppressWarnings("serial")
public class MarketController extends AbstractAgentMind {
    private static final String MARKET_BROKER = "broker_agent";
    private final DefaultContainer container;
    private final String[] userSelectedBuyers;
    private final String[] competitorTypePool;
    private final String[] sellersTypePool;
    private final Map<String, PoolableAgentBrain> selectedBuyers;
    private final Map<String, PoolableAgentBrain> competitors;
    private final Map<String, PoolableAgentBrain> sellers;
    private int inconsequentialMarketAgentCount;
    private final AgentParameters params;
    private final Clicker clicker;
    private final String appData;
    private final String experimentLabel;
    private final List<String> departingAgents;

    private int numberCompetitors;
    private int numberSellers;
    private long lastChangedTime;
    private int marketChangeTime; // 0 means will never change

    // Agent Variables
    private final ContinuousVariable buyerInitialVar;
    private final ContinuousVariable sellerInitialVar;
    private final ContinuousVariable buyerResVar;
    private final ContinuousVariable sellerResVar;
    private final ContinuousVariable deadlineVar;

    private final DiscreteVariable changeRateDensityVar;
    private final DiscreteVariable changeRateRatioVar;
    private final DiscreteVariable marketChangeTimeVar;
    private Ratio currentRateRatio;

    private final Random random = new Random();
    private final int runsPerCombo;
    private int currentRun = 1;

    private boolean startNewRun;
    private boolean marketBrokerCreated;

    private static final String PRODUCT_ID = "bananas";
    private ExecutorService marketAgentThreadService;

    public MarketController(AgentBrain brain, DefaultContainer container,
            String appData, ConbineUserInput userInput) {
        super(brain);
        this.container = container;
        this.appData = appData;


        departingAgents = new ArrayList<>();

        // Agents
        selectedBuyers = new HashMap<>();
        competitors = new HashMap<>();
        sellers = new HashMap<>();

        // Sort user input
        experimentLabel = userInput.getExperimentLabel();
        runsPerCombo = userInput.getRunsPerCombination();
        userSelectedBuyers = userInput.getSelectedBuyers();
        competitorTypePool = userInput.getBuyerTypePool();
        sellersTypePool = userInput.getSellerTypePool();

        marketChangeTimeVar = userInput.getMarketChangeTime();
        changeRateDensityVar = userInput.getMarketChangeRateDensity();
        changeRateRatioVar = userInput.getMarketChangeRateRatio();
        deadlineVar = userInput.getDeadline();

        buyerInitialVar = userInput.getBuyerInitial();
        sellerInitialVar = userInput.getSellerInitial();
        buyerResVar = userInput.getBuyerReservation();
        sellerResVar = userInput.getSellerReservation();

        // to generate all possible combinations of the above deadlines
        clicker = new Clicker(marketChangeTimeVar, changeRateDensityVar,
                changeRateRatioVar, deadlineVar);

        try {
            clicker.printCombinationMapToFile(Paths.get(appData,
                    experimentLabel, "combinationMap.txt"));
        } catch (IOException e) {
            log(getClass().getSimpleName(), "Unable to print combination map.");
            e.printStackTrace();
        }

        params = new AgentParameters();
        setLogLevel(STANDARD);
    }

    @Override
	public List<Action> executeStep() throws Exception {    	
	    List<Action> actionsToPerform = new ArrayList<>();

	    if (!marketBrokerCreated) {
	    	createMarketBroker();
	    	marketBrokerCreated = true;
            startNewRun = true;
	    }
	    
	    if (startNewRun) {
	    	newRun();
            startNewRun = false;
	    	return actionsToPerform;
	    }

	    /* ********************************************************************
	     * Dealing with messages and notifications
	     * ********************************************************************
	     */
	    List<Percept> percepts = getBrain().getAllPerceptions();
        for (Percept percept : percepts) {
            Action action = percept.getPerceptContent();
            String type = action.getActionType();

            if (type.equals(ConbineActionType.EXIT_ALL.toString())) {
	            actionsToPerform.addAll(decideBasedOnExitAll((ExitAllAction) action));
	        }
	    }

        /* ********************************************************************
	     * Every loop (executeStep()) after dealing with messages
	     * ********************************************************************
	     */

        long changeTime = lastChangedTime + marketChangeTime * 1000;
        long currentTime = System.currentTimeMillis();
        if (marketChangeTime >= 0 && currentTime > changeTime) {
            alterMarket(currentTime);
        }
	
	    return actionsToPerform;
	}

    /**
     * Actions to perform when a run is complete.
     *
     * Clears all market agents left in the container.
     * Saves container history to file.
     * Iterates to next run, if there are any left to do.
     * Otherwise, iterates to next combination of variables, if any left to do.
     * Else, finishes the experiment.
     */
    private List<Action> onRunComplete() {
        List<Action> actionsToPerform = new ArrayList<>();

        actionsToPerform.add(new NotifyRunCompleteAction.Builder()
                .setCombinationNumber(clicker.getCurrentCombination())
                .setRunNumber(currentRun)
                .build());

        // hard kick all market agents
        removeAllMarketAgents();

        // save run to file
        try {
            printRunToFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        container.clearHistory();

        // advance to next run, if possible
        if (currentRun < runsPerCombo) {
            currentRun++;
            startNewRun = true;
            inconsequentialMarketAgentCount = 0;
            return actionsToPerform;
        }

        // else advance to next combination, and reset run number
        if (clicker.click()) {
            currentRun = 1;
            startNewRun = true;
            return actionsToPerform;
        }

        // else end experiment
        finish();
        return actionsToPerform;
    }

    /**
     * Stops the infrastructure agents.
     */
    private void finish() {
    	log("Specified combinations covered, cleaning environment.");

    	// Stop market broker
    	AgentBody broker = (AgentBody) container.getEntity(MARKET_BROKER);
    	broker.getBrain().stopCycle();
    	
    	// "I know now why you cry"
    	getBrain().stopCycle();

    	// TODO: Start all the MetricsAgents?
    	log("Environment clean.");
    }

    /**
     * Stops execution of all market agents and removes from container.
     */
    private void removeAllMarketAgents() {
    	List<AgentBrain> agents;
        agents = new ArrayList<>();
        agents.addAll(selectedBuyers.values());
	    agents.addAll(sellers.values());
	    agents.addAll(competitors.values());
	    
	    selectedBuyers.clear();
	    sellers.clear();
	    competitors.clear();
	    departingAgents.clear();

	    inconsequentialMarketAgentCount = 0;
	    
	    for (AgentBrain agent : agents) {
	        agent.stopCycle();
	        container.removeEntity(agent.getAgentId());
	    }
    }

    /**
     * Creates and starts the agents required for a new run.
     */
	private void newRun() {
		log("\n\n\nStarting new run: " + clicker.getCurrentCombination() + "_" + currentRun);
		generateAgentParameters();

        // ThreadPool size == max # of market agents (at any point) during this run
        int densityBucketIndex = clicker.getCurrentIndex(changeRateDensityVar);
        int maxDensityInBucket = (int) ((DiscreteValueBucket) changeRateDensityVar.get(densityBucketIndex)).getMaxValue();
        marketAgentThreadService = Executors.newFixedThreadPool(maxDensityInBucket + selectedBuyers.size());

        calculateNumberOfMarketAgents();
        initialiseSelectedBuyers();
        initialiseCompetitors();
        initialiseSellers();
	}

	/**
	 * Selects random values (from all buckets) for initial and reservation
	 * prices, and selects the deadline from the bucket specified by the
	 * clicker.
	 * 
	 * Called at the start of each run.
	 */
	private void generateAgentParameters() {
	    params.copyValues(new AgentParameters((Integer) buyerInitialVar.pickValue(),
	            (Integer) buyerResVar.pickValue(),
	            (Integer) sellerInitialVar.pickValue(),
	            (Integer) sellerResVar.pickValue(),
	            (Integer) deadlineVar.get(
	                    clicker.getCurrentIndex(deadlineVar)).pickValue()));
	}

    /**
     * Changes the number of sellers and competitors in the market.
     *
     * Selects new values for number of sellers and number of competitors.
     * Selects a new market change time (calls to {@link #alterMarket(long)} will not be in fixed intervals).
     * Initiates method to update buyers with new information about the market.
     *
     * @param currentTime
     * @return
     */
	private List<Action> alterMarket(long currentTime) {
        lastChangedTime = currentTime;
		logMicro("Altering market at time: " + currentTime/1000);
    	List<Action> actionsToPerform = new ArrayList<>();
        for (String id: departingAgents) {
            logMicro("Waiting to depart: " + id);
        }
        logMicro("MarketAgent count (before recalculation): sellers (" + sellers.size() + ") " + "competitors (" + competitors.size() + ")");
        logMicro("MarketAgent count (before recalculation): sellers (" + numberSellers + ") " + "competitors (" + numberCompetitors + ")");
        calculateNumberOfMarketAgents();
        logMicro("MarketAgent count ( after recalculation): sellers (" + numberSellers + ") " + "competitors (" + numberCompetitors + ")");

        marketChangeTime = (int) marketChangeTimeVar.get(
                clicker.getCurrentIndex(marketChangeTimeVar)).pickValue();

        int deltaCompetitors = Math.abs(competitors.size() - numberCompetitors);
        if (competitors.size() < numberCompetitors) {
            for (int i = 0; i < deltaCompetitors; i++) {
                logMicro("Adding new competitor");
                PoolableAgentBrain competitor = addCompetitor();
                Future future = marketAgentThreadService.submit(competitor);
                competitor.setFuture(future);
                competitor.startCycle();
            }
        } else if (competitors.size() > numberCompetitors) {
            for (int i = 0; i < deltaCompetitors; i++) {
                logMicro("Requesting exit from a competitor //" + i);
                actionsToPerform.addAll(sendExitRequestToCompetitor());
            }
        }

        int deltaSellers = Math.abs(sellers.size() - numberSellers);
        if (sellers.size() < numberSellers) {
            for (int i = 0; i < deltaSellers; i++) {
                logMicro("Adding new seller");
                PoolableAgentBrain seller = addSeller();
                Future future = marketAgentThreadService.submit(seller);
                seller.setFuture(future);
                seller.startCycle();
            }
        } else if (sellers.size() > numberSellers) {
            for (int i = 0; i < deltaSellers; i++) {
                logMicro("Requesting exit from a seller //" + i);
                actionsToPerform.addAll(sendExitRequestToSeller());
            }
        }

        actionsToPerform.addAll(notifyBuyersAboutMarketChanges());
        return actionsToPerform;
    }

    private List<Action> notifyBuyersAboutMarketChanges() {
        List<Action> actionsToPerform = new ArrayList<>();

        List<String> buyerIds = new ArrayList<>();
        buyerIds.addAll(selectedBuyers.keySet());
        buyerIds.addAll(competitors.keySet());

        // total buyers in market - 1
        int numOtherBuyers = numberCompetitors + selectedBuyers.size() - 1;
        logMicro("notifyComp(" + numOtherBuyers +")");
        for (String buyerId : buyerIds) {
            actionsToPerform.add(new NotifyAboutChangeInDemandSupplyRatioAction.Builder()
                    .setRatio(currentRateRatio)
                    .setRecipient(buyerId)
                    .build());

            actionsToPerform.add(new NotifyAboutChangeInNumberOfCompetitors.Builder()
                    .setNumCompetitors(numOtherBuyers)
                    .setRecipient(buyerId)
                    .build());
        }
        return actionsToPerform;
    }
    
    private List<Action> sendExitRequestToSeller() {
        List<Action> actionsToPerform = new ArrayList<>();

        List<String> keys = new ArrayList<>(sellers.keySet());
        Collections.shuffle(keys);

        for (String seller : keys) {
            if (!departingAgents.contains(seller)) {
                departingAgents.add(seller);
                actionsToPerform.add(new NotifyLeaveMarketAction.Builder().setRecipient(seller).build());
                break;
            }
        }
        return actionsToPerform;
    }
    
    private List<Action> sendExitRequestToCompetitor() {
        List<Action> actionsToPerform = new ArrayList<>();

        List<String> keys = new ArrayList<>(competitors.keySet());
        Collections.shuffle(keys);

        for (String competitor : keys) {
            if (!departingAgents.contains(competitor)) {
                departingAgents.add(competitor);
                actionsToPerform.add(new NotifyLeaveMarketAction.Builder().setRecipient(competitor).build());
                break;
            }
        }
        return actionsToPerform;
    }
    
    /**
     * Calculates the correct number of sellers and competitors that should be
     * in the marketplace.
     * 
     * The chosen values are based on the ratio of buyers:sellers in the market,
     * and the number of selected buyers that are currently in the marketplace.
     * 
     * If a buyer for which the user is collected metrics has completed its
     * task and leaves the marketplace, then a competitor will be added to
     * ensure the ratio of buyers to sellers, and the total number of
     * MarketAgents in the marketplace stay correct.
     * 
     * Ensures that there are always a minimum of 0 competitors and 0 sellers
     * in the marketplace; no negative values.
     * 
     */
    private void calculateNumberOfMarketAgents() {
    	log("Calculating number of market agents");
        currentRateRatio = (Ratio) changeRateRatioVar.get(
                clicker.getCurrentIndex(changeRateRatioVar)).pickValue();
                        
        int density = (int) changeRateDensityVar.get(
                clicker.getCurrentIndex(changeRateDensityVar)).pickValue();
        
        // Note, this is the number of competitors that should be in the
        // marketplace and the reference to "selectedBuyersTrading" is to
        // ensure there is always the correct number of buyers in total.
        numberCompetitors = Math.max(0,  
                Math.round(density * currentRateRatio.getAntecedent()
                / (currentRateRatio.getAntecedent() + currentRateRatio.getConsequent()))
                - selectedBuyers.size()); 
        
        numberSellers = Math.max(0, density - numberCompetitors);
    }

    /**
     * Creates a buyer agent of the specified type/strategy, with the given
     * agent ID.
     * 
     * @param strategy
     * @param agentId
     * @return
     */
    private PoolableAgentBrain createBuyerAgent(String strategy, String agentId) {
        switch (strategy) {
        	case "SimpleBuyer":
        		return AgentFactory.createSimpleBuyerAgent(agentId,
                    container, params, PRODUCT_ID);
            case "FaratinTimeDependentLinearBuyer":
                return AgentFactory.createFaratinTimeDependentLinearBuyer(agentId,
                        container, params, PRODUCT_ID);
                
            case "FaratinTimeDependentConcederBuyer":
                return AgentFactory.createFaratinTimeDependentConcederBuyer(agentId,
                        container, params, PRODUCT_ID);
                
            case "FaratinTimeDependentBoulwareBuyer":
                return AgentFactory.createFaratinTimeDependentBoulwareBuyer(agentId,
                        container, params, PRODUCT_ID);

            case "FaratinResourceDependentBuyer":
                return AgentFactory.createFaratinResourceDependentBuyer(agentId,
                        container, params, PRODUCT_ID);
                
            case "FaratinResourceTimeDependentBuyer":
            	return AgentFactory.createFaratinResourceTimeDependentBuyer(agentId,
                        container, params, PRODUCT_ID);
                
            case "FaratinRelativeTitForTatBuyer":
                return AgentFactory.createFaratinRelativeTitForTatBuyer(agentId,
                        container, params, PRODUCT_ID);
                
            case "FaratinRandomAbsoluteTitForTatBuyer":
                return AgentFactory.createFaratinRandomAbsoluteTitForTatBuyer(agentId,
                        container, params, PRODUCT_ID);
                
            case "FaratinAverageTitForTatBuyer":
                return AgentFactory.createFaratinAverageTitForTatBuyer(agentId,
                        container, params, PRODUCT_ID);
                
            case "WilliamsIAmHagglerBuyer":
                return AgentFactory.createWilliamsBuyerAgent(agentId,
                        container, params, PRODUCT_ID);
                
                // TODO: add cases for other Java agents

            default:
                Path prologMind = Paths.get(appData, strategy);
                log("is there a prolog file at: " + prologMind.toString());
                if (prologMind.toFile().exists()) {
                	return AgentFactory.createConbinePrologAgent(agentId,
                            prologMind.toFile().getAbsolutePath(),
                            AgentType.BUYER, container, params, PRODUCT_ID);
                }
                log("Trying to create agent failed, unrecognised option: "
                                + strategy);
                return null;
        }
    }

    /**
     * Creates a seller agent of the specified type/strategy, with the given
     * agent ID.
     * 
     * @param strategy
     * @param agentId
     * @return
     */
    private PoolableAgentBrain createSellerAgent(String strategy, String agentId) {
        switch (strategy) {
            case "SimpleSeller":
                return AgentFactory.createSimpleSellerAgent(agentId, container,
                        params, PRODUCT_ID);
                
            case "FaratinTimeDependentLinearSeller":
                return AgentFactory.createFaratinTimeDependentLinearSeller(agentId, container,
                        params, PRODUCT_ID);

            case "FaratinTimeDependentConcederSeller":
                return AgentFactory.createFaratinTimeDependentConcederSeller(agentId,
                        container, params, PRODUCT_ID);
                
            case "FaratinTimeDependentBoulwareSeller":
                return AgentFactory.createFaratinTimeDependentBoulwareSeller(agentId,
                        container, params, PRODUCT_ID);

            case "FaratinResourceDependentSeller":
                return AgentFactory.createFaratinResourceDependentSeller(agentId,
                        container, params, PRODUCT_ID);
                
            case "FaratinResourceTimeDependentSeller":
                return AgentFactory.createFaratinResourceTimeDependentSeller(agentId,
                        container, params, PRODUCT_ID);  
                
            case "FaratinRelativeTitForTatSeller":
                return AgentFactory.createFaratinRelativeTitForTatSeller(agentId,
                        container, params, PRODUCT_ID);
                
            case "FaratinRandomAbsoluteTitForTatSeller":
                return AgentFactory.createFaratinRandomAbsoluteTitForTatSeller(agentId,
                        container, params, PRODUCT_ID);  
                
            case "FaratinAverageTitForTatSeller":
                return AgentFactory.createFaratinAverageTitForTatSeller(agentId,
                        container, params, PRODUCT_ID);  
                
                // TODO: add cases for other Java agents

            default:
                Path prologMind = Paths.get(appData, strategy);
                if (prologMind.toFile().exists()) {
                    return AgentFactory.createConbinePrologAgent(agentId,
                            prologMind.toFile().getAbsolutePath(),
                            AgentType.SELLER, container, params, PRODUCT_ID);
                }
                log(
                        "Trying to create agent failed, unrecognised option: "
                                + strategy);
                return null;
        }
    }

    private void createMarketBroker() {
        AgentBrain broker = AgentFactory.createMarketBroker(MARKET_BROKER, container);
        broker.startCycle();
    }
    
    private List<Action> decideBasedOnExitAll(ExitAllAction exitAction) {
        List<Action> actionsToPerform = new ArrayList<>();
        String id = exitAction.getAgentId();
        logMicro( "heard an exit all from: " + exitAction.getAgentId());

        if (selectedBuyers.containsKey(id)) {
            AgentBrain agent = selectedBuyers.get(id);
            agent.stopCycle();
            container.removeEntity(id);
            selectedBuyers.remove(id);
            log("selectedbuyer removed from container: " + id);

            if (selectedBuyers.size() == 0) {
                return onRunComplete();
            }

            return actionsToPerform;
        }

        // a seller/competitor is leaving
        log ("agentRemove, depagents pre: " + departingAgents.size());
        departingAgents.remove(id);
        log ("agentRemove, depagents post: " + departingAgents.size());

        if (sellers.containsKey(id)) {
        	AgentBrain agent = sellers.get(id);
            agent.stopCycle();
            container.removeEntity(id);
            sellers.remove(id);

        } else if (competitors.containsKey(id)) {
            AgentBrain agent = competitors.get(id);
            agent.stopCycle();
            container.removeEntity(id);
            competitors.remove(id);
            
        } else {
            log("EXIT_ALL heard from unknown agent: " + id);
            return actionsToPerform;
        }

        logMicro( "processed an exit all from " + exitAction.getAgentId());
        return actionsToPerform;
    }

    private PoolableAgentBrain addSeller() {
        String agentId = "s_" + inconsequentialMarketAgentCount++;
        PoolableAgentBrain newSeller = createSellerAgent(
                sellersTypePool[random.nextInt(sellersTypePool.length)],
                agentId);
        sellers.put(agentId, newSeller);
        return newSeller;
    }

    private void initialiseSelectedBuyers() {
        int counter = 1;
        for (String buyerStrategy : userSelectedBuyers) {
            String agentId = "buyer_" + counter;
            logMicro( "initialise " + agentId + " using " + buyerStrategy);
            selectedBuyers.put(agentId,
                    createBuyerAgent(buyerStrategy, agentId));
            counter++;
        }

        for (PoolableAgentBrain buyer : selectedBuyers.values()) {
            Future future = marketAgentThreadService.submit(buyer);
            buyer.setFuture(future);
            buyer.startCycle();
        }
    }

    private PoolableAgentBrain addCompetitor() {
		String agentId = "b_" + inconsequentialMarketAgentCount++;
        PoolableAgentBrain newCompetitor = createBuyerAgent(
                competitorTypePool[random.nextInt(competitorTypePool.length)],
                agentId);
        competitors.put(agentId, newCompetitor);
        return newCompetitor;
	}

	private void initialiseCompetitors() {
	    List<PoolableAgentBrain> competitors = new ArrayList<>();
        for (int i = 0; i < numberCompetitors; i++) {
	        competitors.add(addCompetitor());
	    }

        for (PoolableAgentBrain competitor : competitors) {
            Future future = marketAgentThreadService.submit(competitor);
            competitor.setFuture(future);
            competitor.startCycle();
        }
	}

	private void initialiseSellers() {
        List<PoolableAgentBrain> sellers = new ArrayList<>();
        for (int i = 0; i < numberSellers; i++) {
            sellers.add(addSeller());
        }

        for (PoolableAgentBrain seller : sellers) {
            Future future = marketAgentThreadService.submit(seller);
            seller.setFuture(future);
            seller.startCycle();
        }
    }

    /**
     * Prints the container history of the current run to a file.
     *
     * @throws IOException
     */
    private void printRunToFile() throws IOException {
        ContainerHistory history = container.getHistory();
        if (history.size() == 0) {
            return;
        }

        log("Printing to file");
        final String delimiter = ":::";
        Path output = Paths.get(appData, experimentLabel,
                clicker.getCurrentCombination() + "_" + currentRun + ".runhistory");
        output.toFile().getParentFile().mkdirs();
        output.toFile().createNewFile();

        BufferedWriter writer = Files.newBufferedWriter(output,
                StandardCharsets.UTF_8, StandardOpenOption.TRUNCATE_EXISTING,
                StandardOpenOption.CREATE, StandardOpenOption.WRITE);

        writer.write("TIMESTAMP:::FROM:::[TO:::]ACTION\n");
        for (int i = 0; i < history.size(); i++) {

            Event event = history.get(i);
            String recipient = event.getAction().getRecipient();
            recipient = (recipient != null && recipient.length() > 0) ? recipient + delimiter : "";

            String line = event.getTimestamp() + delimiter +
                    event.getInitiatorId() + delimiter +
                    recipient +
                    event.getAction().toString() + "\n";

            writer.write(line);
        }
        writer.close();
    }
}
