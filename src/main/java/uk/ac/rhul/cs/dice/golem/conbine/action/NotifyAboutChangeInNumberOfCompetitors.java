package uk.ac.rhul.cs.dice.golem.conbine.action;

import org.javatuples.Unit;

import uk.ac.rhul.cs.dice.golem.action.Action;

/**
 * As per {@link ConbineActionType#NOTIFY_ABOUT_CHANGE_IN_NUMBER_OF_COMPETITORS}
 * .
 * 
 * @author ataulm
 * 
 */
public class NotifyAboutChangeInNumberOfCompetitors extends Action {
    public static class Builder {
        private String recipientId;
        private int numCompetitors = -1;

        public NotifyAboutChangeInNumberOfCompetitors build() {
            if (numCompetitors < 0) {
                throw new IllegalStateException(
                        "The number of competitors must be set.");
            }

            if (recipientId == null || recipientId.length() == 0) {
                throw new IllegalStateException(
                        "Recipient must be set for notify actions.");
            }

            Unit<Integer> payload = new Unit<>(numCompetitors);

            return new NotifyAboutChangeInNumberOfCompetitors(recipientId,
                    payload);
        }

        public Builder setNumCompetitors(int numCompetitors) {
            this.numCompetitors = numCompetitors;
            return this;
        }

        public Builder setRecipient(String recipientId) {
            this.recipientId = recipientId;
            return this;
        }
    }

    public static final int NUM_COMPETITORS = 0;

    protected NotifyAboutChangeInNumberOfCompetitors(String recipient,
            Unit<Integer> payload) {
        super(ConbineActionType.NOTIFY_ABOUT_CHANGE_IN_NUMBER_OF_COMPETITORS
                .toString(), recipient, payload);
    }

    public int getNumberOfCompetitors() {
        return (int) getPayload().getValue(NUM_COMPETITORS);
    }

    @Override
    public String toString() {
        return getActionType() + "(" + getNumberOfCompetitors() + ")";
    }
}
