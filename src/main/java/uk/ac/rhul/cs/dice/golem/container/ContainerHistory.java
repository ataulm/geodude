package uk.ac.rhul.cs.dice.golem.container;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import uk.ac.rhul.cs.dice.golem.action.Event;
import uk.ac.rhul.cs.dice.golem.util.Logger;

/**
 * An ordered collection of {@link Event} objects.
 * 
 * @author ataulm
 */
public class ContainerHistory {
    private final List<Event> events;
    
    public ContainerHistory() {
        events = Collections.synchronizedList(new ArrayList<Event>());
    }
    
    /**
     * Returns a copy of the specified History, populated with references to
     * the immutable {@link Event} objects in the original.
     *
     * Note, Events are still being added to the source ContainerHistory but
     * these won't be included in the copied version (and they're not relevant).
     * 
     * @param original the History object to copy
     */
    public ContainerHistory(ContainerHistory original) {
        this();
        int historySize = original.size();
        Logger.print(this, "Creating ContainerHistory from existing container, size: " + historySize);
        for (int i = 0; i < historySize; i++) {
            assertEvent(original.get(i));
        }
        Logger.print(this, "ContainerHistory created");
    }
    
    public boolean assertEvent(Event event) {
        return events.add(event);   
    }
    
    public Event get(int i) {
        return events.get(i);
    }
    
    public int size() {
        return events.size();
    }
    
    public boolean clear() {
        events.clear();
        return (events.size() == 0) ? true : false;
    }
}
