package uk.ac.rhul.cs.dice.golem.util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.javatuples.Pair;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

/**
 * A common interface via which to save configuration files, with options added
 * as key-value pairs.
 * 
 * The key is used to identify an option <i>type</i>, rather than a unique
 * option.
 * 
 * Values are expressed as Strings, and stored as a String array, which is
 * useful in cases where single options require multiple values.
 * 
 * Calling {@link ConfigurationIO#get(String)} will return an ArrayList of
 * String arrays. This allows multiple records to be added using a single key.
 * 
 * {@link ConfigurationIO#put(String, String...)} takes two or more arguments, a
 * String key, and a series of String values; this class will concatenate the
 * Strings into an array. Each valid call to this method will add a new entry;
 * it will not overwrite previous entries.
 * 
 * 
 * 
 * @author ataulm
 * 
 */
public class ConfigurationIO {
    private final ArrayList<Pair<String, String[]>> options;
    private final OnConfigurationIOCompleteListener context;

    public ConfigurationIO(OnConfigurationIOCompleteListener context) {
        this.context = context;
        options = new ArrayList<>();
    }

    /**
     * Saves all the stored options to file.
     */
    public void commit(final String file) {
        new Thread() {
            @Override
            public void run() {
                CSVWriter writer = null;
                try {
                    writer = new CSVWriter(new FileWriter(file), '\t');

                    ArrayList<String> entry = new ArrayList<>();

                    for (Pair<String, String[]> element : options) {
                        // key
                        entry.add(element.getValue0());

                        // values
                        Collections.addAll(entry, element.getValue1());

                        String[] toWrite = new String[entry.size()];
                        for (int i = 0; i < entry.size(); i++) {
                            toWrite[i] = entry.get(i);
                        }
                        // write entry in
                        writer.writeNext(toWrite);
                        if (!entry.isEmpty())
                            entry.clear();
                    }

                    // notify calling class that write completed successfully
                    context.writeComplete(OnConfigurationIOCompleteListener.SUCCESSFUL);
                } catch (IOException e) {
                    context.writeComplete(OnConfigurationIOCompleteListener.FAILURE);
                    e.printStackTrace();
                } finally {
                    try {
                        writer.close();
                    } catch (IOException e) {
                        Logger.e(this,
                                "IOException while trying to close CSVWriter object.");
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    /**
     * Returns the values for each entry with the given key
     * 
     * @param key
     * @return
     */
    public ArrayList<String[]> get(String key) {
        ArrayList<String[]> result = new ArrayList<>();
        for (Pair<String, String[]> entry : options) {
            if (entry.getValue0().equalsIgnoreCase(key))
                result.add(entry.getValue1());
        }
        return result;
    }

    /**
     * Returns the values for the first entry in the list with the specified
     * key.
     * 
     * @param key
     * @param singleEntry
     * @return
     */
    public String[] get(String key, boolean singleEntry) {
        if (singleEntry) {
            for (Pair<String, String[]> entry : options) {
                if (entry.getValue0().equalsIgnoreCase(key))
                    return entry.getValue1();
            }
        }
        return null;
    }

    /**
     * Returns the list of entries for this configuration object.
     * 
     * @return
     */
    protected ArrayList<Pair<String, String[]>> getOptions() {
        return options;
    }

    /**
     * Adds the specified value(s) using the specified key
     */
    public void put(String key, String... values) {
        options.add(new Pair<>(key, values));
    }

    /**
     * Reads the specified configuration file as values in this object, then
     * notifies the calling class that the read is complete (i.e. that this
     * configuration object is ready for interaction).
     * 
     * @param file
     */
    public void read(final String file) {
        if (!options.isEmpty())
            options.clear();
        new Thread() {
            @Override
            public void run() {
                CSVReader reader = null;
                try {
                    reader = new CSVReader(new FileReader(file), '\t', '\"',
                            '\0');
                    List<String[]> allLines = reader.readAll();

                    for (String[] line : allLines) {
                        Pair<String, String[]> entry;
                        String[] values = new String[line.length - 1];

                        System.arraycopy(line, 1, values, 0, values.length);

                        entry = new Pair<>(line[0], values);
                        options.add(entry);
                    }

                    // notify calling class that the read completed successfully
                    context.readComplete(OnConfigurationIOCompleteListener.SUCCESSFUL);

                } catch (FileNotFoundException e) {
                    Logger.e(this, "Specified configuration file doesn't exist: "
                            + file);
                    context.readComplete(OnConfigurationIOCompleteListener.FAILURE);
                    return;
                } catch (IOException e) {
                    context.readComplete(OnConfigurationIOCompleteListener.FAILURE);
                    e.printStackTrace();
                } finally {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        Logger.e(this,
                                "IOException while trying to close CSVReader object.");
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

}
