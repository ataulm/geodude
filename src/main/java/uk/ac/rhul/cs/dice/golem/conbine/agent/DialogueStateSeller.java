package uk.ac.rhul.cs.dice.golem.conbine.agent;

public interface DialogueStateSeller extends DialogueState {
	/**
	 * Returns the start time of this dialogue.
	 * 
	 * The time is represented as milliseconds since the epoch.
	 * 
	 * @return startTime  the time the first offer (of this dialogue) was received
	 */
	public long getStart();
}
