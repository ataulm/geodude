package uk.ac.rhul.cs.dice.golem.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import uk.ac.rhul.cs.dice.golem.agent.AgentBody;

public abstract class AbstractSensor implements Sensor {
    private final ArrayList<String> types = new ArrayList<>();
    private final String id;
    private final AgentBody context;
    private SensorHasPerceptListener listener;
    private final Queue<Percept> perceptions;

    protected AbstractSensor(String id, AgentBody context) {
        this.context = context;
        this.id = id + "_" + context.getId();
        perceptions = new ConcurrentLinkedQueue<>();
    }

    /**
     * Adds an action type that this {@link Sensor} can produce
     * 
     * @param type
     *            the action type to add
     */
    protected void addType(String type) {
        if (!types.contains(type))
            types.add(type);
    }

    @Override
    public ArrayList<String> getActionTypes() {
        return types;
    }

    @Override
    public AgentBody getBody() {
        return context;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Percept getPerception() {
        return perceptions.poll();
    }

    @Override
    public boolean handlesType(String type) {
        return types.contains(type);
    }

    @Override
    public void sense(Action action) {
        perceptions.add(new DefaultPercept(action, Calendar.getInstance()
                .getTimeInMillis()));
        if (listener != null) {
            listener.onSensorHasPercept(this);
        }
    }

    /**
     * Registers a listener who is interested in knowing when the Sensor has a
     * new Percept.
     * 
     * @param listener
     *            the listener to register
     */
    @Override
    public void setSensorHasPerceptListener(SensorHasPerceptListener listener) {
        this.listener = listener;
    }
}
