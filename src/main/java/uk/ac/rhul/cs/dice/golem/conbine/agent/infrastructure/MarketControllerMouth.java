package uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure;

import uk.ac.rhul.cs.dice.golem.action.AbstractEffector;
import uk.ac.rhul.cs.dice.golem.agent.AgentBody;
import uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType;

public class MarketControllerMouth extends AbstractEffector {

    public MarketControllerMouth(String id, AgentBody context) {
        super(id, context);
        addType(ConbineActionType.NOTIFY_RUN_COMPLETE.toString());
        addType(ConbineActionType.NOTIFY_ABOUT_CHANGE_IN_NUMBER_OF_COMPETITORS.toString());
        addType(ConbineActionType.NOTIFY_ABOUT_CHANGE_IN_DEMAND_SUPPLY_RATIO.toString());
        addType(ConbineActionType.NOTIFY_LEAVE_MARKET.toString());
        addType(ConbineActionType.COLLECT_METRICS.toString());
    }

}
