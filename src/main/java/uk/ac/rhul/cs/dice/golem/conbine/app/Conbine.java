package uk.ac.rhul.cs.dice.golem.conbine.app;

import uk.ac.rhul.cs.dice.golem.agent.AgentBrain;
import uk.ac.rhul.cs.dice.golem.application.AbstractApplication;
import uk.ac.rhul.cs.dice.golem.base.Base;
import uk.ac.rhul.cs.dice.golem.conbine.agent.AgentFactory;
import uk.ac.rhul.cs.dice.golem.conbine.ui.SetupUI;
import uk.ac.rhul.cs.dice.golem.container.Container;
import uk.ac.rhul.cs.dice.golem.container.DefaultContainer;
import uk.ac.rhul.cs.dice.golem.physics.Physics;

public class Conbine extends AbstractApplication {
    private static final String APP_NAME = "ConBiNe";
    private static final String APP_DATA = "ConBiNe_data";
    private static final String MARKET_CONTROLLER = "market_controller";

    private Container marketplace;
    private SetupUI setup;

    public Conbine() {
        super(APP_NAME, APP_DATA);
    }

    @Override
    public void launch() {
        Physics governor = new MarketRules();
        marketplace = new DefaultContainer("ConbineMarketplace", governor);

        // TODO: this should be replaced with values scraped from the UI
        ConbineUserInput userInput = new SimulatedUserInput();
        startMarketController(userInput);
    }

    private void startMarketController(ConbineUserInput userInput) {
        AgentBrain controller = AgentFactory.createMarketController(
                MARKET_CONTROLLER, marketplace, getAppDir(), userInput);
        controller.startCycle();
    }

    @Override
    protected void postInit() {
//        setup = new SetupUI(this, APP_NAME);
//        setup.setup();
        launch();
    }
    
    /**
     * A convenience method during testing: instead of running GOLEM and
     * launching the app, this'll do it in one go.
     * 
     * @param args
     */
    public static void main(String[] args) throws Exception {
        Base golemlite = new Base(null);
        Conbine conbineApp = new Conbine();
        golemlite.createApplication(conbineApp);
    }
}
