package uk.ac.rhul.cs.dice.golem.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;

import net.miginfocom.swing.MigLayout;
import uk.ac.rhul.cs.dice.golem.base.Base;

public class GolemGUI {

    private final Base base;

    /**
     * Main application window, including window controls
     */
    private JFrame frame;

    /**
     * JTabbedPane for the instances of the applications running in GOLEM.
     * Applications appear as tabs in this component.
     */
    private JTabbedPane desktopManager;

    /**
     * Launch menu
     */
    private JMenu mnLaunch;

    /**
     * Create the application.
     */
    public GolemGUI(Base base) throws Exception {
        this.base = base;

        if (Base.NATIVE_SYSTEM_THEMING) {
            // Set System look and feel (emulates host system UI)
            UIManager.setLookAndFeel(UIManager
                    .getSystemLookAndFeelClassName());
        }

        frame = new JFrame();
        initialize();
    }

    /* ACCESSORS */

    /**
     * Creates and initialises the main menu, returning the resulting JMenuBar
     * item.
     * 
     * @return
     */
    private JMenuBar createMenu() {
        JMenuBar menuBar = new JMenuBar();

        /* Main menu items */
        JMenu mnGolem = new JMenu("GOLEM");
        menuBar.add(mnGolem);

        mnLaunch = new JMenu("Launch");
        mnGolem.add(mnLaunch);

        JMenuItem mntmOpen = new JMenuItem("Open");
        mntmOpen.setEnabled(false);
        mnGolem.add(mntmOpen);

        JMenuItem mntmSaveAs = new JMenuItem("Save as");
        mntmSaveAs.setEnabled(false);
        mnGolem.add(mntmSaveAs);

        JSeparator separator = new JSeparator();
        mnGolem.add(separator);

        JMenuItem mntmExit = new JMenuItem("Exit");
        mntmExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                int result = JOptionPane.showConfirmDialog(frame,
                        "Exit without saving?", "Exit Application",
                        JOptionPane.YES_NO_OPTION);

                // Exit the application
                if (result == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }
        });
        mnGolem.add(mntmExit);

        JMenu mnHelp = new JMenu("Help");
        menuBar.add(mnHelp);

        JMenuItem mntmHowToUse = new JMenuItem("Help Contents");
        mnHelp.add(mntmHowToUse);

        JMenuItem mntmAbout = new JMenuItem("About");
        mnHelp.add(mntmAbout);
        /* end of Main menu items */

        return menuBar;
    }

    /**
     * Returns the spawning instance of GOLEM
     * 
     * @return
     */
    public Base getBase() {
        return base;
    }

    /**
     * Returns the StackDockStation holding the tabs
     * 
     * @return
     */
    public JTabbedPane getDesktopManager() {
        return desktopManager;
    }

    /* MUTATORS */

    /* OTHER */

    /**
     * Returns the main application frame
     * 
     * @return
     */
    public JFrame getMainFrame() {
        return frame;
    }

    /**
     * Initialize the contents of the main window.
     */
    private void initialize() {
        // main window
        frame.setBounds(100, 100, 1100, 800);
        frame.setTitle(Base.APP_NAME);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(
                new MigLayout("insets 0", "grow, fill", "grow, fill"));

        // Add menu bar (dock north, wrap subsequent items to next line, min
        // height 25px)
        frame.getContentPane().add(createMenu(), "north, wrap, hmin 25");

        // New-style application tab manager
        desktopManager = new JTabbedPane();
        desktopManager.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        frame.getContentPane().add(desktopManager);
    }

    /**
     * Updates the GOLEM menu, with the set of available applications
     * 
     * @param availableApps
     *            a collection of the names of available applications
     */
    public void setAvailableApplications(Set<String> availableApps) {
        for (final String app : availableApps) {
            // Create a menu item with the application name
            JMenuItem menuItem = new JMenuItem(app);

            menuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e){
                    try {
                        base.createApplication(app);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            });

            // Add the button to the Launch menu
            mnLaunch.add(menuItem);
        }
    }
}
