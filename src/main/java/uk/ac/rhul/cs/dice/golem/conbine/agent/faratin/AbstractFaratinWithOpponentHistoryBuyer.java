package uk.ac.rhul.cs.dice.golem.conbine.agent.faratin;

import java.util.List;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.agent.AgentBrain;
import uk.ac.rhul.cs.dice.golem.conbine.action.NegotiationAction;
import uk.ac.rhul.cs.dice.golem.conbine.agent.AbstractBuyerAgent;
import uk.ac.rhul.cs.dice.golem.conbine.agent.AgentParameters;
import uk.ac.rhul.cs.dice.golem.conbine.agent.DialogueState;
import uk.ac.rhul.cs.dice.golem.conbine.agent.OfferHistory;

@SuppressWarnings("serial")
public abstract class AbstractFaratinWithOpponentHistoryBuyer extends AbstractBuyerAgent {
    public AbstractFaratinWithOpponentHistoryBuyer(AgentBrain brain, AgentParameters params, String product) {
        super(brain, params, product);        
    }
    
	@Override
	protected List<Action> sendCounterOffer(NegotiationAction offer,
			double counterOffer) {
		DefaultDialogueStateFratinBuyerOfferHistory dialogueState =
				(DefaultDialogueStateFratinBuyerOfferHistory) getDialogues().get(offer.getDialogueId());
		
		dialogueState.updateMyLastBid(counterOffer);
		
		return super.sendCounterOffer(offer, counterOffer);
	}
    
    @Override
    protected DialogueState makeNewDialogueState(NegotiationAction offer) {
    	log("Make new dialogue state in abstract faratin");
		DialogueState state = new DefaultDialogueStateFratinBuyerOfferHistory(
                offer.getDialogueId(),
                offer.getProtocol(),
                offer.getReplyToId(),
                offer.getProductId());
		
		((OfferHistory) state).updateMyLastBid(Double.parseDouble(offer.getValue()));
        
        return state;
	}
}
