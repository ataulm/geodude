package uk.ac.rhul.cs.dice.golem.agent;

/**
 * Runs the agent on a new Thread dedicated to this agent.
 *
 * Use this implementation when an agent will stay alive for the entire simulation.
 * If an agent will die (and there are other agents being creating at runtime), then
 * use {@link DefaultPoolableAgentBrain} in conjunction with an ExecutorService.
 *
 */
@SuppressWarnings("serial")
public class DefaultAgentBrain extends AbstractAgentBrain {
    private Thread agentThread;
    private boolean keepThreadAlive;

    protected Thread getAgentThread() {
        return agentThread;
    }

    protected void setAgentThread(Thread agentThread) {
        this.agentThread = agentThread;
    }

    @Override
    public void startCycle() {
        if (agentThread == null) {
            setAgentRunning(true);
            keepThreadAlive = true;
            agentThread = new Thread(this);
            agentThread.setName(getAgentId());
            agentThread.start();
        }
    }

    @Override
    public void startSuspended() {
        setAgentRunning(false);
        keepThreadAlive = true;
        agentThread = new Thread(this);
        agentThread.start();
    }

    @Override
    public void resumeCycle() {
        setAgentRunning(true);
    }

    @Override
    public void suspendCycle() {
        setAgentRunning(false);
    }

    @Override
    public void stopCycle() {
        setAgentRunning(false);
        agentThread = null;
        keepThreadAlive = false;
    }

    @Override
    public void run() {
        if (agentThread == Thread.currentThread()) {
            while (keepThreadAlive) {
                if (isAgentRunning()) {
                    stepForward();
                }
            }
        }
    }
}
