package uk.ac.rhul.cs.dice.golem.agent;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.action.Percept;
import alice.tuprolog.InvalidTheoryException;
import alice.tuprolog.MalformedGoalException;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Theory;

@SuppressWarnings("serial")
public abstract class PrologAgentMind extends AbstractAgentMind {

    private final Prolog interpreter;
    private static final String ASSERT_PRE = "assert(";
    private static final String ASSERT_POST = ").";
    private static final String INITIALLY_PRE = "initially(";
    private static final String INITIALLY_POST = ")";

    protected PrologAgentMind(String theoryFilePath, AgentBrain brain) {
        super(brain);
        interpreter = new Prolog();

        setTheory(theoryFilePath);
        setLogLevel(MICRO);
    }

    protected void addTheory(String theoryFilePath) {
        Theory theory = null;
        try {
            theory = new Theory(new FileInputStream(theoryFilePath));
        } catch (IOException e) {
            log("Theory file not found, theory not set ("
                    + theoryFilePath + ")");
            e.printStackTrace();
            return;
        }
        try {
            interpreter.addTheory(theory);
        } catch (InvalidTheoryException e) {
            log("Invalid theory, theory not set (" + theoryFilePath
                    + ")");
            e.printStackTrace();
        }
    }

    protected SolveInfo assertInitially(String term)
            throws MalformedGoalException {
        log(INITIALLY_PRE + term + INITIALLY_POST);
        return assertTerm(INITIALLY_PRE + term + INITIALLY_POST);
    }

    protected SolveInfo assertTerm(String term) throws MalformedGoalException {
        log(ASSERT_PRE + term + ASSERT_POST);
        return interpreter.solve(ASSERT_PRE + term + ASSERT_POST);
    }

    /**
     * Converts a {@link Percept} to a String term that can be passed to the
     * {@link Prolog} interpreter in {@link Prolog#solve(String)}.
     * 
     * @param percept
     *            the Percept to convert
     * @return term the term to solve
     */
    protected abstract String convertPerceptToTerm(Percept percept);

    /**
     * 
     * @param term
     * @return Action
     */
    protected abstract Action convertTermToAction(String term);

    /**
     * Gets all unread perceptions from the {@link AgentBrain}, asserts them to
     * the Prolog knowledge base, then requests a set of {@link Action} objects
     * to pass back to the AgentBrain to perform.
     */
    @Override
    public List<Action> executeStep() throws Exception {
    	List<Percept> percepts = getBrain().getAllPerceptions();
        for (Percept percept : percepts) {
            log("received percept in executeStep(): "
                    + percept.getPerceptContent().toString());
            assertTerm(convertPerceptToTerm(percept));
        }

        final ArrayList<Action> actionsToPerform = new ArrayList<>();

        logMicro("Requesting list of actions");
        SolveInfo solution = requestListOfActions("select(Actions).");

        if (solution.isSuccess() && solution.getTerm("Actions").toString().length() > 2) {
            log("solution found: " + solution.getSolution().toString());
            log("solution found all: "
                    + solution.getTerm("Actions").toString());

            // listOfActions == "[action(x), action(y), ..., action(z)]"
            String listOfActions = solution.getTerm("Actions").toString();

            // get rid of square brackets on either side
            listOfActions = listOfActions.substring(1,
                    listOfActions.length() - 1);

            // get rid of whitespace
            listOfActions = listOfActions.replaceAll(" ", "");

            // separate actions using a unique/unusual delimiter (";;;")
            final String delimiter = ";;;";
            listOfActions = listOfActions.replaceAll(",action", delimiter
                    + "action");

            // create an array of actions
            String[] actionTerms = listOfActions.split(delimiter);

            // for each action
            for (String actionTerm : actionTerms) {
                // get meat of action: "action(x)" -> "x"
            	log("actionTerm: " + actionTerm);
                String action = actionTerm.substring("action(".length(),
                        actionTerm.length() - 1);

                if (action != null)
                    log(" returned action: "
                            + action);
                actionsToPerform.add(convertTermToAction(action));
            }
        } else {
            log("solution not found for query: "
                    + solution.getQuery().toString());
        }

        return actionsToPerform;
    }

    /**
     * Allows subclasses to specify their own goal for requesting of Actions.
     * 
     * This allows them to send extra parameters (e.g. current time), but the
     * returned solution should still be a list of Actions:
     * 
     * "[action(x), action(y), ..., action(z)]"
     * 
     * @param goal
     *            the goal that will request a list of actions
     * @return solution solution which should be empty or contain a list of
     *         Actions
     * @throws MalformedGoalException
     */
    protected SolveInfo requestListOfActions(String goal)
            throws MalformedGoalException {
    	logMicro("interpreter.solve: " + goal);
        return interpreter.solve(goal);
    }

    protected void setTheory(String theoryFilePath) {
        Theory theory = null;
        try {
            theory = new Theory(new FileInputStream(theoryFilePath));
        } catch (IOException e) {
            log("Couldn't read theory file (is it there?), theory not set ("
                            + theoryFilePath + ")");
            e.printStackTrace();
            return;
        }
        try {
            interpreter.setTheory(theory);
        } catch (InvalidTheoryException e) {
            log("Invalid theory, theory not set (" + theoryFilePath
                    + ")");
            e.printStackTrace();
        }
    }
}
