package uk.ac.rhul.cs.dice.golem.util;

public interface OnConfigurationIOCompleteListener {
    public final static int FAILURE = 0;
    public final static int SUCCESSFUL = 1;

    void readComplete(int result);

    void writeComplete(int result);
}
