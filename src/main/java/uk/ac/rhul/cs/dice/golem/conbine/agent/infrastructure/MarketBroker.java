package uk.ac.rhul.cs.dice.golem.conbine.agent.infrastructure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.action.Percept;
import uk.ac.rhul.cs.dice.golem.agent.AbstractAgentMind;
import uk.ac.rhul.cs.dice.golem.agent.AgentBrain;
import uk.ac.rhul.cs.dice.golem.conbine.agent.MarketAgent.AgentType;
import uk.ac.rhul.cs.dice.golem.conbine.action.AnnounceEntranceAction;
import uk.ac.rhul.cs.dice.golem.conbine.action.ConbineActionType;
import uk.ac.rhul.cs.dice.golem.conbine.action.ExitAllAction;
import uk.ac.rhul.cs.dice.golem.conbine.action.NotifyAboutNewSellerAction;
import uk.ac.rhul.cs.dice.golem.container.Container;
import uk.ac.rhul.cs.dice.golem.container.Environment;

/**
 * MarketBroker is an infrastructure agent which notifies buyers with the IDs of
 * suitable sellers in the market.
 * 
 * The MarketBroker listens for actions of type {@link ExitAllAction} and
 * {@link AnnounceEntranceAction}, and sends actions of type
 * {@link NotifyAboutNewSellerAction} (used to send a single seller agent ID) to
 * a buyer that is interested.
 * 
 * An infrastructure agent is aware of the application infrastructure, for
 * example, where a regular agent would see an {@link Environment}, the
 * infrastructure agent would see a {@link Container}.
 * 
 * Infrastructure agents, given their close-coupling with the application,
 * cannot be customised by the user; the strategy is set by the application
 * developer only, and are used to serve other agents, or perform some task for
 * the end-user, like collecting metrics. They are part of the infrastructure.
 * 
 * @author ataulm
 * 
 */
@SuppressWarnings("serial")
public class MarketBroker extends AbstractAgentMind {

    private int dialogueId;

    /**
     * Maps product ID to a list of sellers which are selling that product
     */
    private final Map<String, List<String>> sellers;

    /**
     * Maps product ID to a list of buyers that are looking for that product
     */
    private final Map<String, List<String>> buyers;

    public MarketBroker(AgentBrain brain) {
        super(brain);
        buyers = new HashMap<>();
        sellers = new HashMap<>();
        setLogLevel(STANDARD);
    }

    @Override
    public List<Action> executeStep() throws Exception {
        List<Percept> percepts = getBrain().getAllPerceptions();
        List<Action> actionsToPerform = new ArrayList<>();

        for (Percept percept : percepts) {
            Action action = percept.getPerceptContent();
            String type = action.getActionType();
            if (type.equals(ConbineActionType.NOTIFY_RUN_COMPLETE.toString())) {
            	clearRegister();
            	
            } else if (type.equals(ConbineActionType.ANNOUNCE_ENTRANCE.toString())) {
                actionsToPerform
                        .addAll(processNewAgent((AnnounceEntranceAction) action));
            } else if (type.equals(ConbineActionType.EXIT_ALL.toString())) {
                processLeavingAgent((ExitAllAction) action);
            }
        }

        int actionCount = 1;
	    for (Action action:actionsToPerform) {
	    	logMicro(getBrain().getAgentId() + " to perform: " +
	    			actionCount++ + ". " + action.toString());
	    }
        return actionsToPerform;
    }

    /**
     * Removes reference of the leaving agent from any registers.
     * 
     * The MarketBroker uses registers to keep track of which buyers and sellers
     * are in the market, and what they're looking for so it can notify them or
     * pass details on if requested - calling this method when an agent leaves
     * ensures that stale details are not passed on.
     * 
     * @param action
     *            the {@link ExitAllAction} produced by the leaving agent
     */
    private void processLeavingAgent(ExitAllAction action) {
        String leavingAgentId = action.getAgentId();
        String agentType = action.getAgentType();
        String productId = action.getProductId();

        log("processing leaving agent called: " + leavingAgentId
                + ", of type: " + agentType);
        
        List<String> register = null;
        
        if (agentType.equals(AgentType.BUYER.toString())) {
        	register = buyers.get(productId);
        } else if (agentType.equals(AgentType.SELLER.toString())) {
            register = sellers.get(productId);
        }
        
        // clearRegister may have been called && this thread can be slow!
        if (register != null) {
            register.remove(leavingAgentId);
        }
    }

    /**
     * Updates registers with information of new agent.
     * 
     * The MarketBroker uses registers to keep track of which buyers and sellers
     * are in the market, and what they're looking for so it can notify them or
     * pass details on if requested.
     * 
     * This will notify new buyers of existing sellers, or existing buyers of a
     * new seller, if the product type they are interested in matches.
     * 
     * @param action
     *            the {@link AnnounceEntranceAction} produced by the new agent
     * @return messagesToSend a set of messages notifying new buyers of existing
     *         sellers or existing buyers of a new seller
     */
    private List<Action> processNewAgent(AnnounceEntranceAction action) {
    	List<Action> messagesToSend = new ArrayList<>();

        String newAgentId = action.getAgentId();
        String agentType = action.getAgentType();
        String productId = action.getProductId();

        log("processing new agent called: " + newAgentId
                + ", of type: " + agentType);

        if (agentType.equals(AgentType.BUYER.toString())) {
            List<String> interestedBuyers = buyers.get(productId);
            if (interestedBuyers == null) {
                interestedBuyers = new ArrayList<>();
                buyers.put(productId, interestedBuyers);
            }

            if (!interestedBuyers.contains(newAgentId)) {
                interestedBuyers.add(newAgentId);
                logMicro("new buyer added, total buyer size: " + buyers.get(productId).size());
            }

            List<String> relevantSellers = sellers.get(productId);

            if (relevantSellers != null && !relevantSellers.isEmpty()) {
                for (String seller : relevantSellers) {
                	messagesToSend.add(new NotifyAboutNewSellerAction.Builder()
                            .setNewSellerId(seller)
                            .setRecipient(newAgentId)
                            .setDialogueId("" + dialogueId++).build());
                }
            }

        } else if (agentType.equals(AgentType.SELLER.toString())) {
            List<String> relatedSellers = sellers.get(productId);

            if (relatedSellers == null) {
                relatedSellers = new ArrayList<>();
                sellers.put(productId, relatedSellers);
            }

            if (!relatedSellers.contains(newAgentId)) {
                relatedSellers.add(newAgentId);
                logMicro("new seller added, total seller size: " + sellers.get(productId).size());
            }

            // notify all buyers about a new seller
            List<String> interestedBuyers = buyers.get(productId);

            if (interestedBuyers != null && !interestedBuyers.isEmpty()) {
                for (String buyer : interestedBuyers) {
                	messagesToSend.add(new NotifyAboutNewSellerAction.Builder()
                            .setNewSellerId(newAgentId)
                            .setRecipient(buyer)
                            .setDialogueId("" + dialogueId++).build());
                }
            }
        }

        for (Action message : messagesToSend) {
            logMicro("sending msg: " + message.toString());
        }

        return messagesToSend;
    }
    
    private void clearRegister() {
    	buyers.clear();
    	sellers.clear();
        dialogueId = 0;
    }
}
