package uk.ac.rhul.cs.dice.golem.container;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.action.Action.ActionType;
import uk.ac.rhul.cs.dice.golem.action.Event;
import uk.ac.rhul.cs.dice.golem.action.Sensor;
import uk.ac.rhul.cs.dice.golem.agent.AgentBody;
import uk.ac.rhul.cs.dice.golem.entity.Entity;
import uk.ac.rhul.cs.dice.golem.physics.Physics;
import uk.ac.rhul.cs.dice.golem.util.Logger;

/**
 * TODO: the first upgrade to Geodude will be the reintroduction of a proper
 * transportation layer. Discussion with Kostas required.
 *
 */
public abstract class AbstractContainer implements Container {
    private final Physics governor;
    private final ContainerIdFormer idFormer;
    private final Map<String, ArrayList<AgentBody>> subscriptions;
    public static final int RANDOM_PORT = 0;

    protected AbstractContainer(String name, Physics governor) {
        this(name, RANDOM_PORT, governor);
    }
    
    protected AbstractContainer(String name, int port, Physics governor) {
        this.governor = governor;
        subscriptions = new ConcurrentHashMap<>();
        idFormer = new ContainerIdFormer(name, port);
    }
    
    @Override
    public final void attempt(Entity entity, Action action) {
        // only entities registered with the container can attempt actions
        if (getEntity(entity.getId()) == null) {
            return;
        }

    	Event proposedEvent = new Event(entity.getId(), action, Calendar
                .getInstance().getTimeInMillis());

        if (governor.attempt(proposedEvent)) {
        	Logger.micro(this, "Governer approves action, assert it | Entity: "
                    + entity.getId() + ", action: " + action.toString());
            assertEventInHistory(proposedEvent);
            notifyAll(proposedEvent);
        }
    }

    protected Physics getGovernor() {
        return governor;
    }

    @Override
    public String getId() {
        return idFormer.toString();
    }

    protected ContainerIdFormer getIdFormer() {
        return idFormer;
    }

    /**
     * Notify every {@link Entity} that needs to know that the {@link Event}
     * occurred.
     * 
     * @param event
     *            the Event to notify Entities about
     */
    protected abstract void notifyAll(Event event);

    /**
     * Notify all agents that are subscribed to this particular
     * {@link ActionType}.
     * 
     * @param action
     */
    protected void notifySubscribed(Action action) {
        Logger.micro(this,
                "notifySubscribed(action), action = " + action.toString());
        ArrayList<AgentBody> subscribedAgents = subscriptions.get(action
                .getActionType());
        if (subscribedAgents == null)
            return;
        for (AgentBody agent : subscribedAgents) {
            agent.perceive(action);
        }
    }

    @Override
    public void subscribe(AgentBody agent, Sensor sensor) {
    	Logger.i(this, "subscribing agent: sensor " + sensor.getId());

        for (String actionType : sensor.getActionTypes()) {
            if (!subscriptions.containsKey(actionType)) {
                subscriptions.put(actionType, new ArrayList<AgentBody>());
            }

            ArrayList<AgentBody> subscribedAgents = subscriptions
                    .get(actionType);

            if (subscribedAgents == null)
                subscribedAgents = new ArrayList<>();
            subscribedAgents.add(agent);
        }
    }
    
    protected boolean clearSubscriptions() {
    	subscriptions.clear();
    	return (subscriptions.size() == 0) ? true : false;
    }
}
