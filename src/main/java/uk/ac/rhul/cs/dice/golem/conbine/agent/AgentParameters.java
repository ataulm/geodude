package uk.ac.rhul.cs.dice.golem.conbine.agent;

import java.util.HashMap;
import java.util.Map;

public class AgentParameters {
    public static final int BUYER_INITIAL = 0;
    public static final int BUYER_RESERVATION = 1;
    public static final int SELLER_INITIAL = 2;
    public static final int SELLER_RESERVATION = 3;
    public static final int DEADLINE = 4;

    private final Map<Integer, Integer> params;

    public AgentParameters() {
        params = new HashMap<>();
        params.put(BUYER_INITIAL, 0);
        params.put(BUYER_RESERVATION, 0);
        params.put(SELLER_INITIAL, 0);
        params.put(SELLER_RESERVATION, 0);
        params.put(DEADLINE, 0);
    }

    public AgentParameters(AgentParameters copy) {
        params = new HashMap<>();
        params.put(BUYER_INITIAL, copy.get(BUYER_INITIAL));
        params.put(BUYER_RESERVATION, copy.get(BUYER_RESERVATION));
        params.put(SELLER_INITIAL, copy.get(SELLER_INITIAL));
        params.put(SELLER_RESERVATION, copy.get(SELLER_RESERVATION));
        params.put(DEADLINE, copy.get(DEADLINE));
    }

    public AgentParameters(int buyerInitial, int buyerRes, int sellerInitial,
            int sellerRes, int deadline) {
        params = new HashMap<>();
        params.put(BUYER_INITIAL, buyerInitial);
        params.put(BUYER_RESERVATION, buyerRes);
        params.put(SELLER_INITIAL, sellerInitial);
        params.put(SELLER_RESERVATION, sellerRes);
        params.put(DEADLINE, deadline);
    }

    public void copyValues(AgentParameters copy) {
        params.put(BUYER_INITIAL, copy.get(BUYER_INITIAL));
        params.put(BUYER_RESERVATION, copy.get(BUYER_RESERVATION));
        params.put(SELLER_INITIAL, copy.get(SELLER_INITIAL));
        params.put(SELLER_RESERVATION, copy.get(SELLER_RESERVATION));
        params.put(DEADLINE, copy.get(DEADLINE));
    }

    public int get(int key) {
        return params.get(key);
    }

    public void update(int buyerInitial, int buyerRes, int sellerInitial,
            int sellerRes, int deadline) {
        params.put(BUYER_INITIAL, buyerInitial);
        params.put(BUYER_RESERVATION, buyerRes);
        params.put(SELLER_INITIAL, sellerInitial);
        params.put(SELLER_RESERVATION, sellerRes);
        params.put(DEADLINE, deadline);
    }
}
