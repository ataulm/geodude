package uk.ac.rhul.cs.dice.golem.action;

import java.util.ArrayList;

import uk.ac.rhul.cs.dice.golem.agent.AgentBody;
import uk.ac.rhul.cs.dice.golem.entity.Entity;

/**
 * Effectors are the agent's actuators in the environment. They produce events
 * (perform actions) in the environment of a certain type.
 * 
 * Types are defined at a platform or application level, representing a format
 * of message, such that an effector that produces events of a given type, and a
 * sensor that consumes events of that type, can communicate with each other.
 * 
 * @author ataulm
 * 
 */
public interface Effector extends Entity {
    /**
     * Process the given {@link Action}.
     * 
     * @param action
     * @return
     */
    public boolean act(Action action);

    /**
     * Return a list of action types that this effector can handle.
     * 
     * @return
     */
    public ArrayList<String> getActionTypes();

    /**
     * Return a reference to the {@link AgentBody} this effector is attached to.
     * 
     * @return body the associated AgentBody
     */
    public AgentBody getBody();

    /**
     * Returns true if it can process events of the given type.
     * 
     * @param type
     *            event type.
     * @return canHandle true if the Effector can process {@link Action} objects
     *         of this type, else false
     */
    public boolean handlesType(String type);

}
