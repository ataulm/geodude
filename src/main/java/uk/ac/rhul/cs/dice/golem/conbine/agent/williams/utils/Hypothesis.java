package uk.ac.rhul.cs.dice.golem.conbine.agent.williams.utils;

/**
 * @author Colin Williams
 * 
 */
public class Hypothesis {
    private double probability;

    public double getProbability() {
        return this.probability;
    }

    public void setProbability(double probability) {
        this.probability = probability;
    }
}
