package uk.ac.rhul.cs.dice.golem.agent;

import java.util.List;

import uk.ac.rhul.cs.dice.golem.action.Action;
import uk.ac.rhul.cs.dice.golem.action.Percept;
import uk.ac.rhul.cs.dice.golem.action.Sensor;
import uk.ac.rhul.cs.dice.golem.action.SensorHasPerceptListener;

public interface AgentBrain extends Runnable, SensorHasPerceptListener {

    /**
     * Try to perform the given action.
     * 
     * @param action
     *            the object containing the action type action to be performed
     * @throws Exception
     *             throws exception if the action could not be performed
     */
    public void act(Action action) throws Exception;



    /**
     * Gets the agent ID from the body.
     * 
     * @return agentId the id of the agent
     */
    public String getAgentId();

    /**
     * Returns all the unread {@link Percept} objects that are currently on the
     * list, or null if no perceptions found.
     * 
     * @return perceptions a list of all Percept objects on the AgentBrain's
     *         queue.
     */
    public List<Percept> getAllPerceptions();

    /**
     * Returns the last {@link Percept} that was added to the list (FIFO), or
     * null if no perceptions were found.
     * 
     * @return perception the last {@link Percept} in the queue
     */
    public Percept getPerception();

    /**
     * 
     * @param sensor
     */
    @Override
    public void onSensorHasPercept(Sensor sensor);

    /**
     * Resume the agent cycle
     * 
     * This should be the opposite of {@link #suspendCycle()}.
     */
    public void resumeCycle();

    /**
     * Sets the associated {@link AgentBody} with this AgentBrain.
     * 
     * @param body
     *            the associated AgentBody
     */
    public void setBody(AgentBody body);
    
    public AgentBody getBody();

    /**
     * Sets the associated {@link AgentMind} with this AgentBrain.
     * 
     * @param mind
     *            the associated AgentMind
     */
    public void setMind(AgentMind mind);
    
    public AgentMind getMind();

    /**
     * Set the delay between cycle steps in milliseconds.
     * 
     * @param threadDelay
     *            the delay between cycle steps
     */
    public void setThreadDelay(long threadDelay);

    /**
     * Start the agent cycle.
     * 
     * This should be the opposite of {@link #stopCycle()}.
     */
    public void startCycle();

    /**
     * Start the agent, but in a suspended state.
     * 
     * @see {@link #suspendCycle()}
     */
    public void startSuspended();

    /**
     * TODO: pending definition of active perception,and implementation details
     * Actively perceive the environment using the sensor and focus specified.
     * 
     * @param sensorId
     *            sensor to use
     * @param focus
     *            the area at which to focus the sensor
     */
    // public void perceive(String sensorId, String focus);

    /**
     * Stop the agent.
     * 
     * This should be the opposite of {@link #startCycle()}.
     */
    public void stopCycle();

    /**
     * Pause the agent cycle
     * 
     * This should be the opposite of {@link #resumeCycle()}.
     */
    public void suspendCycle();

}
