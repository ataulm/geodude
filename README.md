## GOLEMlite
**GOLEMlite** is a framework for the creation of multi-agent systems, using a combination of Java and Prolog, or Java exclusively.

The primary motivation behind the project is to introduce students to the concept of cognitive agents and multi-agent systems, via a framework that demonstrates the foundational aspects of the theory: sensors, effectors, and various stages of the agent-cycle (perceive, decide, act).

**GOLEMlite** is based on some of the concepts which underlie the [**OpenGOLEM**][golemsite] project, namely _agents_, _containers_, _sensors_ and _effectors_. While **OpenGOLEM** requires the agent environment to be "specified declaratively as a logic-based theory", **GOLEMlite** allows a Java-based agent environment, with the possibility of extension, using a logic-based theory.

The reasoning behind this is largely due to the ubiquity of procedural languages like Java in university courses, relative to declarative languages like Prolog. That said, **GOLEMlite** _does_ currently support Prolog-based agent minds; from the point of view of the project's purpose (to serve as a learning/teaching tool), this is sufficient.

[golemsite]: http://golem.cs.rhul.ac.uk/


## Getting started
Clone the repository to your local machine (or download and extract the archive).

**GOLEMlite** requires Java 7 and Maven (or a Maven plugin for your IDE).

If using an unlisted IDE, please remember not to commit any IDE specific files by adding exceptions in the .gitignore file. Eclipse and IntelliJ IDEA exceptions have already been added (for the most part).

### Eclipse
Eclipse users should install a relevant Maven plugin to open this project (m2e is recommended).

Update site for m2e: `http://download.eclipse.org/technology/m2e/releases`. After m2e is installed, import **GOLEMlite**.

Select "File > Import > Maven > Existing Maven Projects", and navigate to the project, selecting the project's parent directory.

Eclipse (and/or m2e) _should_ handle everything, but there may be a few errors:

- Sometimes, Eclipse thinks the project is using the Java project structure convention where "src/" is the source directory. Maven opts for a different structure, with "src/main/java" for source, and "src/test/java" for tests. If this is the error, right-click on the project in the Package or Project Explorer pane and choose "Properties". Under "Java Build Path > Source", select the "src" directory and remove it. Choose "Add Folder" and select the "src/main/java" and "src/main/resources" directories.

- If the pom.xml file has been changed externally, select the project in the Project Explorer pane and press "alt-F5" for "Maven > Update Project".

### IntelliJ IDEA
IntelliJ users can import the project by selecting the "pom.xml" file. (I'm new to IntelliJ, and have not used any Maven plugins for it; I opt instead for a local Maven installation.)

### Maven
[Download Maven][maven-download] (we use v3.0.5) and extract it to a safe location (on Linux, I chose "/opt/maven/" and Windows, "c:/maven"). Add the "/bin" directory to your PATH so you can run maven from the command line.

On Windows, it's necessary to add an environment variable called "JAVA_HOME" to point to your JDK installation. Right-click on My Computer and select Properties. Choose "Advanced system settings > Environment Variables..." and under "System variables" click "New". Use `JAVA_HOME` as the name, and the location of your JDK as the value, e.g. `C:\Java\jdk1.7.0_25`. Opening a fresh command prompt will be needed before changes are recognised.

While Java 7 is required, it's not often the default on many machines yet (e.g. Android developers will likely be on JDK 1.6 as 1.7 isn't supported yet). As such, the build will fail (when running from the command line) unless you also set the environment variable called "JAVA_1_7_HOME" and have it point to your JDK 1.7 directory (which may be the same as your JAVA_HOME variable). In the case where JAVA_HOME is your JDK 1.7 directory, add a new environment variable with name `JAVA_1_7_HOME` and value `%JAVA_HOME%`, otherwise (if "JAVA_HOME" points to 1.6) put the path to the 1.7 directory as the value.

Navigate to the project root and type `mvn install`. This will download all the non-local dependencies, and build the project. `mvn clean` will remove the output files, and `mvn clean install` will remove the old output files and build the project again.

[maven-download]: http://maven.apache.org/download.cgi

